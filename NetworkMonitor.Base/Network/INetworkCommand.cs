﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.Network
{
    public interface INetworkCommand : IPacket
    {
        EventType ExpectResponseType { get; }
        ThreadCommandType CommandType { get; }
        string SendContent { get; set; }
    }
    public enum ThreadCommandType
    {
        LogIn,
        ChangePassword,
        KichUser,
        LoadManagedThreads,
        StoreManagedThread,
        QueryUserList,
        SendMessage,
        StartThread,
        StartThreadImmediate,
        StopThread,
        DestroyThread,
        LoadThreadSetting,
        LoadThreadSchedule,
        AddThreadSchedule,
        ModifyThreadSchedule,
        DeleteThreadSchedule,
        LoadThreadLog,
        LoadThreadLogContent,
        StoreThreadSetting
    }
}
