﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.Network
{
    public interface INetworkEvent : IPacket
    {
        EventType Type { get; }
        string OriginalContent { get; set; }
        string Function { get; set; }
        string ClassName { get; set; }
    }

    public enum EventType
    {
        None = -2,
        UnknowEvent = -1,
        SuccessEvent = 0,
        LoginResponse = 1,
        LogMonitorEvent = 2,
        LogActionEvent = 3,
        UserConnectedEvent = 4,
        UserDisconnectedEvent = 5,
        SettingInfo = 6,
        ScheduleInfo = 7,
        LogInfo = 8,
        UnloadThreadEvent = 9,
        LoadThreadEvent = 10,
        AddScheduleResponse = 11,
        LogContent = 12,
        RenameThread = 13,
        ManagedThreadsLoad = 14,
        Error = 15
    }
}
