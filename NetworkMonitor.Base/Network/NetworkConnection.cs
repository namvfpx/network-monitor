﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Dynamic;
using NetworkMonitor.Utils;

namespace NetworkMonitor.Network
{
    public class NetworkConnection : IDisposable
    {
        class ResponseWaiter
        {
            public EventType ExpectType { get; set; }
            public INetworkEvent Event { get; set; }
        }
        private TcpClient TcpClient { get; set; } = null;
        public string Host { get; private set; } = "";
        public int Port { get; private set; } = 0;
        public bool IsConnected { get => TcpClient != null && TcpClient.Connected; }
        private Action<IPacket> OnPacket{ get; set; } = null;

        public IPacketFactory PacketFactory { get; set; }
        public Action<Exception> OnDisconnected { get; set; } = (x) => {};

        private Queue<INetworkEvent> eventQueues = new Queue<INetworkEvent>();
        private Dictionary<string, ResponseWaiter> waitingCommand = new Dictionary<string, ResponseWaiter>();

        private Thread receivingThread = null;
        private Thread processingThread = null;

        /// <summary>
        /// Creates instance of <code>NetworkConnection</code>
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="factory"></param>
        public NetworkConnection(string host, int port, IPacketFactory factory)
        {
            PacketFactory = factory;
            TcpClient = new TcpClient();
            TcpClient.SendTimeout = Config.Timeout;
            //TcpClient.ReceiveTimeout = Config.Timeout;
            Host = host;
            Port = port;
        }

        /// <summary>
        /// Connects and starts to receive messages
        /// </summary>
        /// <param name="messageHandler"></param>
        public void Connect(Action<IPacket> messageHandler)
        {
            OnPacket = messageHandler;
            TcpClient.Connect(Host, Port);

            receivingThread = new Thread(() => {
                Exception e = null;
                while (IsConnected)
                {
                    try
                    {
                        INetworkEvent pack = PacketFactory.FromStream(TcpClient.GetStream(),
                            (responseId) => {
                                ResponseWaiter waiter = null;
                                lock (waitingCommand)
                                {
                                    waitingCommand.TryGetValue(responseId, out waiter);
                                }
                                if (waiter != null)
                                {
                                    return waiter.ExpectType;
                                }
                                return EventType.None;
                            });
                        if (pack != null)
                        {
                            lock (eventQueues)
                            {
                                eventQueues.Enqueue(pack);
                                if (eventQueues.Count > Config.MaxEventStoragePerThread)
                                {
                                    eventQueues.Dequeue();
                                }
                            }
                            if (!string.IsNullOrEmpty(pack.Identifier))
                            {
                                ResponseWaiter waiter = null;
                                lock (waitingCommand)
                                {
                                    waitingCommand.TryGetValue(pack.Identifier, out waiter);
                                }

                                if (waiter != null)
                                {
                                    lock (waiter)
                                    {
                                        waiter.Event = pack;
                                        Monitor.PulseAll(waiter);
                                    }
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                Thread.Sleep(100);
                            }
                            catch { }
                        }
                    }
                    catch (IOException ioe)
                    {
                        e = ioe;
                        try
                        {
                            TcpClient.Close();
                        }
                        catch
                        { }
                        break;
                    }
                    catch
                    {

                    }
                }
                

                OnDisconnected?.Invoke(e);
            });

            receivingThread.Start();
            processingThread = new Thread(() => {
                while (IsConnected)
                {
                    INetworkEvent pack = null;
                    lock (eventQueues)
                    {
                        if (eventQueues.Count > 0)
                            pack = eventQueues.Dequeue();
                    }
                    if (pack != null)
                    {
                        OnPacket?.Invoke(pack);
                    }
                    else
                    {
                        try
                        {
                            Thread.Sleep(100);
                        }
                        catch { }
                    }
                }
            });
            processingThread.Start();
        }

        public virtual void Dispose()
        {
            TcpClient.Close();
            TcpClient.Dispose();
            TcpClient = null;
            eventQueues.Clear();
            waitingCommand.Clear();
        }

        /// <summary>
        /// Sends message to server
        /// </summary>
        /// <param name="packet"></param>
        public void SendNoWait(INetworkCommand packet)
        {
            Send(packet, false);
        }

        public INetworkEvent Send(INetworkCommand packet)
        {
            return Send(packet, true);
        }
        private INetworkEvent Send(INetworkCommand packet, bool waitResponse)
        {
            try
            {
                var data = PacketFactory.PacketToBytes(packet);
                if (!waitResponse)
                {
                    TcpClient.GetStream().Write(data, 0, data.Length);
                    OnPacket?.Invoke(packet);
                    return null;
                }
                ResponseWaiter waiter = new ResponseWaiter();
                waiter.ExpectType = packet.ExpectResponseType;
                lock (waitingCommand)
                {
                    waitingCommand.Add(packet.Identifier, waiter);
                }
                TcpClient.GetStream().Write(data, 0, data.Length);
                OnPacket?.Invoke(packet);

                lock (waiter)
                {
                    try
                    {
                        Monitor.Wait(waiter, Config.Timeout);
                    }
                    catch (ThreadInterruptedException)
                    {
                        Thread.CurrentThread.Interrupt();
                    }
                }

                lock (waitingCommand)
                {
                    waitingCommand.Remove(packet.Identifier);
                }

                if (waiter.Event != null)
                {
                    return waiter.Event;
                }
                throw new TimeoutException("timeout");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
