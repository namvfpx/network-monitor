﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.Network
{
    public interface IPacket
    {
        string Protocol { get; set; }
        string Identifier { get; set; }
        bool IsOutDirection { get; }
        DateTime Time { get; set; }
    }
}
