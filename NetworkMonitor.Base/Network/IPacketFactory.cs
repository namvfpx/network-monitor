﻿using NetworkMonitor.ThreadMonitor.Command;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.Network
{
    public interface IPacketFactory
    {
        /// <summary>
        /// Appends bytes to Factory Buffer, return IPacket if buffer is enough or else return null
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="getExpectedType">fucntion to get type of response from responseId to convert to INetworkEvent</param>
        /// <returns></returns>
        INetworkEvent FromStream(Stream stream, Func<string, EventType> getExpectedType);

        /// <summary>
        /// Converts packet to byte array
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        byte[] PacketToBytes(INetworkCommand packet);
    }
}
