﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.Utils
{
    public class NameValuePair<TName, TValue> where TValue : IComparable
    { 
        public TName Name { get; set; } = default(TName);
        public TValue Value { get; set; } = default(TValue);

        public NameValuePair()
        {

        }

        public NameValuePair(TName name, TValue value)
        {
            Name = name;
            Value = value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (obj.GetType().IsGenericType &&
                obj.GetType().GetGenericTypeDefinition() == typeof(NameValuePair<TName, TValue>))
            {
                var newObj = obj as NameValuePair<TName, TValue>;
                if ((newObj.Value == null && Value != null)
                    || (newObj.Value != null && Value == null))
                    return false;
                else if (newObj.Value == null && Value == null)
                    return true;
                return Value.CompareTo(newObj.Value) == 0;
            }
            return false;
        }
    }
}
