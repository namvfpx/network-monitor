﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.Utils
{
    public static class CollectionUtil
    {
        public static List<object> CopyList(this List<object> src)
        {
            return src.Select(x => {
                if (x is List<object>)
                {
                    return CopyList(x as List<object>);
                }
                else if (x is ICloneable)
                {
                    return ((ICloneable)x).Clone();
                }
                else
                {
                    return x;
                }
            }).ToList();
        }
        public static List<T> CopyList<T>(this List<T> src) where T : ICloneable
        {
            return src.Select(x => (T)x.Clone()).ToList();
        }

        public static TValue GetOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dic, 
            TKey key, TValue defaultValue = default(TValue)) 
        {
            TValue value;
            if (dic.TryGetValue(key, out value))
            {
                return value;
            }
            return defaultValue;
        }
    }
}
