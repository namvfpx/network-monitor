﻿using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.ThreadMonitor.ThreadException;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NetworkMonitor.Utils
{
    public partial class MessageUtil
    {
        public const string KEY_ABORT = "abort";
        public const string KEY_ADD = "add";
        public const string KEY_ADD_NODE = "add-node";
        public const string KEY_ANONYMOUS = "anonymous";
        public const string KEY_APP_NAME = "app-name";
        public const string KEY_APP_VER = "app-ver";
        public const string KEY_CANCEL = "cancel";
        public const string KEY_CHANGE_PASSWORD = "change-password";
        public const string KEY_CHANNEL = "channel";
        public const string KEY_CLASS_NAME = "class-name";
        public const string KEY_CONFIRM_PASSWORD = "confirm-password";
        public const string KEY_CONNECT = "connect";
        public const string KEY_CONTENT = "content";
        public const string KEY_COPY = "copy";
        public const string KEY_CYCLE = "cycle";
        public const string KEY_DAILY = "daily";
        public const string KEY_DAY_SUN = "day-sun";
        public const string KEY_DAY_MON = "day-mon";
        public const string KEY_DAY_TUE = "day-tue";
        public const string KEY_DAY_WED = "day-wed";
        public const string KEY_DAY_THU = "day-thu";
        public const string KEY_DAY_FRI = "day-fri";
        public const string KEY_DAY_SAT = "day-sat";
        public const string KEY_DELETE = "delete";
        public const string KEY_DELETE_CONFIRM = "delete-confirm";
        public const string KEY_DESTROY = "destroy";
        public const string KEY_DESTROY_THREAD_CONFIRM = "destroy-thread-confirm";
        public const string KEY_DETAIL = "detail";
        public const string KEY_DISCONNECT = "disconnect";
        public const string KEY_EDIT = "edit";
        public const string KEY_ENCRYPT = "encrypt";
        public const string KEY_END_TIME = "end-time";
        public const string KEY_EXIT = "exit";
        public const string KEY_EXIT_PROGRAM = "exit-program";
        public const string KEY_EXIT_PROGRAM_CONFIRM = "exit-program-confirm";
        public const string KEY_EXPECTED_DATE = "expected-date";
        public const string KEY_HOST = "host";
        public const string KEY_ID = "id";
        public const string KEY_IMMEDIATE = "immediate";
        public const string KEY_INFO = "info";
        public const string KEY_INVALID_FIELD = "invalid-field";
        public const string KEY_INVALID_INPUT = "invalid-input";
        public const string KEY_IGNORE = "ignore";
        public const string KEY_LOG = "log";
        public const string KEY_LOG_OF_THREAD = "log-of-thread";
        public const string KEY_LOGIN = "login";
        public const string KEY_MANAGE = "manage";
        public const string KEY_MONITOR = "monitor";
        public const string KEY_MONTH_1 = "month-1";
        public const string KEY_MONTH_2 = "month-2";
        public const string KEY_MONTH_3 = "month-3";
        public const string KEY_MONTH_4 = "month-4";
        public const string KEY_MONTH_5 = "month-5";
        public const string KEY_MONTH_6 = "month-6";
        public const string KEY_MONTH_7 = "month-7";
        public const string KEY_MONTH_8 = "month-8";
        public const string KEY_MONTH_9 = "month-9";
        public const string KEY_MONTH_10 = "month-10";
        public const string KEY_MONTH_11 = "month-11";
        public const string KEY_MONTH_12 = "month-12";
        public const string KEY_MONTH_DAY = "month-day";
        public const string KEY_MONTHLY = "monthly";
        public const string KEY_NAME = "name";
        public const string KEY_NEW = "new";
        public const string KEY_NEW_ENCRYPTION = "new-encryption";
        public const string KEY_NEW_PASSWORD = "new-password";
        public const string KEY_NO = "no";
        public const string KEY_NO_ENCRYPT = "no-encrypt";
        public const string KEY_NODE = "node";
        public const string KEY_NODE_ALREADY_EXISTED = "node-already-existed";
        public const string KEY_NODE_CONNECT_ERROR = "node-connect-error";
        public const string KEY_NODE_DISCONNECTED = "node-disconnected";
        public const string KEY_NODE_EXISTING = "node-existing";
        public const string KEY_OK = "ok";
        public const string KEY_OLD_ENCRYPT = "old-encryption";
        public const string KEY_OLD_PASSWORD = "old-password";
        public const string KEY_PARAMETER = "parameter";
        public const string KEY_PARAMETERS = "parameters";
        public const string KEY_PASSWORD = "password";
        public const string KEY_PASSWORD_CHANGED_SUCCESS = "password-changed-success";
        public const string KEY_PASSWORD_INVALID = "password-invalid";
        public const string KEY_PASSWORD_NOT_MATCH = "password-not-match";
        public const string KEY_PASSWORD_WRONG = "password-wrong";
        public const string KEY_PORT = "port";
        public const string KEY_PROFILE = "profile";
        public const string KEY_RECONNECT = "reconnect";
        public const string KEY_REMOVE = "remove";
        public const string KEY_RESTART = "restart";
        public const string KEY_RETRY = "retry";
        public const string KEY_SAVE_BEFORE_EXIT = "save-before-exit";
        public const string KEY_SAVE_LOGIN_INFO = "save-login-info";
        public const string KEY_SAVE_SETTING = "save-setting";
        public const string KEY_SAVE_SETTING_CONFIRM = "save-setting-confirm";
        public const string KEY_SCHEDULE = "schedule";
        public const string KEY_SERVICE = "service";
        public const string KEY_SESSION = "session";
        public const string KEY_SETTING = "setting";
        public const string KEY_START = "start";
        public const string KEY_START_TIME = "start-time";
        public const string KEY_STARTUP_TYPE = "startup-type";
        public const string KEY_STATUS = "status";
        public const string KEY_STOP = "stop";
        public const string KEY_STORED_NODE = "stored-node";
        public const string KEY_SWITCH = "switch";
        public const string KEY_TIME = "time";
        public const string KEY_TIMES = "times";
        public const string KEY_THREAD = "thread";
        public const string KEY_THREAD_APP_NAME = "thread-app-name";
        public const string KEY_THREAD_APP_VER = "thread-app-ver";
        public const string KEY_THREAD_MANAGE = "thread-manage";
        public const string KEY_TYPE = "type";
        public const string KEY_USERNAME = "username";
        public const string KEY_USERNAME_INVALID = "username-invalid";
        public const string KEY_USERNAME_NOT_EXIST = "username-not-exist";
        public const string KEY_VALUE = "value";
        public const string KEY_WEEK_DAY = "week-day";
        public const string KEY_WEEKLY = "weekly";
        public const string KEY_YEAR_MONTH = "year_month";
        public const string KEY_YEARLY = "yearly";
        public const string KEY_YES = "yes";

        public static string ToMessage(ErrorEvent e, params string[] obj)
        {
            string message = ToMessage(e.ErrorContext) + "\r\n"
                            + ToMessage(e.ErrorDescription) + "\r\n"
                            + ToMessage(e.ErrorInfo);
            return string.Format(message, obj);
        }
        public static string ToMessage(Exception e, params string[] obj)
        {
            if (e is CommandException)
            {
                return ToMessage(((CommandException)e).Event);
            }
            else return ToMessage(e.Message, obj);
        }

        public static string ToMessage(string message, params object[] objs)
        {
            var msg = LanguageUtil.GetText(message);
            return string.Format(msg, objs);
        }

        public static string[] AppendLog(string[] logLines, string log)
        {
            string[] newLines = ConvertToLines(log);
            if (logLines == null)
                return newLines;
            logLines = logLines.Concat(newLines).ToArray();
            if (logLines.Length > Config.MaxLogLinePerThread)
            {
                logLines = logLines.Skip(logLines.Length - Config.MaxLogLinePerThread).ToArray();
            }

            return logLines;
        }

        public static string[] ConvertToLines(string log)
        {
            string[] newLines = Regex.Split(TrimLastCRLF(log), "\r\n|\r|\n");
            return newLines;
        }
        public static string TrimLastCRLF(string log)
        {
            return log.TrimEnd('\r', '\n');
        }
        public static string RemoveHtmlTag(string text)
        {
            return Regex.Replace(text, @"<[^>]*>", string.Empty);
        }
    }
}
