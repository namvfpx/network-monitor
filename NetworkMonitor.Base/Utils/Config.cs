﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.Utils
{
    public class Config
    {
        public static int Timeout { get; set; } = 30000;
        public static string DefaultLanguage { get; set; } = "en";

        public static int MaxLogLinePerThread { get; set; } = 200;
        public static int MaxEventStoragePerThread { get; set; } = 50;
    }
}
