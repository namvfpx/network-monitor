﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace NetworkMonitor.Utils
{
    public class EncryptUtil
    {

        public static string SHA = "SHA";

        private static byte[] AES_SALT = new byte[8];

        public static List<NameValuePair<string, string>> Algorithms => new List<NameValuePair<string, string>>()
        {
                new NameValuePair<string, string>() { Name = LanguageUtil.GetText(MessageUtil.KEY_NO_ENCRYPT), Value = "" },
                new NameValuePair<string, string>() { Name = LanguageUtil.GetText(SHA), Value = SHA }
        };
        public static string EncryptPassword(string input, string algorithm)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            var bytes = Encoding.Default.GetBytes(input);
            if (SHA.Equals(algorithm))
            {
                var alg = new SHA1Managed();
                var result = alg.ComputeHash(bytes);
                return Convert.ToBase64String(result);
            }
            throw new Exception("algorithm-not-support");
        }

        public static byte[] AESEncrypt(byte[] input, string password)
        {
            var aes = new AesManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            var key = new Rfc2898DeriveBytes(password, AES_SALT, 1024);
            aes.Key = key.GetBytes(aes.KeySize / 8);
            aes.IV = key.GetBytes(aes.BlockSize / 8);
            aes.Mode = CipherMode.CBC;
            var transform = aes.CreateEncryptor(aes.Key, aes.IV);

            using (var outputStream = new MemoryStream())
            {
                using (var cryptStream = new CryptoStream(outputStream, transform, CryptoStreamMode.Write))
                {
                    cryptStream.Write(input, 0, input.Length);
                }

                return outputStream.ToArray();
            }
        }

        public static byte[] AESDecrypt(byte[] input, string password)
        {
            var aes = new AesManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            var key = new Rfc2898DeriveBytes(password, AES_SALT, 1024);
            aes.Key = key.GetBytes(aes.KeySize / 8);
            aes.IV = key.GetBytes(aes.BlockSize / 8);
            aes.Mode = CipherMode.CBC;
            var transform = aes.CreateDecryptor(aes.Key, aes.IV);

            using (var outputStream = new MemoryStream())
            {
                using (var cryptStream = new CryptoStream(outputStream, transform, CryptoStreamMode.Write))
                {
                    cryptStream.Write(input, 0, input.Length);
                }

                return outputStream.ToArray();
            }
        }
    }
}
