﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Xml.Serialization;
using System.IO;

namespace NetworkMonitor.Utils
{
    [XmlRoot(ElementName = "localization")]
    public class XmlLocalization
    {
        [XmlElement(ElementName = "entry")]
        public List<XmlEntry> Entries { get; set; } = new List<XmlEntry>();
    }
    public class XmlEntry
    {
        [XmlAttribute]
        public string Key { get; set; }
        [XmlText]
        public string Value { get; set; }
    }
    public class LanguageUtil
    {
        private static Dictionary<string, string> dictionary = null;
        public static string GetText(string key)
        {
            if (dictionary == null)
            {
                FromFile("en");
            }

            string value = "";
            if (dictionary.TryGetValue(key, out value))
            {
                if (!string.IsNullOrEmpty(value))
                {
                    return value;
                }
            }
            return key;
        }
        public static string GetText(string key, params object[] parameters)
        {
            var text = GetText(key);
            return string.Format(text, parameters);
        }
        public static void FromFile(string defaultLanguage)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(XmlLocalization));
            try
            {
                XmlLocalization localization = null;
                using (var fileStream = new FileStream(defaultLanguage + ".xml", FileMode.Open))
                {
                    localization = (XmlLocalization)serializer.Deserialize(fileStream);
                }

                if (localization != null)
                {
                    var localizationEntries = (from entry in localization.Entries
                                               group entry by entry.Key into gr
                                               select new
                                               {
                                                   gr.Key,
                                                   gr.Last().Value
                                               }).ToDictionary(x => x.Key, x => x.Value);

                    var fieldInfos = typeof(MessageUtil).GetFields(BindingFlags.Public |
                                    BindingFlags.Static | BindingFlags.FlattenHierarchy).Where(x => x.IsLiteral && !x.IsInitOnly).ToList();
                    dictionary = new Dictionary<string, string>();
                    fieldInfos.Select(x => Convert.ToString(x.GetValue(null))).ToList().ForEach(x => {
                        var content = localizationEntries.GetOrDefault(x, "");
                        if (dictionary.ContainsKey(x))
                        {
                            dictionary[x] = content;
                        }
                        else
                        {
                            dictionary.Add(x, content);
                        }
                    });
                    return;
                }
            }
            catch
            {
            }
            dictionary = new Dictionary<string, string>();
        }

        public static void ToFile(string defaultLanguage)
        {
            var xmlEntry = new XmlLocalization();
            xmlEntry.Entries = dictionary.OfType<KeyValuePair<string, string>>()
                .Select(x => new XmlEntry() {
                    Key = x.Key,
                    Value = x.Value
            }).OrderBy(x => x.Key).ToList();
            XmlSerializer serializer = new XmlSerializer(typeof(XmlLocalization));
            using (var fileStream = new FileStream(defaultLanguage + ".xml", FileMode.Create))
            {
                serializer.Serialize(fileStream, xmlEntry);
            }
        }
    }
}
