﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class AddScheduleCommand : ThreadCommand
    {
        public ScheduleSetting Schedule { get; set; }
        public override EventType ExpectResponseType => EventType.AddScheduleResponse;
        public AddScheduleCommand(string threadId) : base(threadId, ThreadCommandType.AddThreadSchedule)
        {

        }
    }

    public class DeletecheduleCommand : ThreadCommand
    {
        public string ScheduleId { get; set; }
        public override EventType ExpectResponseType => EventType.SuccessEvent;
        public DeletecheduleCommand(string threadId) : base(threadId, ThreadCommandType.DeleteThreadSchedule)
        {

        }
    }

    public class ModifyScheduleCommand : ThreadCommand
    {
        public ScheduleSetting Schedule { get; set; }
        public override EventType ExpectResponseType => EventType.SuccessEvent;
        public ModifyScheduleCommand(string threadId) : base(threadId, ThreadCommandType.ModifyThreadSchedule)
        {

        }
    }

}
