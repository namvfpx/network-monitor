﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class LogIn : CommandBase
    {
        public string Username { get; set; } = "";
        public string Password { get; set; } = "";
        public override ThreadCommandType CommandType => ThreadCommandType.LogIn;
        public override EventType ExpectResponseType => EventType.LoginResponse;
    }
}
