﻿using NetworkMonitor.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public abstract class CommandBase : INetworkCommand
    {
        public virtual string Identifier { get; set; } = Convert.ToString(DateTime.Now.Ticks);
        public virtual EventType ExpectResponseType => EventType.None;
        public virtual ThreadCommandType CommandType { get; protected set; }
        public virtual string SendContent { get; set; }
        public string Protocol { get; set; }
        public bool IsOutDirection => true;

        public DateTime Time { get; set; } = DateTime.Now;

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Protocol);
            builder.Append(" | ID=" + Identifier);
            builder.Append(" | Type=" + CommandType.ToString());
            builder.Append(" | SendContent=" + SendContent);
            return builder.ToString();
        }
    }
}
