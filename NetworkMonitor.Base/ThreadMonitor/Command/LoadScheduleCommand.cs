﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class LoadScheduleCommand : ThreadCommand
    {
        public override EventType ExpectResponseType => EventType.ScheduleInfo;

        public LoadScheduleCommand(string threadId) : base(threadId, ThreadCommandType.LoadThreadSchedule)
        {

        }
    }
}
