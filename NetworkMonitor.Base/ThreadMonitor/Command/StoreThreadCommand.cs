﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class StoreThreadCommand : ThreadCommand
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public string ClassName { get; set; }
        public StoreThreadCommand(string threadId) : base(threadId, Network.ThreadCommandType.StoreManagedThread)
        {

        }
    }
}
