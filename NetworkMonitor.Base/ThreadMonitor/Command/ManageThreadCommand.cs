﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class ManageThreadCommand : CommandBase
    {
        public override ThreadCommandType CommandType => ThreadCommandType.LoadManagedThreads;
        public override EventType ExpectResponseType => EventType.ManagedThreadsLoad;
    }
}
