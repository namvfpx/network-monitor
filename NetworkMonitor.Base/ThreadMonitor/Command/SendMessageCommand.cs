﻿using NetworkMonitor.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class SendMessageCommand : CommandBase
    {
        public override ThreadCommandType CommandType => ThreadCommandType.SendMessage;
        public override EventType ExpectResponseType => EventType.SuccessEvent;
        public string Message { get; set; } = "";
    }
}
