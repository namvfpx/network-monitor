﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class ThreadCommand : CommandBase
    {
        public string ThreadId { get; set; } = "";
        public override EventType ExpectResponseType => EventType.SuccessEvent;
        public ThreadCommand(string threadId, ThreadCommandType type)
        {
            ThreadId = threadId;
            CommandType = type;
        }
    }
}
