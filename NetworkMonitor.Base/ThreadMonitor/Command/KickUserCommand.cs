﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Network;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class KickUserCommand : CommandBase
    {
        public override ThreadCommandType CommandType => ThreadCommandType.KichUser;
        public override EventType ExpectResponseType => EventType.SuccessEvent;
        public string SessionId { get; set; } = "";
    }
}
