﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class LoadLogCommand : ThreadCommand
    {
        public override EventType ExpectResponseType => EventType.LogInfo;
        public LoadLogCommand(string threadId) : base(threadId, ThreadCommandType.LoadThreadLog)
        {

        }
    }
}
