﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class LoadLogContentCommand : ThreadCommand
    {
        public override EventType ExpectResponseType => EventType.LogContent;
        public string ThreadLogName { get; set; } = "";
        public LoadLogContentCommand(string threadId, string logName) 
            : base(threadId, ThreadCommandType.LoadThreadLogContent)
        {
            ThreadLogName = logName;
        }
    }
}
