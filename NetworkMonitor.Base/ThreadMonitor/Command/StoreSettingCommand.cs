﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class StoreSettingCommand : ThreadCommand
    {
        public override EventType ExpectResponseType => EventType.SuccessEvent;
        public string ThreadName { get; set; } = "";
        public int ThreadStartupType { get; set; } = 0;
        public string ClassName { get; set; } = "";
        public List<ThreadParameter> Parameters { get; set; } = new List<ThreadParameter>();
        
        public StoreSettingCommand(string threadId) : base(threadId, ThreadCommandType.StoreThreadSetting)
        {

        }
    }
}
