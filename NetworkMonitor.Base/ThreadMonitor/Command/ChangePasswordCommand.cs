﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class ChangePasswordCommand : CommandBase
    {
        public override EventType ExpectResponseType => EventType.SuccessEvent;
        public override ThreadCommandType CommandType => ThreadCommandType.ChangePassword;
        public string OldPassword { get; set; } = "";
        public string NewPassword { get; set; } = "";
        public string RealPassword { get; set; } = "";
    }
}
