﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Network;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class QueryUserListCommand : CommandBase
    {
        public override ThreadCommandType CommandType => ThreadCommandType.QueryUserList;
        public override EventType ExpectResponseType => EventType.SuccessEvent;
    }
}
