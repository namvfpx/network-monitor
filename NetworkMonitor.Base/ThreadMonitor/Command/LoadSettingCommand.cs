﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Command
{
    public class LoadSettingCommand : ThreadCommand
    {
        public override EventType ExpectResponseType => EventType.SettingInfo;
        public LoadSettingCommand(string threadId) : base(threadId, ThreadCommandType.LoadThreadSetting)
        {

        }
    }
}
