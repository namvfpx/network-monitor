﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Utils;

namespace NetworkMonitor.ThreadMonitor
{
    public class ThreadParameter : ICloneable
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public ParameterType Type { get; set; }
        public List<ThreadParameter> Definition { get; set; }
        public string Description { get; set; }
        public string Index { get; set; }

        public object OriginalDefinition { get; set; } // To send back server if have to

        public static object ValueCopy(object value)
        {
            if (value is ICloneable)
            {
                return ((ICloneable)value).Clone();
            }
            else if (value is List<object>)
            {
                return ((List<object>)value).CopyList();
            }
            else return value;
        }
        public static List<ThreadParameter> GetParameterDefinition(object definitions)
        {
            var result = new List<ThreadParameter>();
            if (definitions is List<object>)
            {
                var lsDefinitions = definitions as List<object>;
                lsDefinitions.ForEach(definition => {
                    if (definition is List<object>)
                    {
                        var setting = definition as List<object>;

                        ThreadParameter p = new ThreadParameter();
                        p.Name = Convert.ToString(setting[0]);
                        try
                        {
                            p.Value = setting[1];
                        }
                        catch
                        {
                            p.Value = p.Name;
                            // dont have value, nevermind, use Name
                        }
                        try
                        {
                            p.Type = (ParameterType)Convert.ToInt32(setting[2]);
                        }
                        catch
                        {
                            // dont have type, nevermind, use text
                            p.Type = ParameterType.PARAM_TEXTBOX_MAX;
                        }
                        try
                        {
                            p.Definition = GetParameterDefinition(setting[3]);
                        }
                        catch
                        {
                            // dont have definition, nevermind, leave it blank
                        }
                        try
                        {
                            p.Description = Convert.ToString(setting[4]);
                            p.Index = Convert.ToString(setting[5]);
                        }
                        catch { }

                        result.Add(p);
                    }
                    else
                    {
                        var def = Convert.ToString(definition);
                        ThreadParameter p = new ThreadParameter()
                        {
                            Name = def,
                            Value = def,
                            Type = ParameterType.PARAM_TEXTBOX_MAX,
                            Definition = new List<ThreadParameter>(),
                            Description = "",
                            Index = ""
                        };

                        result.Add(p);
                    }
                });
            }
            else
            {
                var def = Convert.ToString(definitions);
                ThreadParameter p = new ThreadParameter()
                {
                    Name = def,
                    Value = def,
                    Type = ParameterType.PARAM_TEXTBOX_MAX,
                    Definition = new List<ThreadParameter>(),
                    Description = "",
                    Index = ""
                };

                result.Add(p);
            }

            return result;
        }

        public static string ValueToString(object value)
        {
            if (value is List<object>)
            {
                var lst = (List<object>)value;
                StringBuilder sb = new StringBuilder();

                sb.Append("[");
                if (lst != null)
                {
                    if (lst.Count > 0)
                    {
                        sb.Append(ValueToString(lst[0]));
                    }
                    for (int i = 1; i < lst.Count; i++)
                    {
                        sb.Append(",").Append(ValueToString(lst[i]));
                    }

                }
                sb.Append("]");
                return sb.ToString();
            }
            else return Convert.ToString(value);
        }

        public object Clone()
        {
            ThreadParameter param = new ThreadParameter();
            param.Name = Name;
            param.Value = ValueCopy(Value);
            param.Type = Type;
            param.Definition = Definition.CopyList();
            param.Description = Description;
            param.Index = Index;

            return param;
        }
    }
    public enum ParameterType
    {
        PARAM_LABEL = 0,
        PARAM_TEXTBOX_MASK = 1,
        PARAM_TEXTBOX_MAX = 2,
        PARAM_PASSWORD = 3,
        PARAM_COMBOBOX = 4,
        PARAM_CHECKBOX = 5,
        PARAM_TABLE = 6,
        PARAM_TEXTBOX_FILTER = 7,
        PARAM_TEXTAREA_MAX = 8,
        PARAM_SUB_TABULAR_EDITOR = 9,
        PARAM_LIST = 10
    }
}
