﻿using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor
{
    public class ScheduleSetting : ICloneable
    {
        public string Name => string.Format("{0}-{1}", LanguageUtil.GetText(ScheduleTypeText[Type]), 
                        string.IsNullOrEmpty(ScheduleId) ? LanguageUtil.GetText(MessageUtil.KEY_NEW) : ScheduleId);
        public string ScheduleId { get; set; } = "";
        public ScheduleType Type { get; set; } = ScheduleType.Daily;
        public string StartTime { get; set; } = "00:00:00";
        public string EndTime { get; set; } = "23:59:59";
        public DateTime ExpectDate { get; set; } = DateTime.Now;
        public int Cycle { get; set; } = 1;
        public int Times { get; set; } = 1;
        public List<int> WeekDays { get; set; } = new List<int>();
        public List<int> MonthDays { get; set; } = new List<int>();
        public List<int> YearMonths { get; set; } = new List<int>();
        public bool IsNew { get; set; } = false;
        
        public static Dictionary<ScheduleType, string> ScheduleTypeText { get; private set; }

        static ScheduleSetting()
        {
            ScheduleTypeText = new Dictionary<ScheduleType, string>();
            ScheduleTypeText.Add(ScheduleType.Daily, MessageUtil.KEY_DAILY);
            ScheduleTypeText.Add(ScheduleType.Weekly, MessageUtil.KEY_WEEKLY);
            ScheduleTypeText.Add(ScheduleType.Monthly, MessageUtil.KEY_MONTHLY);
            ScheduleTypeText.Add(ScheduleType.Yearly, MessageUtil.KEY_YEARLY);
        }

        public object Clone()
        {
            var newSetting = new ScheduleSetting()
            {
                ScheduleId = ScheduleId,
                Type = this.Type,
                StartTime = this.StartTime,
                EndTime = this.StartTime,
                ExpectDate = this.ExpectDate,
                Cycle = this.Cycle,
                Times = this.Times,
                WeekDays = WeekDays.Select(x => x).ToList(),
                MonthDays = MonthDays.Select(x => x).ToList(),
                YearMonths = YearMonths.Select(x => x).ToList(),
            };

            return newSetting;
        }
    }

    public enum ScheduleType
    {
        Daily = 0,
        Weekly = 1,
        Monthly = 2,
        Yearly = 3
    }
}
