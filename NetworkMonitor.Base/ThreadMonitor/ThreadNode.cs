﻿using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Command;
using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.ThreadMonitor.ThreadException;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor
{
    public class ThreadNode : IDisposable
    {
        public string Name
        {
            get
            {
                return string.Format("{0}:{1}({2})", Host, Port, Username);
            }
        }
        public string Username { get; set; } = "";
        public string Host { get; set; } = "";
        public int Port { get; set; } = 0;
        public string Password { get; set; } = "";
        public string Encrypt { get; set; } = "";
        public bool IsConnected { get => Connection != null && Connection.IsConnected; }
        public LogInResponse Info { get; set; } = null;
        public NetworkConnection Connection { get; private set; }
        protected IPacketFactory PacketFactory { get; private set; }
        public Action<ThreadNode, IPacket> OnMessage { get; set; }
        public Action<Exception> OnDisconnect { get; set; }
        public Action<ThreadNode, Exception> OnSendingError { get; set; }
        public string[] LogLines { get; set; } = new string[] { };
        public List<IPacket> EventStorage { get; set; } = new List<IPacket>();

        private bool isBusy = false;
        public bool IsBusy
        {
            get
            {
                lock(this)
                {
                    return isBusy;
                }
            }
            set
            {
                lock(this)
                {
                    isBusy = value;
                }
            }
        }

        public ThreadNode(string host, int port)
        {
            this.Host = host;
            this.Port = port;
        }

        public bool EstablishConnection(IPacketFactory netPacketFactory)
        {
            PacketFactory = netPacketFactory;
            Connection = new NetworkConnection(Host, Port, PacketFactory);
            Connection.OnDisconnected = (e) => OnDisconnect?.Invoke(e);
            Connection.Connect(HandleMessage);

            Info = (LogInResponse)SendCommand(new LogIn() {
                Username = Username,
                Password = Password
            });
            if (Info != null)
            {
                LogLines = MessageUtil.AppendLog(LogLines, Info.Log);
                return true;
            }
            else
            {
                Dispose();
                return false;
            }
        }

        public void HandleMessage(IPacket eventArg)
        {
            lock(EventStorage)
            {
                EventStorage.Add(eventArg);
                if (EventStorage.Count > Config.MaxEventStoragePerThread)
                {
                    EventStorage.RemoveAt(0);
                }
            }
            if (!eventArg.IsOutDirection)
            {
                var evt = eventArg as INetworkEvent;
                if (evt.Type == EventType.LogMonitorEvent)
                {
                    var monitorEvent = evt as LogMonitorEvent;
                    if (Info != null && Info.ThreadInfos != null)
                    {
                        var info = Info.ThreadInfos.Where(x => x.Id.Equals(monitorEvent.ThreadId)).FirstOrDefault();
                        if (info != null)
                        {
                            lock (info)
                            {
                                info.LogLines = MessageUtil.AppendLog(info.LogLines, monitorEvent.LogResult);
                            }
                        }
                    }
                }
                else if (evt.Type == EventType.LogActionEvent)
                {
                    var actionEvent = evt as LogActionEvent;
                    LogLines = MessageUtil.AppendLog(LogLines, actionEvent.Log);
                }
            }

            OnMessage?.Invoke(this, eventArg);
        }

        public void StartThread(ThreadInfo info)
        {
            SendCommand(info, ThreadCommandType.StartThread);
        }
        public void RestartThread(ThreadInfo info)
        {
            SendCommand(info, ThreadCommandType.StartThread);
        }
        public void StopThread(ThreadInfo info)
        {
            SendCommand(info, ThreadCommandType.StopThread);
        }
        public void DestroyTrhead(ThreadInfo info)
        {
            SendCommand(info, ThreadCommandType.DestroyThread);
        }
        public void StartImmediate(ThreadInfo info)
        {
            SendCommand(info, ThreadCommandType.StartThreadImmediate);
        }
        public INetworkEvent GetThreadSetting(ThreadInfo info)
        {
            var cmd = new LoadSettingCommand(info.Id);
            return SendCommand(cmd);
        }
        public INetworkEvent GetThreadSchedule(ThreadInfo info)
        {
            var cmd = new LoadScheduleCommand(info.Id);
            return SendCommand(cmd);
        }
        public INetworkEvent GetThreadLog(ThreadInfo info)
        {
            var cmd = new LoadLogCommand(info.Id);
            return SendCommand(cmd);
        }

        public INetworkEvent StoreSetting(ThreadInfo info, string threadName, string className, 
            int startupType, List<ThreadParameter> parameters)
        {
            var cmd = new StoreSettingCommand(info.Id)
            {
                ClassName = className,
                ThreadName = threadName,
                ThreadStartupType = startupType,
                Parameters = parameters
            };

            return SendCommand(cmd);
        }

        public INetworkEvent SaveScheduleSetting(ThreadInfo info, ScheduleSetting setting, bool isNew)
        {
            ThreadCommand cmd;
            if (isNew)
            {
                cmd = new AddScheduleCommand(info.Id)
                {
                    Schedule = setting
                };
            }
            else
            {
                cmd = new ModifyScheduleCommand(info.Id)
                {
                    Schedule = setting
                };
            }

            return SendCommand(cmd);
        }
        public INetworkEvent DeleteScheduleSetting(ThreadInfo info, ScheduleSetting setting)
        {
            ThreadCommand cmd = new DeletecheduleCommand(info.Id)
            {
                ScheduleId = setting.ScheduleId
            };

            return SendCommand(cmd);
        }

        public void SendCommand(ThreadInfo info, ThreadCommandType cmdType)
        {
            var cmd = new ThreadCommand(info.Id, cmdType);
            SendCommand(cmd);
        }

        public INetworkEvent SendCommand(INetworkCommand command)
        {
            try
            {
                if (IsConnected)
                {
                    if (command.ExpectResponseType == EventType.None)
                        Connection.SendNoWait(command);
                    else
                    {
                        var result = Connection.Send(command);
                        if (result is ErrorEvent)
                        {
                            throw new CommandException(result as ErrorEvent);
                        }
                        return result;
                    }
                }
                else
                {
                    throw new IOException("connection-closed");
                }
            }
            catch (Exception ex)
            {
                if (OnSendingError != null)
                {
                    OnSendingError.Invoke(this, ex);
                }
                else
                {
                    throw ex;
                }
            }
            return null;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public void Dispose()
        {
            if (IsConnected)
            {
                Connection.OnDisconnected = null;
                OnDisconnect = null;
                OnMessage = null;
                
                Connection.Dispose();
                Connection = null;
                
                EventStorage.Clear();
                LogLines = new string[] { };
            }
        }
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is ThreadNode))
                return false;
            var node = (ThreadNode)obj;
            return string.Compare(Host, node.Host) == 0 &&
                    string.Compare(Username, node.Username) == 0 && Port == node.Port;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_HOST), Host));
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_PORT), Port));
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_USERNAME), Username));
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_SESSION), Info.SessionId));
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_THREAD_APP_NAME), Info.ThreadName));
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_THREAD_APP_VER), Info.ThreadVersion));
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_APP_NAME), Info.AppName));
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_APP_VER), Info.AppVersion));
            return sb.ToString();
        }
    }
}
