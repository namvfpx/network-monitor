﻿using NetworkMonitor.ThreadMonitor.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.ThreadException
{
    public class CommandException : Exception
    {
        public ErrorEvent Event { get; private set; }
        public CommandException(ErrorEvent err) : base()
        {
            Event = err;
        } 
    }
}
