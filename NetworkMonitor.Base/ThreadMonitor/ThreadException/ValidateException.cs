﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.ThreadException
{
    public class ValidateException : Exception
    {
        public ValidateException(string message) : base(message)
        {

        }
        public ValidateException(string message, string source) : base(message)
        {

        }
    }
}
