﻿using NetworkMonitor.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class LogMonitorEvent : EventBase
    {
        public override EventType Type => EventType.LogMonitorEvent;
        public string ThreadId { get; set; } = "";
        public string ThreadStatus { get; set; } = "";
        public string LogResult { get; set; } = "";
    }
}
