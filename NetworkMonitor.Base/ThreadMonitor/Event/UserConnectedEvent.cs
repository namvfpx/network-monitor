﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Network;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class UserConnectedEvent : EventBase
    {
        public override EventType Type => EventType.UserConnectedEvent;

        public string Username { get; set; } = "";
        public DateTime StartTime { get; set; } = DateTime.Now;
        public string Host { get; set; } = "";
        public string Token { get; set; } = "";
    }
}
