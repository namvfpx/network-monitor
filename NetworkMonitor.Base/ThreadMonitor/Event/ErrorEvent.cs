﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Network;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class ErrorEvent : EventBase
    {
        public override EventType Type => EventType.Error;
        public string ErrorContext { get; set; } = "";
        public string ErrorDescription { get; set; } = "";
        public string ErrorInfo { get; set; } = "";
    }
}
