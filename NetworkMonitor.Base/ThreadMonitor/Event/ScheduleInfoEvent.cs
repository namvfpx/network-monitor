﻿using NetworkMonitor.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class ScheduleInfoEvent : EventBase
    {
        public override EventType Type => EventType.ScheduleInfo;
        public List<ScheduleSetting> Schedule { get; set; } = new List<ScheduleSetting>();
    }
}
