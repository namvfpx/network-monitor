﻿using NetworkMonitor.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class UnknowEvent : EventBase
    {
        public override EventType Type => EventType.UnknowEvent;
        public Exception Exception { get; set; }
    }
}
