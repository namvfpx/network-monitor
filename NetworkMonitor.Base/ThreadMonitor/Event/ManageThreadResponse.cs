﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Network;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class ManageThreadResponse : EventBase
    {
        public override EventType Type => EventType.ManagedThreadsLoad;
        public Dictionary<int, string> StartupType { get; set; } = new Dictionary<int, string>();
        public List<ThreadInfo> Threads { get; set; } = new List<ThreadInfo>();
    }
}
