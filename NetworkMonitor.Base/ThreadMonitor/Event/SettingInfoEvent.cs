﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Network;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class SettingInfoEvent : EventBase
    {
        public override EventType Type => EventType.SettingInfo;
        public Dictionary<int, string> StartupType { get; set; } = new Dictionary<int, string>();
        public string ThreadClassName { get; set; } = "";
        public int ThreadStartupType { get; set; } = 0;
        public List<ThreadParameter> Parameters { get; set; } = new List<ThreadParameter>();
    }
}
