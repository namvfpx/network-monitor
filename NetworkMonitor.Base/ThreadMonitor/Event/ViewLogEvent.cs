﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Network;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class ViewLogEvent : EventBase
    {
        public override EventType Type => EventType.LogInfo;
        public List<LogDir> Dirs { get; set; } = new List<LogDir>();
    }

    public class LogDir
    {
        public string Directory { get; set; } = "";
        public List<string> Files { get; set; } = new List<string>();
    }
}
