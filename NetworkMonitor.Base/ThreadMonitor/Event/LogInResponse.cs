﻿using NetworkMonitor.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class LogInResponse : EventBase
    {
        public bool IsPasswordExpired { get; set; } = false;
        public string AppName { get; set; } = "";
        public string ThreadName { get; set; } = "";
        public string AppVersion { get; set; } = "";
        public string ThreadVersion { get; set; } = "";
        public string SessionId { get; set; } = "";
        public string Log { get; set; } = "";

        public List<ThreadInfo> ThreadInfos { get; set; } = new List<ThreadInfo>();
        public override EventType Type => EventType.LoginResponse;

    }
}
