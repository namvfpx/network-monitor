﻿using NetworkMonitor.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public abstract class EventBase : INetworkEvent
    {
        public virtual string Protocol { get; set; } = "";
        public virtual DateTime Time { get; set; } = DateTime.Now;
        public virtual string Identifier { get; set; } = "";
        public virtual string OriginalContent { get; set; } = "";
        public virtual EventType Type => EventType.UnknowEvent;

        public virtual string Function { get; set; } = "";
        public virtual string ClassName { get; set; } = "";

        public bool IsOutDirection => false;

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Protocol);
            builder.Append(" | ID=" + Identifier);
            builder.Append(" | EventType=" + Type);
            builder.Append(" | Original=" + OriginalContent);
            return builder.ToString();
        }
    }
}
