﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Network;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class LogContentEvent : EventBase
    {
        public override EventType Type => EventType.LogContent;

        public string Content { get; set; } = "";
    }
}
