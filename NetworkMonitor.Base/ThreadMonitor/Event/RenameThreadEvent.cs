﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Network;

namespace NetworkMonitor.ThreadMonitor.Event
{
    public class RenameThreadEvent : EventBase
    {
        public override EventType Type => EventType.RenameThread;

        public string ThreadId { get; set; } = "";
        public string ThreadName { get; set; } = "";
    }
}
