﻿using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NetworkMonitor.ThreadMonitor
{
    public class ThreadInfo
    {
        public const string STATUS_RUNNING = "1";
        public const string STATUS_STOP = "2";

        public string Id { get; set; } = "";
        public string Name { get; set; } = "";
        public string Status { get; set; } = "";
        public string ClassName { get; set; } = "";
        public string[] LogLines { get; set; } = new string[] { };
        public ThreadNode ContainerNode { get; set; } = null;
        private bool isBusy = false;
        public bool IsBusy
        {
            get
            {
                lock (this)
                {
                    return isBusy;
                }
            }
            set
            {
                lock (this)
                {
                    isBusy = value;
                }
            }
        }
        public ThreadInfo(string id)
        {
            this.Id = id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is ThreadInfo))
                return false;
            var other = obj as ThreadInfo;
            return ((ContainerNode != null && ContainerNode.Equals(other.ContainerNode))
                || ContainerNode == null)
                && Id.Equals(other.Id);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_THREAD), Id));
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_NAME), Name));
            sb.Append(string.Format("{0}: {1}\r\n", MessageUtil.ToMessage(MessageUtil.KEY_STATUS), Status));
            return sb.ToString();
        }
    }
}
