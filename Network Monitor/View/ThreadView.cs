﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.Network;
using NetworkMonitor.Utils;
using NetworkMonitor.ThreadMonitor.ThreadException;
using NetworkMonitor.ThreadMonitor.Command;

namespace NetworkMonitor.View
{
    public partial class ThreadView : UserControl
    {
        IPacketFactory factory = new DDTProtocol.DDTPFactory();
        List<ThreadNode> nodes = new List<ThreadNode>();
        public Action<TreeNode> OnThreadSelect { get; set; }
        public Action<ThreadNode, Exception> OnDisconnectNode { get; set; }
        public Action<ThreadInfo> OnStartThread { get; set; } = (x) => { };
        public Action<ThreadInfo> OnStopThread { get; set; } = (x) => { };
        public Action<ThreadInfo> OnDestroyThread { get; set; } = (x) => { };
        public Action<ThreadNode, ThreadInfo, IPacket> OnMessage { get; set; }
        public ThreadView()
        {
            InitializeComponent();
            Localization.LocalizationControl(this);
            foreach (ToolStripItem item in cmsMain.Items)
            {
                item.Text = LanguageUtil.GetText(item.Text);
            }
            var imageList = new ImageList();
            imageList.ImageSize = new Size(16, 16);

            imageList.Images.Add("0", Properties.Resources.None);
            imageList.Images.Add("1", Properties.Resources.RunningThread);
            imageList.Images.Add("2", Properties.Resources.StoppedThread);
            imageList.Images.Add("3", Properties.Resources.ServerConnected);
            imageList.Images.Add("4", Properties.Resources.ServerDisconnected);
            tvThread.ImageList = imageList;
        }

        private bool AddNode(ThreadNode newNode, bool isReconnect)
        {
            var tNode = tvThread.Nodes.OfType<TreeNode>()
                            .FirstOrDefault(y => y.Tag != null && y.Tag.Equals(newNode));
            ThreadNode node = null;
            if (tNode != null)
            {
                node = (ThreadNode)tNode.Tag;
                node.Password = newNode.Password;
                node.Encrypt = newNode.Encrypt;
            }

            if (node == null)
            {
                node = newNode;
            }
            else if (!isReconnect)
            {
                NotificationBox.Show(MessageUtil.ToMessage(MessageUtil.KEY_NODE_ALREADY_EXISTED),
                    MessageUtil.ToMessage(MessageUtil.KEY_NODE_EXISTING));
            }

            if (!node.IsConnected)
            {
                node.OnSendingError = (x, y) =>
                {
                    ProcessError(x, y);
                };

                if (node.EstablishConnection(factory))
                {
                    node.OnMessage = (x, y) => ProcessMessage(x, y);
                    node.OnDisconnect = (x) =>
                    {
                        var treeNode = tvThread.Nodes.OfType<TreeNode>()
                                        .FirstOrDefault(y => y.Tag != null && y.Tag.Equals(node));
                        UpdateControlOnDisconnected(treeNode, node, x);
                    };
                    if (!nodes.Contains(node))
                        nodes.Add(node);

                    tvThread.BeginUpdate();
                    if (tNode == null)
                        tNode = tvThread.Nodes.Add(node.Name);
                    tNode.ImageKey = "3";
                    tNode.SelectedImageKey = "3";
                    tNode.Tag = node;
                    node.Info.ThreadInfos.ForEach(x => {
                        x.ContainerNode = node;
                        MountNode(tNode, x);
                    });
                    tvThread.EndUpdate();

                    tvThread.SelectedNode = tNode;
                    OnThreadSelect?.Invoke(tNode);
                    return true;
                }
                return false;
            }
            tvThread.SelectedNode = tNode;
            OnThreadSelect?.Invoke(tNode);
            return true;
        }
        protected void FormatNode(TreeNode node, bool isRunning)
        {
            if (tvThread.InvokeRequired)
            {
                try
                {
                    tvThread.Invoke(new Action(() => FormatNode(node, isRunning)));
                }
                catch { }
                return;
            }
            if (isRunning)
            {
                node.ForeColor = Color.DarkGreen;
                node.ImageKey = "1";
                node.SelectedImageKey = "1";
            }
            else
            {
                node.ForeColor = Color.Black;
                node.ImageKey = "2";
                node.SelectedImageKey = "2";
            }
        }

        private TreeNode MountNode(TreeNode parent, ThreadInfo info)
        {
            if (tvThread.InvokeRequired)
            {
                try
                {
                    return tvThread.Invoke(new Action(() => MountNode(parent, info))) as TreeNode;
                }
                catch {  }
                return null;
            }
            var child = parent.Nodes.Add(info.Name);
            child.Tag = info;
            FormatNode(child, info.Status.Equals("1"));

            return child;
        }

        private ThreadInfo UnmountNode(TreeNode parent, ThreadNode node, TreeNode child)
        {
            if (tvThread.InvokeRequired)
            {
                try
                {
                    return tvThread.Invoke(new Action(() => UnmountNode(parent, node, child))) as ThreadInfo;
                }
                catch { }
                return null;
            }

            if (child != null)
            {
                var info = child.Tag as ThreadInfo;
                node.Info.ThreadInfos.Remove(info);
                parent.Nodes.Remove(child);

                return info;
            }

            return null;
        }

        protected void ProcessMessage(ThreadNode node, IPacket pack)
        {
            ThreadInfo info = null;
            var treeNode = tvThread.Nodes.OfType<TreeNode>().FirstOrDefault(x => x.Tag != null && x.Tag.Equals(node));
            if (!pack.IsOutDirection)
            {
                var evt = pack as INetworkEvent;
                if (evt.Type == EventType.LogMonitorEvent)
                {
                    LogMonitorEvent threadEvent = (LogMonitorEvent)evt;
                    if (treeNode != null)
                    {
                        var child = treeNode.Nodes.OfType<TreeNode>().FirstOrDefault(x => x.Tag != null
                                    && x.Tag is ThreadInfo && ((ThreadInfo)x.Tag).Id == threadEvent.ThreadId);
                        if (child != null)
                        {
                            info = (ThreadInfo)child.Tag;
                            if (info != null)
                            {
                                if (!info.Status.Equals(threadEvent.ThreadStatus))
                                    FormatNode(child, threadEvent.ThreadStatus.Equals(ThreadInfo.STATUS_RUNNING));
                                lock (info)
                                {
                                    info.Status = threadEvent.ThreadStatus;
                                }
                            }
                        }
                    }
                }
                else if (evt.Type == EventType.UnloadThreadEvent)
                {
                    var unloadThread = evt as UnloadThreadEvent;
                    if (treeNode != null)
                    {
                        var child = treeNode.Nodes.OfType<TreeNode>().FirstOrDefault(x => x.Tag != null
                                    && x.Tag is ThreadInfo && ((ThreadInfo)x.Tag).Id == unloadThread.ThreadId);
                        UnmountNode(treeNode, node, child);
                    }
                }
                else if (evt.Type == EventType.LoadThreadEvent)
                {
                    var loadThread = evt as LoadThreadEvent;
                    if (treeNode != null)
                    {
                        info = new ThreadInfo(loadThread.ThreadId)
                        {
                            Name = loadThread.ThreadName,
                            Status = loadThread.ThreadStatus,
                        };
                        info.ContainerNode = node;
                        MountNode(treeNode, info);
                    }
                }
                else if (evt.Type == EventType.RenameThread)
                {
                    var renameThread = evt as RenameThreadEvent;
                    if (treeNode != null)
                    {
                        var child = treeNode.Nodes.OfType<TreeNode>().FirstOrDefault(x => x.Tag != null
                                    && x.Tag is ThreadInfo && ((ThreadInfo)x.Tag).Id == renameThread.ThreadId);
                        if (tvThread.InvokeRequired)
                        {
                            tvThread.Invoke(new Action(() => { child.Text = renameThread.ThreadName; }));
                        }
                        else
                        {
                            child.Text = renameThread.ThreadName;
                        }
                    }
                }
            }
            OnMessage?.Invoke(node, info, pack);
        }

        public void DisposeAllNodes()
        {
            nodes.ForEach(x => x.Dispose());
        }

        private void UpdateControlOnDisconnected(TreeNode node, ThreadNode threadNode, Exception e)
        {
            if (tvThread.InvokeRequired)
            {
                tvThread.Invoke(new Action(() => UpdateControlOnDisconnected(node, threadNode, e)));
                return;
            }

            OnDisconnectNode?.Invoke(threadNode, e);

            if (node != null)
            {
                tvThread.BeginUpdate();
                node.ImageKey = "4";
                node.SelectedImageKey = "4";
                node.Nodes.Clear();
                tvThread.SelectedNode = node;
                tvThread.EndUpdate();
            }
        }

        public void UserDisconnectNode(ThreadNode node)
        {

            node.Dispose();
            var treeNode = tvThread.Nodes.OfType<TreeNode>()
                          .FirstOrDefault(y => y.Tag != null && y.Tag.Equals(node));
            UpdateControlOnDisconnected(treeNode, node, null);
        }

        public void UserStartThread(TreeNode node)
        {
            var info = node.Tag as ThreadInfo;
            if (node.Parent != null && node.Parent.Tag != null && node.Parent.Tag is ThreadNode)
            {
                ThreadNode threadNode = (ThreadNode)node.Parent.Tag;
                threadNode.StartThread(info);
            }
        }
        public void UserRestartThread(TreeNode node)
        {
            var info = node.Tag as ThreadInfo;
            if (node.Parent != null && node.Parent.Tag != null && node.Parent.Tag is ThreadNode)
            {
                ThreadNode threadNode = (ThreadNode)node.Parent.Tag;
                threadNode.RestartThread(info);
            }
        }
        public void UserStartThreadImmediately(TreeNode node)
        {
            var info = node.Tag as ThreadInfo;
            if (node.Parent != null && node.Parent.Tag != null && node.Parent.Tag is ThreadNode)
            {
                ThreadNode threadNode = (ThreadNode)node.Parent.Tag;
                threadNode.StartImmediate(info);
            }
        }
        public void UserStopThread(TreeNode node)
        {
            var info = node.Tag as ThreadInfo;
            if (node.Parent != null && node.Parent.Tag != null && node.Parent.Tag is ThreadNode)
            {
                ThreadNode threadNode = (ThreadNode)node.Parent.Tag;
                threadNode.StopThread(info);
            }
        }

        public void UserDestroyThread(TreeNode node)
        {
            var info = node.Tag as ThreadInfo;
            if (node.Parent != null && node.Parent.Tag != null && node.Parent.Tag is ThreadNode)
            {
                if (NotificationBox.Show(MessageUtil.ToMessage(MessageUtil.KEY_DESTROY_THREAD_CONFIRM, info.Name),
                    MessageUtil.ToMessage(MessageUtil.KEY_DESTROY), MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ThreadNode threadNode = (ThreadNode)node.Parent.Tag;
                    threadNode.DestroyTrhead(info);
                }
            }
        }
        public void UserGetThreadSetting(TreeNode node)
        {
            var info = node.Tag as ThreadInfo;
            if (node.Parent != null && node.Parent.Tag != null && node.Parent.Tag is ThreadNode)
            {
                ThreadNode threadNode = (ThreadNode)node.Parent.Tag;
                try
                {
                    var evt = threadNode.GetThreadSetting(info);
                    if (evt != null)
                    {
                        SettingView pView = new SettingView(info, (SettingInfoEvent)evt);
                        if (pView.ShowDialog() == DialogResult.OK)
                        {
                            threadNode.StoreSetting(info, pView.ThreadName, pView.ClassName,
                                pView.StartupType, pView.Parameters);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ProcessError(threadNode, ex);
                }
            }
        }
        public void UserGetThreadSchedule(TreeNode node)
        {
            var info = node.Tag as ThreadInfo;
            if (node.Parent != null && node.Parent.Tag != null && node.Parent.Tag is ThreadNode)
            {
                ThreadNode threadNode = (ThreadNode)node.Parent.Tag;
                try
                {
                    var evt = threadNode.GetThreadSchedule(info);
                    if (evt != null)
                    {
                        ScheduleManagerView pView = new ScheduleManagerView(threadNode, info, 
                            ((ScheduleInfoEvent)evt).Schedule);
                        pView.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    ProcessError(threadNode, ex);
                }
            }
        }
        public void UserGetThreadLog(TreeNode node)
        {
            var info = node.Tag as ThreadInfo;
            if (node.Parent != null && node.Parent.Tag != null && node.Parent.Tag is ThreadNode)
            {
                ThreadNode threadNode = null;
                try
                { 
                    threadNode = (ThreadNode)node.Parent.Tag;
                    var evt = threadNode.GetThreadLog(info);
                    if (evt != null)
                    {
                        LogView lv = new LogView(threadNode, info, evt as ViewLogEvent);
                        lv.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    ProcessError(threadNode, ex);
                }
            }
        }

        public void UserConnectNode(ThreadNode node, bool isAutologIn, bool isReconnect = false)
        {
            NodeLoginView login = new NodeLoginView(node, isAutologIn);

            if (login.ShowDialog() == DialogResult.OK)
            {
                var newNode = new ThreadNode(login.Host, login.Port)
                {
                    Username = login.Username,
                    Password = login.Password,
                    Encrypt = login.Encrypt
                };
                bool success = false;
                try
                {
                    success = AddNode(newNode, isReconnect);
                }
                catch (Exception e)
                {
                    NotificationBox.Show(MessageUtil.ToMessage(MessageUtil.KEY_NODE_CONNECT_ERROR, newNode.Name, e.Message));
                }
                if (!success)
                {
                    UserConnectNode(node, false, isReconnect);
                }

            }
        }

        public void UserRemoveNode(TreeNode node)
        {
            var threadNode = (ThreadNode)node.Tag;
            if (threadNode.IsConnected)
            {
                UserDisconnectNode(threadNode);
            }
            nodes.Remove(threadNode);
            tvThread.Nodes.Remove(node);
            tvThread.SelectedNode = null;
            OnThreadSelect?.Invoke(null);
        }
        public void UserChangePassword(TreeNode node)
        {
            ThreadNode threadNode = null;
            if (node.Tag is ThreadNode)
            {
                threadNode = node.Tag as ThreadNode;
            }
            else if (node.Parent != null && node.Parent.Tag is ThreadNode)
            {
                threadNode = node.Parent.Tag as ThreadNode;
            }

            NodePasswordView changePass = new NodePasswordView(threadNode);
            changePass.ShowDialog();
        }

        public void UserManageThread(TreeNode node)
        {
            ThreadNode threadNode = null;
            if (node.Tag is ThreadNode)
            {
                threadNode = node.Tag as ThreadNode;
            }
            else if (node.Parent != null && node.Parent.Tag is ThreadNode)
            {
                threadNode = node.Parent.Tag as ThreadNode;
            }

            if (threadNode != null)
            {
                try
                {
                    var evt = threadNode.SendCommand(new ManageThreadCommand());
                    if (evt != null)
                    {
                        ThreadManagerView tmView = new ThreadManagerView(threadNode, evt as ManageThreadResponse);
                        tmView.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    ProcessError(threadNode, ex);
                }
            }
        }

        private void ProcessError(ThreadNode node, Exception ex)
        {
            NotificationBox.Show(MessageUtil.ToMessage(ex), node.Name);
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadNode)
            {
                UserDisconnectNode((ThreadNode)node.Tag);
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadInfo)
            {
                UserStartThread(node);
            }
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadInfo)
            {
                UserStopThread(node);
            }
        }

        private void destroyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadInfo)
            {
                UserDestroyThread(node);
            }
        }
        private void parametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadInfo)
            {
                UserGetThreadSetting(node);
            }
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadNode)
            {
                UserRemoveNode(node);
            }
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadNode)
            {
                UserConnectNode((ThreadNode)node.Tag, true);
            }
        }

        private void addnodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserConnectNode(null, false);
        }

        private void scheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadInfo)
            {
                UserGetThreadSchedule(node);
            }
        }

        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadInfo)
            {
                UserGetThreadLog(node);
            }
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadInfo)
            {
                UserRestartThread(node);
            }
        }

        private void immediateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadInfo)
            {
                UserStartThreadImmediately(node);
            }
        }

        private void tvThread_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            tvThread.SelectedNode = e.Node;
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null && node.Tag is ThreadInfo)
            {
                UserGetThreadSetting(node);
            }
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null && node.Tag != null)
            {
                if (node.Tag is ThreadInfo)
                {
                    var info = node.Tag as ThreadInfo;
                    NotificationBox.Show(info.ToString(), info.Name);
                }
                else if (node.Tag is ThreadNode)
                {
                    var info = node.Tag as ThreadNode;
                    NotificationBox.Show(info.ToString(), info.Name);
                }
            }
        }

        private void tvThread_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            tvThread.SelectedNode = e.Node;

            if (e.Node == null || e.Node.Tag == null)
                return;
            OnThreadSelect?.Invoke(e.Node);
        }

        private void manageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null)
            {
                UserManageThread(node);
            }
        }

        private void changepasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = tvThread.SelectedNode;
            if (node != null)
            {
                UserChangePassword(node);
            }
        }

        private void cmsMain_Opening(object sender, CancelEventArgs e)
        {
            startToolStripMenuItem.Visible = true;
            restartToolStripMenuItem.Visible = true;
            stopToolStripMenuItem.Visible = true;
            destroyToolStripMenuItem.Visible = true;
            parametersToolStripMenuItem.Visible = true;
            scheduleToolStripMenuItem.Visible = true;
            logToolStripMenuItem.Visible = true;
            immediateToolStripMenuItem.Visible = true;
            disconnectToolStripMenuItem.Visible = true;
            removeToolStripMenuItem.Visible = true;
            reconnectToolStripMenuItem.Visible = true;
            infoToolStripMenuItem.Visible = true;
            manageToolStripMenuItem.Visible = true;
            changepasswordToolStripMenuItem.Visible = true;

            var node = tvThread.SelectedNode;
            if (node == null)
            {
                startToolStripMenuItem.Visible = false;
                restartToolStripMenuItem.Visible = false;
                stopToolStripMenuItem.Visible = false;
                destroyToolStripMenuItem.Visible = false;
                parametersToolStripMenuItem.Visible = false;
                scheduleToolStripMenuItem.Visible = false;
                logToolStripMenuItem.Visible = false;
                immediateToolStripMenuItem.Visible = false;
                disconnectToolStripMenuItem.Visible = false;
                infoToolStripMenuItem.Visible = false;
                removeToolStripMenuItem.Visible = false;
                reconnectToolStripMenuItem.Visible = false;
                manageToolStripMenuItem.Visible = false;
                changepasswordToolStripMenuItem.Visible = false;
            }
            else if (node.Tag != null && node.Tag is ThreadNode)
            {
                startToolStripMenuItem.Visible = false;
                restartToolStripMenuItem.Visible = false;
                stopToolStripMenuItem.Visible = false;
                destroyToolStripMenuItem.Visible = false;
                parametersToolStripMenuItem.Visible = false;
                scheduleToolStripMenuItem.Visible = false;
                logToolStripMenuItem.Visible = false;
                immediateToolStripMenuItem.Visible = false;
                if (!((ThreadNode)node.Tag).IsConnected)
                {
                    disconnectToolStripMenuItem.Visible = false;
                    changepasswordToolStripMenuItem.Visible = false;
                    manageToolStripMenuItem.Visible = false;
                }
                else
                {
                    removeToolStripMenuItem.Visible = false;
                    reconnectToolStripMenuItem.Visible = false;
                }
            }
            else if (node.Tag != null && node.Tag is ThreadInfo)
            {
                disconnectToolStripMenuItem.Visible = false;
                reconnectToolStripMenuItem.Visible = false;
                removeToolStripMenuItem.Visible = false;
                changepasswordToolStripMenuItem.Visible = false;

                if (((ThreadInfo)node.Tag).Status.Equals(ThreadInfo.STATUS_RUNNING))
                {
                    startToolStripMenuItem.Visible = false;
                }
                else
                {
                    restartToolStripMenuItem.Visible = false;
                    stopToolStripMenuItem.Visible = false;
                    destroyToolStripMenuItem.Visible = false;
                }
            }
        }
    }
}
