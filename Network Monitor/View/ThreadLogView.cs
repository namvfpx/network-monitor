﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.Utils;
using NetworkMonitor.ThreadMonitor;

namespace NetworkMonitor.View
{
    public partial class ThreadLogView : UserControl
    {
        public object Node { get; private set; }
        public ThreadLogView()
        {
            InitializeComponent();
            Localization.LocalizationControl(this);
        }

        public void ClearLog()
        {
            if (txtLog.InvokeRequired)
            {
                txtLog.Invoke(new Action(ClearLog));
                return;
            }
            else
            {
                if (!txtLog.IsDisposed)
                {
                    txtLog.Text = "";
                }
            }
        }

        public void AppendLog(string[] message)
        {
            if (txtLog.InvokeRequired)
            {
                try
                {
                    txtLog.Invoke(new Action(() => AppendLog(message)));
                }
                catch { }
                return;
            }
            else
            {
                if (!txtLog.IsDisposed)
                {
                    txtLog.Lines = txtLog.Lines.Concat(message).ToArray();
              
                    if (txtLog.Lines.Length > Config.MaxLogLinePerThread)
                    {
                        txtLog.Lines = txtLog.Lines.Skip(txtLog.Lines.Length - Config.MaxLogLinePerThread).ToArray();
                    }
                    txtLog.SelectionStart = txtLog.Text.Length;
                    txtLog.ScrollToCaret();
                }
            }
        }

        public void UpdateView(object node)
        {
            ClearLog();
            if (node is ThreadInfo)
            {
                Node = node;
                lock(node)
                    AppendLog(((ThreadInfo)node).LogLines);
            }
            else if (node is ThreadNode)
            {
                Node = node;
                var logLines = ((ThreadNode)node).LogLines;
                lock (logLines)
                    AppendLog(logLines);
            }
        }
    }
}
