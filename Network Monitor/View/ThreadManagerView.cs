﻿using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.ThreadMonitor.Command;
using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class ThreadManagerView : Form
    {
        private ThreadNode Node { get; set; }
        private bool UpdateDataBack { get; set; } = false;
        private List<ThreadInfo> UpdatedList { get; set; } = new List<ThreadInfo>();
        public ThreadManagerView(ThreadNode node, ManageThreadResponse response)
        {
            InitializeComponent();
            Localization.LocalizationControl(this);
            StartPosition = FormStartPosition.CenterParent;

            Node = node;
            dgvThreads.AutoGenerateColumns = false;
            dgvThreads.Columns.Add(new DataGridViewTextBoxColumn() {
                HeaderText = LanguageUtil.GetText(MessageUtil.KEY_ID),
                AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells,
                ReadOnly = true,
                DataPropertyName = "Id"
            });
            dgvThreads.Columns.Add(new DataGridViewTextBoxColumn()
            {
                HeaderText = LanguageUtil.GetText(MessageUtil.KEY_NAME),
                AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells,
                DataPropertyName = "Name"
            });
            dgvThreads.Columns.Add(new DataGridViewTextBoxColumn()
            {
                HeaderText = LanguageUtil.GetText(MessageUtil.KEY_CLASS_NAME),
                AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells,
                DataPropertyName = "ClassName"
            });
            dgvThreads.Columns.Add(new DataGridViewComboBoxColumn()
            {
                HeaderText = LanguageUtil.GetText(MessageUtil.KEY_STARTUP_TYPE),
                AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells,
                DataSource = response.StartupType.Select(y => new NameValuePair<string, string>()
                {
                    Name = y.Value,
                    Value = Convert.ToString(y.Key)
                }).ToList(),
                DisplayMember = "Name",
                ValueMember = "Value",
                ValueType = typeof(string),
                DataPropertyName = "Status"
            });

            dgvThreads.DataSource = new BindingList<ThreadInfo>(response.Threads);
            //response.Threads.ForEach(x => {
            //    var idx = dgvThreads.Rows.Add(x.Id, x.Name, x.ClassName, x.Status);
            //    var row = dgvThreads.Rows[idx];
            //    row.Tag = x;
            //    row.Cells[0].Value = x.Id;
            //    row.Cells[1].Value = x.Name;
            //    row.Cells[2].Value = x.ClassName;
            //    row.Cells[3].Value = Convert.ToInt32(x.Status);
            //});

            UpdateDataBack = true;
        }

        private void dgvThreads_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var row = dgvThreads.Rows[e.RowIndex];
            var cell = row.Cells[e.ColumnIndex];

            var info = row.DataBoundItem as ThreadInfo;

            if (!UpdatedList.Contains(info))
            {
                UpdatedList.Add(info);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (UpdatedList.Count == 0)
            {
                this.Close();
                return;
            }
            if (NotificationBox.Show(MessageUtil.ToMessage(MessageUtil.KEY_SAVE_SETTING_CONFIRM), 
                MessageUtil.ToMessage(MessageUtil.KEY_SAVE_SETTING), MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    UpdatedList.ForEach(x =>
                    {
                        Node.SendCommand(new StoreThreadCommand(x.Id)
                        {
                            Name = x.Name,
                            ClassName = x.ClassName,
                            Status = x.Status
                        });
                    });

                    this.Close();
                }
                catch (Exception ex)
                {
                    NotificationBox.Show(MessageUtil.ToMessage(ex));
                }
            }
        }

        private void ThreadManagerView_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
