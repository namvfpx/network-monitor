﻿using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class ProfilePasswordView : Form
    {
        public string Password { get; set; } = "";
        public ProfilePasswordView()
        {
            InitializeComponent();
            Localization.LocalizationControl(this);
            ShowIcon = false;
            StartPosition = FormStartPosition.CenterParent;
            DialogResult = DialogResult.Cancel;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                NotificationBox.Show(LanguageUtil.GetText(MessageUtil.KEY_INVALID_INPUT));
                return;
            }
            if (!txtPassword.Text.Equals(txtConfirmPassword.Text))
            {
                NotificationBox.Show(LanguageUtil.GetText(MessageUtil.KEY_PASSWORD_NOT_MATCH));
                return;
            }

            Password = txtPassword.Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void txt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                btnOK_Click(sender, e);
            }
        }
    }
}
