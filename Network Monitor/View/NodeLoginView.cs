﻿using NetworkMonitor.Profile;
using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class NodeLoginView : Form
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Encrypt { get; set; }
        public string Password { get; set; }
        public short Port { get; set; }
        private bool IsAutoLogin { get; set; } = false;
        public NodeLoginView(ThreadNode node, bool isAutoLogin)
        {
            InitializeComponent();
            ShowIcon = false;
            StartPosition = FormStartPosition.CenterParent;
            Localization.LocalizationControl(this);
            if (Properties.Settings.Default.Host == null)
                Properties.Settings.Default.Host = new System.Collections.Specialized.StringCollection();
            if (Properties.Settings.Default.Port == null)
                Properties.Settings.Default.Port = new System.Collections.Specialized.StringCollection();

            if (node != null && !Properties.Settings.Default.Host.Contains(node.Host))
            {
                Properties.Settings.Default.Host.Add(node.Host);
            }
            if (node != null && !Properties.Settings.Default.Port.Contains(Convert.ToString(node.Port)))
            {
                Properties.Settings.Default.Port.Add(Convert.ToString(node.Port));
            }
            txtUsername.Text = Properties.Settings.Default.Username;
            cmbHost.DataSource = Properties.Settings.Default.Host;
            cmbPort.DataSource = Properties.Settings.Default.Port;
            cmbEncrypt.DisplayMember = "Name";
            cmbEncrypt.ValueMember = "Value";
            cmbEncrypt.DataSource = EncryptUtil.Algorithms;
            
            if (node != null)
            {
                IsAutoLogin = isAutoLogin;

                txtUsername.Text = node.Username;
                cmbHost.Text = node.Host;
                cmbPort.Text = Convert.ToString(node.Port);
                txtPassword.Text = node.Password;
                cmbEncrypt.SelectedValue = node.Encrypt;
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string username = txtUsername.Text;
                string password = txtPassword.Text;
                string encrypt = Convert.ToString(cmbEncrypt.SelectedValue);
                if (!string.IsNullOrEmpty(encrypt))
                {
                    password = EncryptUtil.EncryptPassword(password, encrypt);
                }
                string host = cmbHost.Text;
                short port = 0;
                try
                {
                    port = Convert.ToInt16(cmbPort.Text);
                }
                catch
                {
                    throw new Exception(MessageUtil.KEY_INVALID_FIELD);
                }

                if (Properties.Settings.Default.Host.Contains(host))
                    Properties.Settings.Default.Host.Remove(host);
                if (Properties.Settings.Default.Host.Count > 0)
                    Properties.Settings.Default.Host.Insert(0, host);
                else
                    Properties.Settings.Default.Host.Add(host);

                if (Properties.Settings.Default.Port.Contains(port.ToString()))
                    Properties.Settings.Default.Port.Remove(port.ToString());
                if (Properties.Settings.Default.Port.Count > 0)
                    Properties.Settings.Default.Port.Insert(0, port.ToString());
                else
                    Properties.Settings.Default.Port.Add(port.ToString());
                //Properties.Settings.Default.Port = port.ToString();
                Properties.Settings.Default.Username = username;
                Properties.Settings.Default.Save();

                Encrypt = encrypt;
                Username = username;
                Password = password;
                Host = host;
                Port = port;
                DialogResult = DialogResult.OK;

                if (Global.CurrentProfile != null && !IsAutoLogin)
                {

                    if (NotificationBox.Show(MessageUtil.ToMessage(MessageUtil.KEY_SAVE_LOGIN_INFO), "",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        var nodeInfo = Global.CurrentProfile.StoredNodes.Where(x => x.Host.Equals(Host)
                        && x.Port == Port && x.Username.Equals(Username)).FirstOrDefault();
                        if (nodeInfo == null)
                        {
                            nodeInfo = new Profile.StoredNode()
                            {
                                Host = Host,
                                Username = Username,
                                Password = Password,
                                Encrypt = Encrypt,
                                Port = Port
                            };
                            Global.CurrentProfile.StoredNodes.Add(nodeInfo);
                        }
                        else
                        {
                            nodeInfo.Password = Password;
                            nodeInfo.Encrypt = Encrypt;
                        }
                        LocalProfile.Store(Global.CurrentProfile);
                    }
                }

                this.Close();
            }
            catch (Exception ex)
            {
                NotificationBox.Show(MessageUtil.ToMessage(ex));
                DialogResult = DialogResult.Abort;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void LoginForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                btnLogin_Click(sender, e);
            }
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            if (IsAutoLogin)
            {
                btnLogin_Click(null, e);
            }
        }
    }
}
