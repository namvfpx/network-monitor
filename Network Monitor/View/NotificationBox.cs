﻿using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class NotificationBox : Form
    {
        private const int MAX_WIDTH = 750;
        private const int MAX_HEIGHT = 600;
        private const int MIN_WIDTH = 400;
        private const int MIN_HEIGHT = 100;

        List<Button> btnList = new List<Button>();
        private NotificationBox()
        {
            InitializeComponent();
        }

        private NotificationBox(string title, string content, MessageBoxButtons buttons)
        {
            InitializeComponent();
            ShowIcon = false;
            StartPosition = FormStartPosition.CenterParent;
            this.Text = title;
            Size size = TextRenderer.MeasureText(content, txtContent.Font);
            if (size.Width > MAX_WIDTH)
            {
                size.Width = MAX_WIDTH;
            }
            if (size.Height > MAX_HEIGHT)
            {
                size.Height = MAX_HEIGHT;
            }
            txtContent.MaximumSize = new Size(MAX_WIDTH, MAX_HEIGHT);
            txtContent.MinimumSize = new Size(MIN_WIDTH, MIN_HEIGHT);
            txtContent.Size = size;
            txtContent.Text = content;
            txtContent.TabIndex = 10;
            Height = txtContent.Height + 100;
            Width = txtContent.Width + 30;

            if (buttons == MessageBoxButtons.OKCancel)
            {
                Button btnOK = new Button();
                btnOK.Text = LanguageUtil.GetText(MessageUtil.KEY_OK);
                btnOK.Click += BtnOK_Click;
                btnList.Add(btnOK);

                Button btnCancel = new Button();
                btnCancel.Text = LanguageUtil.GetText(MessageUtil.KEY_CANCEL);
                btnCancel.Click += BtnCancel_Click;
                btnList.Add(btnCancel);
            }
            else if (buttons == MessageBoxButtons.AbortRetryIgnore)
            {
                Button btnAbort = new Button();
                btnAbort.Text = LanguageUtil.GetText(MessageUtil.KEY_ABORT);
                btnAbort.Click += BtnAbort_Click; ;
                btnList.Add(btnAbort);

                Button btnRetry = new Button();
                btnRetry.Text = LanguageUtil.GetText(MessageUtil.KEY_RETRY);
                btnRetry.Click += BtnRetry_Click; ;
                btnList.Add(btnRetry);

                Button btnIgnore = new Button();
                btnIgnore.Text = LanguageUtil.GetText(MessageUtil.KEY_IGNORE);
                btnIgnore.Click += BtnIgnore_Click;
                btnList.Add(btnIgnore);
            }
            else if (buttons == MessageBoxButtons.OK)
            {
                Button btnOK = new Button();
                btnOK.Text = LanguageUtil.GetText(MessageUtil.KEY_OK);
                btnOK.Click += BtnOK_Click;
                btnList.Add(btnOK);
            }
            else if (buttons == MessageBoxButtons.RetryCancel)
            {
                Button btnRetry = new Button();
                btnRetry.Text = LanguageUtil.GetText(MessageUtil.KEY_RETRY);
                btnRetry.Click += BtnRetry_Click; ;
                btnList.Add(btnRetry);
                Button btnCancel = new Button();
                btnCancel.Text = LanguageUtil.GetText(MessageUtil.KEY_CANCEL);
                btnCancel.Click += BtnCancel_Click;
                btnList.Add(btnCancel);
            }
            else if (buttons == MessageBoxButtons.YesNo)
            {
                Button btnYes = new Button();
                btnYes.Text = LanguageUtil.GetText(MessageUtil.KEY_YES);
                btnYes.Click += BtnYes_Click;
                btnList.Add(btnYes);
                Button btnNo = new Button();
                btnNo.Text = LanguageUtil.GetText(MessageUtil.KEY_NO);
                btnNo.Click += BtnNo_Click;
                btnList.Add(btnNo);
            }
            else if (buttons == MessageBoxButtons.YesNoCancel)
            {
                Button btnYes = new Button();
                btnYes.Text = LanguageUtil.GetText(MessageUtil.KEY_YES);
                btnYes.Click += BtnYes_Click;
                btnList.Add(btnYes);
                Button btnNo = new Button();
                btnNo.Text = LanguageUtil.GetText(MessageUtil.KEY_NO);
                btnNo.Click += BtnNo_Click;
                btnList.Add(btnNo);
                Button btnCancel = new Button();
                btnCancel.Text = LanguageUtil.GetText(MessageUtil.KEY_CANCEL);
                btnCancel.Click += BtnCancel_Click;
                btnList.Add(btnCancel);
            }

            btnList.Reverse();
            ArrangeButton();
            DialogResult = DialogResult.Cancel;
        }

        private void BtnNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void BtnYes_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void BtnIgnore_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Ignore;
            this.Close();
        }

        private void BtnRetry_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Retry;
            this.Close();
        }

        private void BtnAbort_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ArrangeButton()
        {
            int maxX = ClientRectangle.Width - 10;
            int maxY = ClientRectangle.Height - 10;
            int i = 0;
            btnList.ForEach(x => {
                try
                {
                    Controls.Remove(x);
                }
                catch { }
                x.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
                x.Size = new Size(75, 25);
                x.Left = maxX - x.Size.Width;
                x.Top = maxY - x.Size.Height;
                maxX = maxX - x.Size.Width - 5;
                Controls.Add(x);
                x.TabIndex = i++;
            });
            if (btnList.Count > 0)
            {
                btnList[0].Focus();
            }
        }
        
        public static DialogResult Show(string content, string title = "", 
            MessageBoxButtons buttons = MessageBoxButtons.OK)
        {
            var box = new NotificationBox(title, content, buttons);
            return box.ShowDialog();
        }

        private void NotificationBox_AutoSizeChanged(object sender, EventArgs e)
        {
            ArrangeButton();
        }

        private void NotificationBox_MouseHover(object sender, EventArgs e)
        {
        }
    }
}
