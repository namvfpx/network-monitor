﻿using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class ScheduleManagerView : Form
    {
        private ThreadNode Node { get; set; }
        private ThreadInfo Info { get; set; }
        private BindingList<ScheduleSetting> Settings { get; set; }
        private ScheduleSetting SelectedSetting { get; set; }
        public ScheduleManagerView(ThreadNode node, ThreadInfo info, List<ScheduleSetting> schedule)
        {
            InitializeComponent();
            ShowIcon = false;
            Localization.LocalizationControl(this);
            StartPosition = FormStartPosition.CenterParent;


            foreach (ToolStripItem item in cmsManage.Items)
            {
                item.Text = LanguageUtil.GetText(item.Text);
            }

            cmbType.DisplayMember = "Name";
            cmbType.ValueMember = "Value";
            cmbType.DataSource = new List<NameValuePair<string, ScheduleType>>() {
                new NameValuePair<string, ScheduleType>{ Name = LanguageUtil.GetText(
                    ScheduleSetting.ScheduleTypeText[ScheduleType.Daily]),
                        Value = ScheduleType.Daily },
                new NameValuePair<string, ScheduleType>{ Name = LanguageUtil.GetText(
                    ScheduleSetting.ScheduleTypeText[ScheduleType.Weekly]),
                        Value = ScheduleType.Weekly },
                new NameValuePair<string, ScheduleType>{ Name = LanguageUtil.GetText(
                    ScheduleSetting.ScheduleTypeText[ScheduleType.Monthly]),
                        Value = ScheduleType.Monthly },
                new NameValuePair<string, ScheduleType>{ Name = LanguageUtil.GetText(
                    ScheduleSetting.ScheduleTypeText[ScheduleType.Yearly]),
                        Value = ScheduleType.Yearly },

            };

            var weekDays = new List<NameValuePair<string, int>>()
            {
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_DAY_SUN), Value = 1},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_DAY_MON), Value = 2},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_DAY_TUE), Value = 3},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_DAY_WED), Value = 4},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_DAY_THU), Value = 5},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_DAY_FRI), Value = 6},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_DAY_SAT), Value = 7}
            };
            var yearMonths = new List<NameValuePair<string, int>>()
            {
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_1), Value = 0},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_2), Value = 1},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_3), Value = 2},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_4), Value = 3},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_5), Value = 4},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_6), Value = 5},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_7), Value = 6},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_8), Value = 7},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_9), Value = 8},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_10), Value = 9},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_11), Value = 10},
                new NameValuePair<string, int>{ Name = LanguageUtil.GetText(MessageUtil.KEY_MONTH_12), Value = 11},
            };
            var monthDays = new List<NameValuePair<string, int>>();
            for (int i = 1; i <= 31; i++)
            {
                monthDays.Add(new NameValuePair<string, int> { Name = i.ToString(), Value = i });
            }

            clbYearMonth.DataSource = yearMonths;
            clbYearMonth.DisplayMember = "Name";
            clbYearMonth.ValueMember = "Value";

            clbWeekDay.DataSource = weekDays;
            clbWeekDay.DisplayMember = "Name";
            clbWeekDay.ValueMember = "Value";

            clbMonthDay.DataSource = monthDays;
            clbMonthDay.DisplayMember = "Name";
            clbMonthDay.ValueMember = "Value";

            Node = node;
            Info = info;
            pnDetail.Enabled = false;
            Settings = new BindingList<ScheduleSetting>(schedule);
            lstSetting.DisplayMember = "Name";
            lstSetting.DataSource = Settings;
            DialogResult = DialogResult.Cancel;
        }

        private void PrepareItem()
        {
            pnDetail.Enabled = false;
            btnEdit.Text = LanguageUtil.GetText(MessageUtil.KEY_EDIT);
            btnCancel.Visible = false;
            if (SelectedSetting != null)
            {
                gbDetail.Enabled = true;
                cmbType.SelectedValue = SelectedSetting.Type;
                dtpExpectedDate.Value = SelectedSetting.ExpectDate;
                nudCycle.Value = SelectedSetting.Cycle;
                mtbStartTime.Text = SelectedSetting.StartTime;
                mtbEndTime.Text = SelectedSetting.EndTime;
                nudTimes.Value = SelectedSetting.Times;
                for (int i = 0; i < clbWeekDay.Items.Count; i++)
                {
                    clbWeekDay.SetItemChecked(i, false);
                    if (clbWeekDay.Items[i] is NameValuePair<string, int>)
                    {
                        if (SelectedSetting.WeekDays.Contains((clbWeekDay.Items[i] as NameValuePair<string, int>).Value))
                            clbWeekDay.SetItemChecked(i, true);
                    }
                    if (!clbWeekDay.GetItemChecked(i))
                    {
                        cbWeekDay.Checked = false;
                    }
                }
                for (int i = 0; i < clbMonthDay.Items.Count; i++)
                {
                    clbMonthDay.SetItemChecked(i, false);
                    if (clbMonthDay.Items[i] is NameValuePair<string, int>)
                    {
                        if (SelectedSetting.MonthDays.Contains((clbMonthDay.Items[i] as NameValuePair<string, int>).Value))
                            clbMonthDay.SetItemChecked(i, true);
                    }
                    if (!clbMonthDay.GetItemChecked(i))
                    {
                        cbMonthDay.Checked = false;
                    }
                }
                for (int i = 0; i < clbYearMonth.Items.Count; i++)
                {
                    clbYearMonth.SetItemChecked(i, false);
                    if (clbYearMonth.Items[i] is NameValuePair<string, int>)
                    {
                        if (SelectedSetting.YearMonths.Contains((clbYearMonth.Items[i] as NameValuePair<string, int>).Value))
                            clbYearMonth.SetItemChecked(i, true);
                    }
                    if (!clbYearMonth.GetItemChecked(i))
                    {
                        cbYearMonth.Checked = false;
                    }
                }
                btnEdit.Visible = true;
            }
            else
            {
                cmbType.SelectedValue = ScheduleType.Daily;
                dtpExpectedDate.Value = DateTime.Now;
                nudCycle.Value = 1;
                mtbStartTime.Text = "00:00:00";
                mtbEndTime.Text = "00:00:00";
                nudTimes.Value = 1;
                cbMonthDay.Checked = false;
                cbWeekDay.Checked = false;
                cbYearMonth.Checked = false;

                btnEdit.Visible = false;
            }
        }
        private void SetCheckList(CheckedListBox clist, bool check)
        {
            for (int i = 0; i < clist.Items.Count; i++)
            {
                clist.SetItemChecked(i, check);
            }
        }

        private void cbAll_CheckedChanged(object sender, EventArgs e)
        {
            if (sender.Equals(cbWeekDay))
            {
                SetCheckList(clbWeekDay, cbWeekDay.Checked);
            }
            else if (sender.Equals(cbMonthDay))
            {
                SetCheckList(clbMonthDay, cbMonthDay.Checked);
            }
            else if (sender.Equals(cbYearMonth))
            {
                SetCheckList(clbYearMonth, cbYearMonth.Checked);
            }
        }
        private void btnManage_Click(object sender, EventArgs e)
        {
            var pos = btnManage.Location;
            pos.Y = pos.Y - cmsManage.Height;
            cmsManage.Show(gbSetting, pos);
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectedSetting = new ScheduleSetting();
            PrepareItem();
            btnEdit_Click(sender, e);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var item = new ScheduleSetting()
            {
                
            };
            SelectedSetting = item;
            PrepareItem();
            btnEdit_Click(sender, e);
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lstSetting.SelectedIndex < 0 || lstSetting.SelectedIndex > Settings.Count)
            {
                SelectedSetting = null;
            }
            else
            {
                SelectedSetting = Settings[lstSetting.SelectedIndex];
            }


            if (SelectedSetting != null && NotificationBox.Show(MessageUtil.ToMessage(MessageUtil.KEY_DELETE_CONFIRM),
                MessageUtil.ToMessage(MessageUtil.KEY_DELETE), MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    if (!string.IsNullOrEmpty(SelectedSetting.ScheduleId) && !SelectedSetting.IsNew)
                    {
                        Node.DeleteScheduleSetting(Info, SelectedSetting);
                    }
                    Settings.Remove(SelectedSetting);
                    PrepareItem();
                }
                catch (Exception ex)
                {
                    NotificationBox.Show(MessageUtil.ToMessage(ex));
                }
            }
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (SelectedSetting == null)
                return;

            if (pnDetail.Enabled == false)
            {
                pnDetail.Enabled = true;
                btnCancel.Visible = true;
                btnEdit.Text = LanguageUtil.GetText(MessageUtil.KEY_SAVE_SETTING);
                gbSetting.Enabled = false;
            }
            else
            {
                SelectedSetting.Type = (ScheduleType)cmbType.SelectedValue;
                SelectedSetting.Times = (int)nudTimes.Value;
                SelectedSetting.Cycle = (int)nudCycle.Value;
                SelectedSetting.ExpectDate = dtpExpectedDate.Value;
                SelectedSetting.StartTime = mtbStartTime.Text;
                SelectedSetting.EndTime = mtbEndTime.Text;
                SelectedSetting.WeekDays = clbWeekDay.CheckedItems.OfType<NameValuePair<string, int>>()
                    .Select(x => x.Value).ToList();
                SelectedSetting.MonthDays = clbMonthDay.CheckedItems.OfType<NameValuePair<string, int>>()
                    .Select(x => x.Value).ToList();
                SelectedSetting.YearMonths = clbYearMonth.CheckedItems.OfType<NameValuePair<string, int>>()
                    .Select(x => x.Value).ToList();

                try
                {
                    bool isNew = string.IsNullOrEmpty(SelectedSetting.ScheduleId);
                    var resp = Node.SaveScheduleSetting(Info, SelectedSetting, isNew);
                    if (resp is AddScheduleResponse)
                    {
                        SelectedSetting.ScheduleId = ((AddScheduleResponse)resp).ScheduleId;
                        Settings.Add(SelectedSetting);
                    }

                    btnEdit.Text = LanguageUtil.GetText(MessageUtil.KEY_EDIT);

                    pnDetail.Enabled = false;
                    btnCancel.Visible = false;
                    gbSetting.Enabled = true;
                    lstSetting.SelectedIndex = Settings.IndexOf(SelectedSetting);
                }
                catch (Exception ex)
                {
                    NotificationBox.Show(MessageUtil.ToMessage(ex));
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            gbSetting.Enabled = true;
            PrepareItem();
        }

        private void lstSetting_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstSetting.SelectedIndex < 0 || lstSetting.SelectedIndex > Settings.Count)
            {
                SelectedSetting = null;
            }
            else
            {
                SelectedSetting = Settings[lstSetting.SelectedIndex];
            }

            PrepareItem();
        }

        private void cmsManage_Opening(object sender, CancelEventArgs e)
        {
            if (SelectedSetting == null)
            {
                removeToolStripMenuItem.Enabled = false;
                copyToolStripMenuItem.Enabled = false;
            }
            else
            {
                removeToolStripMenuItem.Enabled = true;
                copyToolStripMenuItem.Enabled = true;
            }
        }

        private void Editpanel_KeyUp(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                btnEdit_Click(sender, e);
            }
        }
    }
}
