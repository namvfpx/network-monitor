﻿using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.ThreadMonitor.Command;
using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class LogView : Form
    {
        private ThreadNode Node { get; set; }
        private ThreadInfo Info { get; set; }
        public LogView(ThreadNode node, ThreadInfo info, ViewLogEvent evt)
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
            Text = LanguageUtil.GetText(MessageUtil.KEY_LOG_OF_THREAD, info.Name);
            Node = node;
            Info = info;
            evt.Dirs.ForEach(x => {
                var tNode = tvLog.Nodes.Add(x.Directory);
                x.Files.ForEach(y => {
                    var fNode = tNode.Nodes.Add(y);
                });
            });
        }

        private void tvLog_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var node = e.Node;
            if (node.Parent == null)
            {
                if (node.IsExpanded)
                {
                    node.Collapse();
                }
                else
                {
                    node.ExpandAll();
                }
            }
            else
            {
                var logName = node.Text;
                try
                {
                    var evt = Node.SendCommand(new LoadLogContentCommand(Info.Id, logName));
                    if (evt is LogContentEvent)
                    {
                        var logEvent = evt as LogContentEvent;
                        var directoryPath = Path.Combine(Path.GetTempPath(), Info.Id);
                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }

                        var tempFilePath = Path.Combine(Path.GetTempPath(), Info.Id, logName);
                        using (var stream = new FileStream(tempFilePath, FileMode.Create))
                        {
                            var bytes = Encoding.Default.GetBytes(logEvent.Content);
                            stream.Write(bytes, 0, bytes.Length);
                            stream.Flush();
                        }

                        Process.Start("rundll32.exe", "shell32.dll, OpenAs_RunDLL " + tempFilePath);
                    }
                }
                catch (Exception ex)
                {
                    NotificationBox.Show(MessageUtil.ToMessage(ex));
                }
            }
        }
    }
}
