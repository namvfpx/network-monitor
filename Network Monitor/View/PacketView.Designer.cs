﻿namespace NetworkMonitor.View
{
    partial class PacketView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvPacket = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacket)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPacket
            // 
            this.dgvPacket.AllowUserToAddRows = false;
            this.dgvPacket.AllowUserToDeleteRows = false;
            this.dgvPacket.BackgroundColor = System.Drawing.Color.White;
            this.dgvPacket.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPacket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPacket.Location = new System.Drawing.Point(0, 0);
            this.dgvPacket.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvPacket.Name = "dgvPacket";
            this.dgvPacket.RowTemplate.Height = 24;
            this.dgvPacket.Size = new System.Drawing.Size(549, 148);
            this.dgvPacket.TabIndex = 0;
            // 
            // PacketView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgvPacket);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "PacketView";
            this.Size = new System.Drawing.Size(549, 148);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacket)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPacket;
    }
}
