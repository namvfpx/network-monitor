﻿namespace NetworkMonitor.View
{
    partial class ScheduleManagerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsManage = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbYearMonth = new System.Windows.Forms.CheckBox();
            this.cbMonthDay = new System.Windows.Forms.CheckBox();
            this.cbWeekDay = new System.Windows.Forms.CheckBox();
            this.clbMonthDay = new System.Windows.Forms.CheckedListBox();
            this.clbYearMonth = new System.Windows.Forms.CheckedListBox();
            this.clbWeekDay = new System.Windows.Forms.CheckedListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.gbDetail = new System.Windows.Forms.GroupBox();
            this.pnDetail = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mtbEndTime = new System.Windows.Forms.MaskedTextBox();
            this.mtbStartTime = new System.Windows.Forms.MaskedTextBox();
            this.nudTimes = new System.Windows.Forms.NumericUpDown();
            this.nudCycle = new System.Windows.Forms.NumericUpDown();
            this.dtpExpectedDate = new System.Windows.Forms.DateTimePicker();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lstSetting = new System.Windows.Forms.ListBox();
            this.gbSetting = new System.Windows.Forms.GroupBox();
            this.btnManage = new System.Windows.Forms.Button();
            this.cmsManage.SuspendLayout();
            this.gbDetail.SuspendLayout();
            this.pnDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCycle)).BeginInit();
            this.gbSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.removeToolStripMenuItem.Text = "remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.addToolStripMenuItem.Text = "add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // cmsManage
            // 
            this.cmsManage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.cmsManage.Name = "cmsManage";
            this.cmsManage.Size = new System.Drawing.Size(115, 70);
            this.cmsManage.Opening += new System.ComponentModel.CancelEventHandler(this.cmsManage_Opening);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.copyToolStripMenuItem.Text = "copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // cbYearMonth
            // 
            this.cbYearMonth.AutoSize = true;
            this.cbYearMonth.Location = new System.Drawing.Point(307, 86);
            this.cbYearMonth.Margin = new System.Windows.Forms.Padding(2);
            this.cbYearMonth.Name = "cbYearMonth";
            this.cbYearMonth.Size = new System.Drawing.Size(78, 17);
            this.cbYearMonth.TabIndex = 10;
            this.cbYearMonth.Text = "year-month";
            this.cbYearMonth.UseVisualStyleBackColor = true;
            this.cbYearMonth.CheckedChanged += new System.EventHandler(this.cbAll_CheckedChanged);
            // 
            // cbMonthDay
            // 
            this.cbMonthDay.AutoSize = true;
            this.cbMonthDay.Location = new System.Drawing.Point(159, 86);
            this.cbMonthDay.Margin = new System.Windows.Forms.Padding(2);
            this.cbMonthDay.Name = "cbMonthDay";
            this.cbMonthDay.Size = new System.Drawing.Size(75, 17);
            this.cbMonthDay.TabIndex = 9;
            this.cbMonthDay.Text = "month-day";
            this.cbMonthDay.UseVisualStyleBackColor = true;
            this.cbMonthDay.CheckedChanged += new System.EventHandler(this.cbAll_CheckedChanged);
            // 
            // cbWeekDay
            // 
            this.cbWeekDay.AutoSize = true;
            this.cbWeekDay.Location = new System.Drawing.Point(8, 86);
            this.cbWeekDay.Margin = new System.Windows.Forms.Padding(2);
            this.cbWeekDay.Name = "cbWeekDay";
            this.cbWeekDay.Size = new System.Drawing.Size(72, 17);
            this.cbWeekDay.TabIndex = 8;
            this.cbWeekDay.Text = "week-day";
            this.cbWeekDay.UseVisualStyleBackColor = true;
            this.cbWeekDay.CheckedChanged += new System.EventHandler(this.cbAll_CheckedChanged);
            // 
            // clbMonthDay
            // 
            this.clbMonthDay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.clbMonthDay.FormattingEnabled = true;
            this.clbMonthDay.Location = new System.Drawing.Point(159, 114);
            this.clbMonthDay.Margin = new System.Windows.Forms.Padding(2);
            this.clbMonthDay.Name = "clbMonthDay";
            this.clbMonthDay.Size = new System.Drawing.Size(126, 124);
            this.clbMonthDay.TabIndex = 12;
            // 
            // clbYearMonth
            // 
            this.clbYearMonth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.clbYearMonth.FormattingEnabled = true;
            this.clbYearMonth.Location = new System.Drawing.Point(307, 114);
            this.clbYearMonth.Margin = new System.Windows.Forms.Padding(2);
            this.clbYearMonth.Name = "clbYearMonth";
            this.clbYearMonth.Size = new System.Drawing.Size(126, 124);
            this.clbYearMonth.TabIndex = 13;
            // 
            // clbWeekDay
            // 
            this.clbWeekDay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.clbWeekDay.FormattingEnabled = true;
            this.clbWeekDay.Location = new System.Drawing.Point(8, 114);
            this.clbWeekDay.Margin = new System.Windows.Forms.Padding(2);
            this.clbWeekDay.Name = "clbWeekDay";
            this.clbWeekDay.Size = new System.Drawing.Size(126, 124);
            this.clbWeekDay.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(237, 53);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "times";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(237, 27);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "end-time";
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEdit.Location = new System.Drawing.Point(366, 290);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(72, 24);
            this.btnEdit.TabIndex = 15;
            this.btnEdit.Text = "edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // gbDetail
            // 
            this.gbDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDetail.Controls.Add(this.pnDetail);
            this.gbDetail.Controls.Add(this.btnCancel);
            this.gbDetail.Controls.Add(this.btnEdit);
            this.gbDetail.Location = new System.Drawing.Point(179, 3);
            this.gbDetail.Margin = new System.Windows.Forms.Padding(2);
            this.gbDetail.Name = "gbDetail";
            this.gbDetail.Padding = new System.Windows.Forms.Padding(2);
            this.gbDetail.Size = new System.Drawing.Size(450, 318);
            this.gbDetail.TabIndex = 1;
            this.gbDetail.TabStop = false;
            this.gbDetail.Text = "detail";
            // 
            // pnDetail
            // 
            this.pnDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnDetail.Controls.Add(this.cbYearMonth);
            this.pnDetail.Controls.Add(this.cbMonthDay);
            this.pnDetail.Controls.Add(this.cbWeekDay);
            this.pnDetail.Controls.Add(this.clbMonthDay);
            this.pnDetail.Controls.Add(this.clbYearMonth);
            this.pnDetail.Controls.Add(this.clbWeekDay);
            this.pnDetail.Controls.Add(this.label6);
            this.pnDetail.Controls.Add(this.label5);
            this.pnDetail.Controls.Add(this.label4);
            this.pnDetail.Controls.Add(this.label3);
            this.pnDetail.Controls.Add(this.label2);
            this.pnDetail.Controls.Add(this.label1);
            this.pnDetail.Controls.Add(this.mtbEndTime);
            this.pnDetail.Controls.Add(this.mtbStartTime);
            this.pnDetail.Controls.Add(this.nudTimes);
            this.pnDetail.Controls.Add(this.nudCycle);
            this.pnDetail.Controls.Add(this.dtpExpectedDate);
            this.pnDetail.Controls.Add(this.cmbType);
            this.pnDetail.Location = new System.Drawing.Point(5, 19);
            this.pnDetail.Name = "pnDetail";
            this.pnDetail.Size = new System.Drawing.Size(440, 258);
            this.pnDetail.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(237, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "start-time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 52);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "cycle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "expected-date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "type";
            // 
            // mtbEndTime
            // 
            this.mtbEndTime.Location = new System.Drawing.Point(333, 27);
            this.mtbEndTime.Margin = new System.Windows.Forms.Padding(2);
            this.mtbEndTime.Mask = "00:00:00";
            this.mtbEndTime.Name = "mtbEndTime";
            this.mtbEndTime.Size = new System.Drawing.Size(100, 20);
            this.mtbEndTime.TabIndex = 6;
            // 
            // mtbStartTime
            // 
            this.mtbStartTime.Location = new System.Drawing.Point(333, 3);
            this.mtbStartTime.Margin = new System.Windows.Forms.Padding(2);
            this.mtbStartTime.Mask = "00:00:00";
            this.mtbStartTime.Name = "mtbStartTime";
            this.mtbStartTime.Size = new System.Drawing.Size(100, 20);
            this.mtbStartTime.TabIndex = 5;
            // 
            // nudTimes
            // 
            this.nudTimes.Location = new System.Drawing.Point(333, 51);
            this.nudTimes.Margin = new System.Windows.Forms.Padding(2);
            this.nudTimes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudTimes.Name = "nudTimes";
            this.nudTimes.Size = new System.Drawing.Size(100, 20);
            this.nudTimes.TabIndex = 7;
            this.nudTimes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudCycle
            // 
            this.nudCycle.Location = new System.Drawing.Point(99, 51);
            this.nudCycle.Margin = new System.Windows.Forms.Padding(2);
            this.nudCycle.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCycle.Name = "nudCycle";
            this.nudCycle.Size = new System.Drawing.Size(102, 20);
            this.nudCycle.TabIndex = 4;
            this.nudCycle.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dtpExpectedDate
            // 
            this.dtpExpectedDate.CustomFormat = "dd/MM/yyyy";
            this.dtpExpectedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpectedDate.Location = new System.Drawing.Point(99, 28);
            this.dtpExpectedDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtpExpectedDate.Name = "dtpExpectedDate";
            this.dtpExpectedDate.Size = new System.Drawing.Size(101, 20);
            this.dtpExpectedDate.TabIndex = 3;
            // 
            // cmbType
            // 
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(99, 3);
            this.cmbType.Margin = new System.Windows.Forms.Padding(2);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(101, 21);
            this.cmbType.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(290, 290);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 24);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lstSetting
            // 
            this.lstSetting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstSetting.ContextMenuStrip = this.cmsManage;
            this.lstSetting.FormattingEnabled = true;
            this.lstSetting.Location = new System.Drawing.Point(4, 21);
            this.lstSetting.Margin = new System.Windows.Forms.Padding(2);
            this.lstSetting.Name = "lstSetting";
            this.lstSetting.Size = new System.Drawing.Size(162, 251);
            this.lstSetting.TabIndex = 0;
            this.lstSetting.SelectedIndexChanged += new System.EventHandler(this.lstSetting_SelectedIndexChanged);
            // 
            // gbSetting
            // 
            this.gbSetting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbSetting.Controls.Add(this.btnManage);
            this.gbSetting.Controls.Add(this.lstSetting);
            this.gbSetting.Location = new System.Drawing.Point(5, 3);
            this.gbSetting.Margin = new System.Windows.Forms.Padding(2);
            this.gbSetting.Name = "gbSetting";
            this.gbSetting.Padding = new System.Windows.Forms.Padding(2);
            this.gbSetting.Size = new System.Drawing.Size(170, 318);
            this.gbSetting.TabIndex = 2;
            this.gbSetting.TabStop = false;
            this.gbSetting.Text = "setting";
            // 
            // btnManage
            // 
            this.btnManage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnManage.Location = new System.Drawing.Point(87, 290);
            this.btnManage.Margin = new System.Windows.Forms.Padding(2);
            this.btnManage.Name = "btnManage";
            this.btnManage.Size = new System.Drawing.Size(79, 24);
            this.btnManage.TabIndex = 1;
            this.btnManage.Text = "manage";
            this.btnManage.UseVisualStyleBackColor = true;
            this.btnManage.Click += new System.EventHandler(this.btnManage_Click);
            // 
            // ScheduleManagerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 325);
            this.Controls.Add(this.gbDetail);
            this.Controls.Add(this.gbSetting);
            this.MinimumSize = new System.Drawing.Size(650, 350);
            this.Name = "ScheduleManagerView";
            this.Text = "schedule";
            this.cmsManage.ResumeLayout(false);
            this.gbDetail.ResumeLayout(false);
            this.pnDetail.ResumeLayout(false);
            this.pnDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCycle)).EndInit();
            this.gbSetting.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsManage;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbYearMonth;
        private System.Windows.Forms.CheckBox cbMonthDay;
        private System.Windows.Forms.CheckBox cbWeekDay;
        private System.Windows.Forms.CheckedListBox clbMonthDay;
        private System.Windows.Forms.CheckedListBox clbYearMonth;
        private System.Windows.Forms.CheckedListBox clbWeekDay;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.GroupBox gbDetail;
        private System.Windows.Forms.Panel pnDetail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox mtbEndTime;
        private System.Windows.Forms.MaskedTextBox mtbStartTime;
        private System.Windows.Forms.NumericUpDown nudTimes;
        private System.Windows.Forms.NumericUpDown nudCycle;
        private System.Windows.Forms.DateTimePicker dtpExpectedDate;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.ListBox lstSetting;
        private System.Windows.Forms.GroupBox gbSetting;
        private System.Windows.Forms.Button btnManage;
        private System.Windows.Forms.Button btnCancel;
    }
}