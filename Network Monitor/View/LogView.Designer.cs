﻿namespace NetworkMonitor.View
{
    partial class LogView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvLog = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // tvLog
            // 
            this.tvLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvLog.Location = new System.Drawing.Point(0, 0);
            this.tvLog.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tvLog.Name = "tvLog";
            this.tvLog.Size = new System.Drawing.Size(308, 401);
            this.tvLog.TabIndex = 0;
            this.tvLog.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvLog_NodeMouseDoubleClick);
            // 
            // LogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 401);
            this.Controls.Add(this.tvLog);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LogView";
            this.ShowIcon = false;
            this.Text = "log-of-thread";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView tvLog;
    }
}