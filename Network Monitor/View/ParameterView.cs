﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.Utils;
using NetworkMonitor.View.Editor;

namespace NetworkMonitor.View
{
    public partial class ParameterView : UserControl
    {
        private bool UpdateDataBack { get; set; } = false;
        public List<ThreadParameter> Parameters { get; private set; }

        private string CurrentFilter { get; set; } = "";
        private string CurrentMask { get; set; } = "";
        public ParameterView(List<ThreadParameter> parameters)
        {
            InitializeComponent();
            Dock = DockStyle.Fill;

            Parameters = parameters;

            dgvParameters.AutoGenerateColumns = false;
            dgvParameters.AlternatingRowsDefaultCellStyle = new DataGridViewCellStyle()
            {
                BackColor = Color.LightBlue
            };
            dgvParameters.Columns.Add(new DataGridViewTextBoxColumn()
            {
                HeaderText = LanguageUtil.GetText(MessageUtil.KEY_NAME),
                DataPropertyName = "Name",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells,
                ReadOnly = true
            });
            dgvParameters.Columns.Add(new DataGridViewEditorColumn()
            {
                HeaderText = LanguageUtil.GetText(MessageUtil.KEY_VALUE),
                DataPropertyName = "Value",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            });

            Parameters.ForEach(x => {
                var idx = dgvParameters.Rows.Add();
                var row = dgvParameters.Rows[idx];
                row.Tag = x;
                row.Cells[0].Value = x.Name;
                row.Cells[0].ToolTipText = string.IsNullOrEmpty(x.Description) ? x.Name : x.Description;
                GenerateCell(row.Cells[1], x);
            });
            UpdateDataBack = true;
            //dgvParameters.DataSource = Setting.Parameters;
        }

        private void GenerateCell(DataGridViewCell cell, ThreadParameter parameter)
        {
            var cellValue = ThreadParameter.ValueToString(parameter.Value);
            cell.Value = cellValue;
            if (parameter.Type == ParameterType.PARAM_LABEL
                || parameter.Type == ParameterType.PARAM_TABLE
                || parameter.Type == ParameterType.PARAM_SUB_TABULAR_EDITOR)
            {
                cell.ReadOnly = true;
            }
            else if (parameter.Type == ParameterType.PARAM_COMBOBOX)
            {
                var dynamicCell = cell as DataGridViewDynamicCell;
                dynamicCell.EditorType = typeof(DataGridViewComboBoxEditingControl);
                dynamicCell.Definition = parameter.Definition;
            }
            else if (parameter.Type == ParameterType.PARAM_TEXTBOX_MASK)
            {
                var dynamicCell = cell as DataGridViewDynamicCell;
                dynamicCell.EditorType = typeof(DataGridViewMaskTextBoxControl);
                dynamicCell.Definition = parameter.Definition;
            }
        }

        private void dgvParameters_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var row = dgvParameters.Rows[e.RowIndex];
            var data = row.Tag as ThreadParameter;
            if (data != null && data.Type == ParameterType.PARAM_PASSWORD && e.ColumnIndex == 1)
            {
                var cell = row.Cells[e.ColumnIndex];
                UpdateDataBack = false;
                cell.Value = new string('*', Convert.ToString(cell.Value).Length);
                UpdateDataBack = true;
            }
        }

        private void dgvParameters_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
                return;
            var row = dgvParameters.Rows[e.RowIndex];
            var cell = row.Cells[e.ColumnIndex];
            var data = row.Tag as ThreadParameter;

            if (data != null)
                GenerateCellAction(cell, data);
        }
        private void dgvParameters_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            var param = dgvParameters.CurrentRow.Tag as ThreadParameter;

            if (e.Control is TextBox)
            {
                var textBox = e.Control as TextBox;
                textBox.KeyPress += TextBox_KeyPress;
                CurrentMask = "";
                CurrentFilter = "";
                // mask password character for text
                if (param.Type == ParameterType.PARAM_PASSWORD)
                {
                    textBox.PasswordChar = '*';
                    //textBox.UseSystemPasswordChar = true;
                }
                else
                {
                    textBox.PasswordChar = '\0';
                    textBox.UseSystemPasswordChar = false;
                }

                // Filter textbox
                if (param.Type == ParameterType.PARAM_TEXTBOX_FILTER)
                {
                    if (param.Definition != null && param.Definition.Count > 0)
                    {
                        CurrentFilter = Convert.ToString(param.Definition[0].Value);
                    }
                }

                if (param.Type == ParameterType.PARAM_TEXTAREA_MAX
                    || param.Type == ParameterType.PARAM_TEXTBOX_MAX
                    || param.Type == ParameterType.PARAM_PASSWORD)
                {
                    if (param.Definition != null && param.Definition.Count > 0)
                    {
                        try
                        {
                            var length = Convert.ToInt32(param.Definition[0].Value);
                            textBox.MaxLength = length > 0 ? length : textBox.MaxLength;
                        }
                        catch
                        {
                        }
                    }
                }
                // mask text box
                else if (param.Type == ParameterType.PARAM_TEXTBOX_MASK)
                {
                    if (param.Definition != null && param.Definition.Count > 0)
                    {
                        try
                        {
                            CurrentMask = Convert.ToString(param.Definition[0].Value);
                            var length = CurrentMask.Length;
                            textBox.MaxLength = length > 0 ? length : textBox.MaxLength;

                        }
                        catch { }
                    }
                }
            }

        }

        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            var textBox = sender as TextBox;
            if (!string.IsNullOrWhiteSpace(CurrentFilter))
            {
                if (CurrentFilter.Contains(e.KeyChar.ToString()))
                {
                    e.Handled = true;
                }
            }

            if (!string.IsNullOrWhiteSpace(CurrentMask))
            {
                var text = textBox.Text;

                var maskChar = CurrentMask[textBox.Text.Length];
                if (CurrentFilter.Contains(e.KeyChar.ToString()))
                {
                    e.Handled = true;
                }
            }
        }

        private void dgvParameters_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (UpdateDataBack)
            {
                var row = dgvParameters.Rows[e.RowIndex];
                var value = row.Cells[e.ColumnIndex].Value;
                ((ThreadParameter)row.Tag).Value = value;
            }
        }

        private void GenerateCellAction(DataGridViewCell cell, ThreadParameter parameter)
        {
            EditorBase editor = null;
            var values = ThreadParameter.ValueCopy(parameter.Value);

            if (parameter.Type == ParameterType.PARAM_TABLE)
            {
                editor = new TableEditor(parameter.Name, values, parameter.Definition);
            }
            else if (parameter.Type == ParameterType.PARAM_SUB_TABULAR_EDITOR)
            {
                var lst = values as List<object>;
                var tablularValue = lst != null && lst.Count > 0 ? lst[0] : "";
                editor = new TabularEditor(parameter.Name, tablularValue, parameter.Definition);
            }
            else if (parameter.Type != ParameterType.PARAM_PASSWORD  
                && parameter.Type != ParameterType.PARAM_TEXTBOX_MASK
                && parameter.Type != ParameterType.PARAM_LABEL
                && parameter.Type != ParameterType.PARAM_COMBOBOX)
            {
                editor = new TextEditor(parameter.Name, values, parameter.Type, parameter.Definition);
            }

            if (editor != null)
            {
                if (editor.ShowDialog() == DialogResult.OK)
                {
                    parameter.Value = editor.Value;
                    UpdateDataBack = false;
                    GenerateCell(cell, parameter);
                    UpdateDataBack = true;
                }
            }
        }
    }
}
