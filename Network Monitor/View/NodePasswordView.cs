﻿using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.ThreadMonitor.Command;
using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class NodePasswordView : Form
    {
        private ThreadNode Node {get;set;}
        public NodePasswordView(ThreadNode node)
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
            Localization.LocalizationControl(this);
            Node = node;
            var lstAlgs = EncryptUtil.Algorithms;
            cmbOldAlg.DisplayMember = "Name";
            cmbOldAlg.ValueMember = "Value";
            cmbOldAlg.DataSource = lstAlgs;
            cmbNewAlg.DisplayMember = "Name";
            cmbNewAlg.ValueMember = "Value";
            cmbNewAlg.DataSource = lstAlgs;

        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            var cmd = new ChangePasswordCommand()
            {
                OldPassword = txtOldPassword.Text,
                NewPassword = txtNewPassword.Text,
                RealPassword = txtNewPassword.Text
            };

            if (string.Compare(cmd.NewPassword, txtConfirmPassword.Text) != 0)
            {
                NotificationBox.Show(MessageUtil.ToMessage(MessageUtil.KEY_PASSWORD_NOT_MATCH));
                return;
            }

            var oldAlg = Convert.ToString(cmbOldAlg.SelectedValue);
            var newAlg = Convert.ToString(cmbNewAlg.SelectedValue);
            if (!string.IsNullOrEmpty(oldAlg))
                cmd.OldPassword = EncryptUtil.EncryptPassword(cmd.OldPassword, oldAlg);
            if (!string.IsNullOrEmpty(newAlg))
                cmd.NewPassword = EncryptUtil.EncryptPassword(cmd.NewPassword, oldAlg);

            var res = Node.SendCommand(cmd);
            if (res is SuccessEvent)
            {
                NotificationBox.Show(MessageUtil.ToMessage(MessageUtil.KEY_PASSWORD_CHANGED_SUCCESS));
                this.Close();
            }
        }

        private void txtOldPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                BtnCancel_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                BtnOk_Click(sender, e);
            }
        }
    }
}
