﻿using NetworkMonitor.Profile;
using NetworkMonitor.ThreadMonitor.ThreadException;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class ProfileLoginView : Form
    {
        public ProfileLoginView()
        {
            InitializeComponent();
            ShowIcon = false;
            Localization.LocalizationControl(this);
            StartPosition = FormStartPosition.CenterScreen;
            Global.ContinueToLoginForm = false;
            Global.ContinueToMainForm = false;
            txtUsername.Text = Properties.Settings.Default.DefaultProfile;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Global.ContinueToMainForm = false;
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            try
            {
                if (!Regex.IsMatch(txtUsername.Text, @"^([a-zA-Z]+)([a-zA-Z0-9_]*)$"))
                {
                    throw new ValidateException(MessageUtil.KEY_USERNAME_INVALID);
                }

                var profile = LocalProfile.LogIn(username, password);
                if (profile == null)
                {
                    var res = NotificationBox.Show(LanguageUtil.GetText(MessageUtil.KEY_USERNAME_NOT_EXIST), "", MessageBoxButtons.YesNo);
                    if (res == DialogResult.Yes)
                    {
                        ProfilePasswordView pfForm = new ProfilePasswordView();
                        if (pfForm.ShowDialog() == DialogResult.OK)
                        {
                            profile = new LocalProfile()
                            {
                                Username = username,
                                Password = pfForm.Password
                            };
                            LocalProfile.Store(profile);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                Global.CurrentProfile = profile;
                Global.ContinueToMainForm = true;
                Properties.Settings.Default.DefaultProfile = txtUsername.Text;
                Properties.Settings.Default.Save();

                this.Close();
            }
            catch (Exception ex)
            {
                NotificationBox.Show(MessageUtil.ToMessage(ex));
            }
        }

        private void btnAnonymous_Click(object sender, EventArgs e)
        {
            Global.CurrentProfile = null;
            Global.ContinueToMainForm = true;
            this.Close();
        }

        private void ProfileLoginView_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void txtUsername_KeyUp(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                btnLogin_Click(sender, e);
            }
        }
    }
}
