﻿using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.ThreadMonitor.ThreadException;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class SettingView : Form
    {
        private ThreadInfo ThreadInfo { get; set; }
        private SettingInfoEvent Setting { get; set; }

        private bool UpdateDataBack { get; set; } = false;
        private ParameterView ParameterView { get; set; } = null;

        public List<ThreadParameter> Parameters { get; set; } = null;
        public string ClassName { get; set; } = "";
        public string ThreadName { get; set; } = "";
        public int StartupType { get; set; } = 0;

        public SettingView(ThreadInfo thread, SettingInfoEvent setting)
        {
            InitializeComponent();
            ShowIcon = false;
            this.StartPosition = FormStartPosition.CenterParent;
            Localization.LocalizationControl(this);
            ThreadInfo = thread;
            Setting = setting;
            LoadToForm();

            ParameterView = new ParameterView(Setting.Parameters);
            gbParameters.Controls.Add(ParameterView);
        }

        private void LoadToForm()
        {
            if (ThreadInfo == null || Setting == null)
                return;
            
            cmbStartupType.DisplayMember = "Text";
            cmbStartupType.ValueMember = "Value";
            cmbStartupType.DataSource = Setting.StartupType.ToList().Select(x => new { Text = x.Value, Value = x.Key }).ToList();
            txtThreadName.Text = ThreadInfo.Name;
            txtClassName.Text = Setting.ThreadClassName;
            cmbStartupType.SelectedValue = Setting.ThreadStartupType;

        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            ThreadName = txtThreadName.Text;
            ClassName = txtClassName.Text;
            if (string.IsNullOrWhiteSpace(ThreadName))
            {
                throw new ValidateException(MessageUtil.KEY_INVALID_FIELD, MessageUtil.KEY_NAME);
            }
            if (string.IsNullOrWhiteSpace(ClassName))
            {
                throw new ValidateException(MessageUtil.KEY_INVALID_FIELD, MessageUtil.KEY_CLASS_NAME);
            }
            StartupType = Convert.ToInt32(cmbStartupType.SelectedValue);
            Parameters = ParameterView.Parameters;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void SettingView_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
    }
}
