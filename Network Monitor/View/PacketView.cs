﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.Network;
using NetworkMonitor.Utils;

namespace NetworkMonitor.View
{
    public partial class PacketView : UserControl
    {
        public ThreadNode Node { get; private set; } = null;
        public PacketView()
        {
            InitializeComponent();
            DataGridViewTextBoxColumn colTime = new DataGridViewTextBoxColumn() {
                Name = "Time",
                HeaderText = LanguageUtil.GetText(MessageUtil.KEY_TIME),
                DataPropertyName = "Time",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells,
                ReadOnly = true
            };
            DataGridViewTextBoxColumn colType = new DataGridViewTextBoxColumn()
            {
                Name = "Type",
                HeaderText = LanguageUtil.GetText(MessageUtil.KEY_TYPE),
                //DataPropertyName = "Time",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells,
                ReadOnly = true
            };
            DataGridViewTextBoxColumn colContent = new DataGridViewTextBoxColumn()
            {
                Name = "Content",
                HeaderText = LanguageUtil.GetText(MessageUtil.KEY_CONTENT),
                DataPropertyName = "OriginalContent",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill,
                ReadOnly = true
            };
            dgvPacket.Columns.Add(colTime);
            dgvPacket.Columns.Add(colType);
            dgvPacket.Columns.Add(colContent);
        }

        public void UpdateView(ThreadNode newNode)
        {
            if (newNode != null && !newNode.Equals(Node))
            {
                Node = newNode;
                if (dgvPacket.InvokeRequired)
                {
                    try
                    {
                        dgvPacket.Invoke(new Action(() => dgvPacket.Rows.Clear()));
                    }
                    catch { }
                }
                else
                {
                    dgvPacket.Invoke(new Action(() => dgvPacket.Rows.Clear()));
                }

                lock (Node.EventStorage)
                {
                    Node.EventStorage.ForEach(x => {
                        AppendPacket(x);
                    });
                }
            }
        }

        public void AppendPacket(IPacket evt)
        {
            if (dgvPacket.InvokeRequired)
            {
                try
                {
                    dgvPacket.Invoke(new Action(() => AppendPacket(evt)));
                }
                catch { }
                return;
            }
            DataGridViewRow row = null;
            if (dgvPacket.Rows.Count == 0)
            {
                var idx = dgvPacket.Rows.Add();
                row = dgvPacket.Rows[idx];
            }
            else
            {
                row = (DataGridViewRow) dgvPacket.Rows[0].Clone();
                dgvPacket.Rows.Insert(0, row);
            }
            var type = !evt.IsOutDirection ? (evt as INetworkEvent).Type : EventType.None;
            row.Cells[0].Value = evt.Time.ToString("dd/MM HH:mm:ss.fff");
            row.Cells[1].Value = !evt.IsOutDirection ? type.ToString() : (evt as INetworkCommand).CommandType.ToString();
            row.Cells[2].Value = evt.ToString();
            row.Cells[2].ToolTipText = evt.ToString();

            var style = row.Cells[1].Style.Clone();
            switch (type)
            {
                case EventType.Error:
                    style.BackColor = Color.Red;
                    break;
                case EventType.SuccessEvent:
                case EventType.LoginResponse:
                case EventType.SettingInfo:
                case EventType.ScheduleInfo:
                case EventType.AddScheduleResponse:
                    style.BackColor = Color.LightGreen;
                    break;
                case EventType.UserConnectedEvent:
                case EventType.UserDisconnectedEvent:
                    style.BackColor = Color.LightBlue;
                    break;
                default:
                    style.BackColor = Color.White;
                    break;
            }

            row.Cells[1].Style = style;


        }
    }
}
