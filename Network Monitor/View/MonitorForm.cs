﻿using NetworkMonitor.DDTProtocol;
using NetworkMonitor.Network;
using NetworkMonitor.Profile;
using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.ThreadMonitor.ThreadException;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class MonitorForm : Form
    {
        ThreadView threadView = new ThreadView();
        ThreadLogView threadLogView = new ThreadLogView();
        PacketView packetView = new PacketView();

        private TreeNode SelectedNode { get; set; } = null;

        public MonitorForm()
        {
            InitializeComponent();
            ShowIcon = false;
            Localization.LocalizationControl(this);
            StartPosition = FormStartPosition.CenterScreen;
            Global.ContinueToLoginForm = false;
            if (Global.CurrentProfile != null)
            {
                profileToolStripMenuItem.Text = Global.CurrentProfile.Username;
                changePasswordToolStripMenuItem1.Enabled = true;
                storednodeToolStripMenuItem.Enabled = true;
            }
            else
            {
                profileToolStripMenuItem.Text = LanguageUtil.GetText(profileToolStripMenuItem.Text);
                changePasswordToolStripMenuItem1.Enabled = false;
                storednodeToolStripMenuItem.Enabled = false;
            }

            switchToolStripMenuItem.Text = LanguageUtil.GetText(switchToolStripMenuItem.Text);
            changePasswordToolStripMenuItem1.Text = LanguageUtil.GetText(changePasswordToolStripMenuItem1.Text);
            storednodeToolStripMenuItem.Text = LanguageUtil.GetText(storednodeToolStripMenuItem.Text);
            exitToolStripMenuItem.Text = LanguageUtil.GetText(exitToolStripMenuItem.Text);

            addnodeToolStripMenuItem.Text = LanguageUtil.GetText(addnodeToolStripMenuItem.Text);
            nodeToolStripMenuItem.Text = LanguageUtil.GetText(nodeToolStripMenuItem.Text);
            reconnectToolStripMenuItem.Text = LanguageUtil.GetText(reconnectToolStripMenuItem.Text);
            disconnectToolStripMenuItem.Text = LanguageUtil.GetText(disconnectToolStripMenuItem.Text);
            removeToolStripMenuItem.Text = LanguageUtil.GetText(removeToolStripMenuItem.Text);
            threadToolStripMenuItem.Text = LanguageUtil.GetText(threadToolStripMenuItem.Text);
            startToolStripMenuItem.Text = LanguageUtil.GetText(startToolStripMenuItem.Text);
            restartToolStripMenuItem.Text = LanguageUtil.GetText(restartToolStripMenuItem.Text);
            stopToolStripMenuItem.Text = LanguageUtil.GetText(stopToolStripMenuItem.Text);
            destroyToolStripMenuItem.Text = LanguageUtil.GetText(destroyToolStripMenuItem.Text);
            parameterToolStripMenuItem.Text = LanguageUtil.GetText(parameterToolStripMenuItem.Text);
            logToolStripMenuItem.Text = LanguageUtil.GetText(logToolStripMenuItem.Text);
            immediateToolStripMenuItem.Text = LanguageUtil.GetText(immediateToolStripMenuItem.Text);
            manageToolStripMenuItem.Text = LanguageUtil.GetText(manageToolStripMenuItem.Text);
            changepasswordToolStripMenuItem.Text = LanguageUtil.GetText(changepasswordToolStripMenuItem.Text);

            UpdateToolbar();
            threadView.Dock = DockStyle.Fill;
            scMain.Panel1.Controls.Add(threadView);
            threadLogView.Dock = DockStyle.Fill;
            packetView.Dock = DockStyle.Fill;
            //scThread.Panel1.Controls.Add(threadLogView);
            //scThread.Panel2.Controls.Add(packetView);

            threadView.OnThreadSelect = (tNode) =>
            {

                SelectedNode = tNode;
                UpdateToolbar();

                if (tNode == null || tNode.Tag == null)
                {
                    scThread.Panel1.Controls.Remove(threadLogView);
                    scThread.Panel2.Controls.Remove(packetView);
                    return;
                }

                scThread.Panel1.Controls.Add(threadLogView);
                scThread.Panel2.Controls.Add(packetView);

                threadLogView.UpdateView(tNode.Tag);

                if (tNode.Tag is ThreadNode)
                {
                    packetView.UpdateView(tNode.Tag as ThreadNode);
                }
                else if (tNode.Parent != null && tNode.Parent.Tag != null
                        && tNode.Parent.Tag is ThreadNode)
                {
                    packetView.UpdateView(tNode.Parent.Tag as ThreadNode);
                }
            };

            threadView.OnMessage = (node, info, pack) =>
            {
                if (node != null && node.Equals(packetView.Node))
                {
                    packetView.AppendPacket(pack);
                }
                if (!pack.IsOutDirection)
                {
                    var evt = pack as INetworkEvent;
                    if (evt.Type == EventType.UnknowEvent)
                    { }
                    else if (evt.Type == EventType.LogMonitorEvent)
                    {
                        if (threadLogView.Node != null && threadLogView.Node.Equals(info))
                        {
                            var lines = MessageUtil.ConvertToLines(((LogMonitorEvent)evt).LogResult);
                            threadLogView.AppendLog(lines);
                        }

                        if (info != null && SelectedNode != null && info.Equals(SelectedNode.Tag)
                            && !info.Status.Equals(((ThreadInfo)SelectedNode.Tag).Status))
                        {
                            UpdateToolbar();
                        }
                    }
                    else if (evt.Type == EventType.LogActionEvent)
                    {
                        if (threadLogView.Node != null && threadLogView.Node.Equals(node))
                        {
                            var lines = MessageUtil.ConvertToLines(((LogActionEvent)evt).Log);
                            threadLogView.AppendLog(lines);
                        }
                    }
                }
            };

            threadView.OnDisconnectNode = (node, ex) => Disconnected(node, ex);
        }

        public void UpdateToolbar()
        {
            addnodeToolStripMenuItem.Enabled = true;
            reconnectToolStripMenuItem.Enabled = true;
            disconnectToolStripMenuItem.Enabled = true;
            removeToolStripMenuItem.Enabled = true;
    
            startToolStripMenuItem.Enabled = true;
            restartToolStripMenuItem.Enabled = true;
            immediateToolStripMenuItem.Enabled = true;
            stopToolStripMenuItem.Enabled = true;
            destroyToolStripMenuItem.Enabled = true;
            parameterToolStripMenuItem.Enabled = true;
            scheduleToolStripMenuItem.Enabled = true;
            logToolStripMenuItem.Enabled = true;
            manageToolStripMenuItem.Enabled = true;
            changepasswordToolStripMenuItem.Enabled = true;

            if (SelectedNode == null)
            {
                reconnectToolStripMenuItem.Enabled = false;
                disconnectToolStripMenuItem.Enabled = false;
                removeToolStripMenuItem.Enabled = false;
                manageToolStripMenuItem.Enabled = false;
                changepasswordToolStripMenuItem.Enabled = false;

                restartToolStripMenuItem.Enabled = false;
                immediateToolStripMenuItem.Enabled = false;
                startToolStripMenuItem.Enabled = false;
                stopToolStripMenuItem.Enabled = false;
                destroyToolStripMenuItem.Enabled = false;
                parameterToolStripMenuItem.Enabled = false;
                scheduleToolStripMenuItem.Enabled = false;
                logToolStripMenuItem.Enabled = false;
            }
            else if (SelectedNode.Tag is ThreadNode)
            {
                restartToolStripMenuItem.Enabled = false;
                immediateToolStripMenuItem.Enabled = false;
                startToolStripMenuItem.Enabled = false;
                stopToolStripMenuItem.Enabled = false;
                destroyToolStripMenuItem.Enabled = false;
                parameterToolStripMenuItem.Enabled = false;
                scheduleToolStripMenuItem.Enabled = false;
                logToolStripMenuItem.Enabled = false;

                var node = (ThreadNode)SelectedNode.Tag;
                if (node.IsConnected)
                {
                    reconnectToolStripMenuItem.Enabled = false;
                    removeToolStripMenuItem.Enabled = false;
                }
                else
                {
                    changepasswordToolStripMenuItem.Enabled = false;
                    disconnectToolStripMenuItem.Enabled = false;
                    manageToolStripMenuItem.Enabled = false;
                }
            }
            else if (SelectedNode.Tag is ThreadInfo)
            {
                changepasswordToolStripMenuItem.Enabled = false;
                reconnectToolStripMenuItem.Enabled = false;
                disconnectToolStripMenuItem.Enabled = false;
                removeToolStripMenuItem.Enabled = false;

                var info = (ThreadInfo)SelectedNode.Tag;
                if (info.Status.Equals(ThreadInfo.STATUS_RUNNING))
                {
                    startToolStripMenuItem.Enabled = false;
                }
                else
                {
                    restartToolStripMenuItem.Enabled = false;
                    stopToolStripMenuItem.Enabled = false;
                    destroyToolStripMenuItem.Enabled = false;
                }
            }
        }

        public void Disconnected(ThreadNode node, Exception e)
        {
            if (e != null)
            {
                NotificationBox.Show(MessageUtil.ToMessage(MessageUtil.KEY_NODE_DISCONNECTED, node.Name, e.Message));
                if (SelectedNode != null)
                {
                    ThreadNode currentNode = null;
                    if (SelectedNode.Tag is ThreadNode)
                    {
                        currentNode = SelectedNode.Tag as ThreadNode;
                    }
                    else if (SelectedNode.Parent != null && SelectedNode.Parent.Tag is ThreadNode)
                    {
                        currentNode = SelectedNode.Parent.Tag as ThreadNode;
                    }
                    if (currentNode != null && currentNode.Equals(node))
                    {
                        // close all openning form of this node
                        Form[] formsList = Application.OpenForms.Cast<Form>().Where(x => !x.Name.Equals("MonitorForm")).ToArray();
                        foreach (Form openForm in formsList)
                        {
                            openForm.Close();
                        }
                    }
                }
            }
            if (SelectedNode != null && node.Equals(SelectedNode.Tag))
            {
                UpdateToolbar();
            }
        }

        private void addNodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            threadView.UserConnectNode(null, false);
        }

        private void Monitor_FormClosing(object sender, FormClosingEventArgs e)
        {
            var result = NotificationBox.Show(MessageUtil.ToMessage(MessageUtil.KEY_EXIT_PROGRAM_CONFIRM),
                MessageUtil.ToMessage(MessageUtil.KEY_EXIT_PROGRAM), MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
            {
                Global.ContinueToLoginForm = false;
                e.Cancel = true;
                return;
            }
            threadView.DisposeAllNodes();
        }

        private void reconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null && SelectedNode.Tag is ThreadNode)
            {
                threadView.UserConnectNode(SelectedNode.Tag as ThreadNode, true);
            }
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null && SelectedNode.Tag is ThreadNode)
            {
                threadView.UserDisconnectNode(SelectedNode.Tag as ThreadNode);
            }
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (SelectedNode != null && SelectedNode.Tag is ThreadNode)
            {
                threadView.UserRemoveNode(SelectedNode);
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null && SelectedNode.Tag is ThreadInfo)
            {
                threadView.UserStartThread(SelectedNode);
            }
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null && SelectedNode.Tag is ThreadInfo)
            {
                threadView.UserStopThread(SelectedNode);
            }
        }

        private void destroyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null && SelectedNode.Tag is ThreadInfo)
            {
                threadView.UserDestroyThread(SelectedNode);
            }
        }

        private void parameterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null && SelectedNode.Tag is ThreadInfo)
            {
                threadView.UserGetThreadSetting(SelectedNode);
            }
        }

        private void scheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null && SelectedNode.Tag is ThreadInfo)
            {
                threadView.UserGetThreadSchedule(SelectedNode);
            }
        }

        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null && SelectedNode.Tag is ThreadInfo)
            {
                threadView.UserGetThreadLog(SelectedNode);
            }
        }

        private void manageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null)
            {
                threadView.UserManageThread(SelectedNode);
            }
        }

        private void changepasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null)
            {
                threadView.UserChangePassword(SelectedNode);
            }
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null && SelectedNode.Tag is ThreadInfo)
            {
                threadView.UserRestartThread(SelectedNode);
            }
        }

        private void immediateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null && SelectedNode.Tag is ThreadInfo)
            {
                threadView.UserStartThreadImmediately(SelectedNode);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void switchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Global.ContinueToLoginForm = true;
            this.Close();
        }

        private void storednodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var view = new StoredLoginManageView();
            if (view.ShowDialog() == DialogResult.OK && view.SelectedNode != null)
            {
                threadView.UserConnectNode(new ThreadNode(view.SelectedNode.Host, view.SelectedNode.Port) {
                    Username = view.SelectedNode.Username,
                    Password = view.SelectedNode.Password,
                    Encrypt = view.SelectedNode.Encrypt,
                }, true);
            }
        }

        private void changePasswordToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var verifyPwd = new VerifyPasswordView();
            var res = verifyPwd.ShowDialog();
            while (res == DialogResult.OK)
            {
                if (!verifyPwd.Password.Equals(Global.CurrentProfile.Password))
                {
                    NotificationBox.Show(MessageUtil.ToMessage(new ValidateException(MessageUtil.KEY_PASSWORD_WRONG)));
                    verifyPwd = new VerifyPasswordView();
                    res = verifyPwd.ShowDialog();
                }
                else
                {
                    break;
                }
            }
            if (res == DialogResult.Cancel)
                return;

            var changePwd = new ProfilePasswordView();
            if (changePwd.ShowDialog() == DialogResult.OK && Global.CurrentProfile != null)
            {
                Global.CurrentProfile.Password = changePwd.Password;
                LocalProfile.Store(Global.CurrentProfile);
            }
        }
    }
}
