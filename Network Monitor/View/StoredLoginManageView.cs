﻿using NetworkMonitor.Profile;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View
{
    public partial class StoredLoginManageView : Form
    {
        BindingList<StoredNode> StoredNodes { get; set; }
        public StoredNode SelectedNode { get; set; } = null;
        public StoredLoginManageView()
        {
            InitializeComponent();
            Localization.LocalizationControl(this);
            this.ShowIcon = false;
            this.StartPosition = FormStartPosition.CenterParent;

            if (Global.CurrentProfile == null)
            {
                this.Close();
            }

            foreach (ToolStripItem item in cmsManage.Items)
            {
                item.Text = LanguageUtil.GetText(item.Text);
            }

            if (Properties.Settings.Default.Host == null)
                Properties.Settings.Default.Host = new System.Collections.Specialized.StringCollection();
            if (Properties.Settings.Default.Port == null)
                Properties.Settings.Default.Port = new System.Collections.Specialized.StringCollection();
            txtUsername.Text = Properties.Settings.Default.Username;
            cmbHost.DataSource = Properties.Settings.Default.Host;
            cmbPort.DataSource = Properties.Settings.Default.Port;

            cmbEncrypt.DisplayMember = "Name";
            cmbEncrypt.ValueMember = "Value";
            cmbEncrypt.DataSource = EncryptUtil.Algorithms;

            StoredNodes = new BindingList<StoredNode>(Global.CurrentProfile.StoredNodes);
            pnDetail.Enabled = false;
            lsbStoredNode.DisplayMember = "Name";
            lsbStoredNode.DataSource = StoredNodes;
            DialogResult = DialogResult.Cancel;
        }

        private void PrepareItem()
        {
            pnDetail.Enabled = false;
            btnEdit.Text = LanguageUtil.GetText(MessageUtil.KEY_EDIT);
            btnCancel.Visible = false;
            if (SelectedNode != null)
            {
                txtUsername.Text = SelectedNode.Username;
                txtPassword.Text = SelectedNode.Password;
                cmbEncrypt.SelectedValue = Convert.ToString(SelectedNode.Encrypt);
                cmbHost.Text = SelectedNode.Host;
                cmbPort.Text = Convert.ToString(SelectedNode.Port);
                btnEdit.Visible = true;
            }
            else
            {
                txtUsername.Text = "";
                txtPassword.Text ="";
                cmbEncrypt.SelectedValue = "";
                cmbHost.Text = "";
                cmbPort.Text = "";
                btnEdit.Visible = false;
            }
        }

        private void btnManage_Click(object sender, EventArgs e)
        {
            var pos = btnManage.Location;
            pos.Y = pos.Y - cmsManage.Height;
            cmsManage.Show(groupBox1, pos);
        }

        private void lsbStoredNode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lsbStoredNode.SelectedIndex < 0 || lsbStoredNode.SelectedIndex > StoredNodes.Count)
            {
                SelectedNode = null;
            }
            else
            {
                SelectedNode = StoredNodes[lsbStoredNode.SelectedIndex];
            }

            PrepareItem();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (pnDetail.Enabled == false)
            {
                pnDetail.Enabled = true;
                btnCancel.Visible = true;
                btnEdit.Text = LanguageUtil.GetText(MessageUtil.KEY_SAVE_SETTING);
                groupBox1.Enabled = false;
            }
            else
            {
                SelectedNode.Host = cmbHost.Text;
                SelectedNode.Password = txtPassword.Text;
                SelectedNode.Port = Convert.ToInt16(cmbPort.Text);
                SelectedNode.Encrypt = Convert.ToString(cmbEncrypt.SelectedValue);
                SelectedNode.Username = txtUsername.Text;

                var oldNode = StoredNodes.Where(x => x.Host.Equals(SelectedNode.Host) && x.Port == SelectedNode.Port
                && x.Username.Equals(SelectedNode.Username)).FirstOrDefault();
                if (oldNode != null)
                {
                    oldNode.Password = SelectedNode.Password;
                    oldNode.Encrypt = SelectedNode.Encrypt;
                }
                else
                {
                    StoredNodes.Add(SelectedNode);
                }

                LocalProfile.Store(Global.CurrentProfile);
                btnEdit.Text = LanguageUtil.GetText(MessageUtil.KEY_EDIT);

                pnDetail.Enabled = false;
                btnCancel.Visible = false;
                groupBox1.Enabled = true;
                lsbStoredNode.SelectedIndex = StoredNodes.IndexOf(SelectedNode);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            groupBox1.Enabled = true;
            PrepareItem();
        }

        private void cmsManage_Opening(object sender, CancelEventArgs e)
        {
            if (SelectedNode == null)
            {
                removeToolStripMenuItem.Enabled = false;
                copyToolStripMenuItem.Enabled = false;
            }
            else
            {
                removeToolStripMenuItem.Enabled = true;
                copyToolStripMenuItem.Enabled = true;
            }
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lsbStoredNode.SelectedIndex < 0 || lsbStoredNode.SelectedIndex > StoredNodes.Count)
            {
                SelectedNode = null;
            }
            else
            {
                SelectedNode = StoredNodes[lsbStoredNode.SelectedIndex];
            }

            StoredNodes.Remove(SelectedNode);

            LocalProfile.Store(Global.CurrentProfile);
            PrepareItem();

        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var item = new StoredNode()
            {
                Host = SelectedNode.Host,
                Port = SelectedNode.Port,
                Username = SelectedNode.Username,
                Password = SelectedNode.Password,
                Encrypt = SelectedNode.Password
            };
            SelectedNode = item;
            PrepareItem();
            btnEdit_Click(sender, e);
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectedNode = new StoredNode()
            {
                Port = 8080
            };
            PrepareItem();
            btnEdit_Click(sender, e);
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            if (SelectedNode != null)
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void lsbStoredNode_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                btnLogIn_Click(sender, e);
            }
        }

        private void Editpanel_KeyUp(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                btnEdit_Click(sender, e);
            }
        }
    }
}
