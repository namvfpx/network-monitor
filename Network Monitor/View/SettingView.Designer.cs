﻿namespace NetworkMonitor.View
{
    partial class SettingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbThread = new System.Windows.Forms.GroupBox();
            this.cmbStartupType = new System.Windows.Forms.ComboBox();
            this.txtClassName = new System.Windows.Forms.TextBox();
            this.txtThreadName = new System.Windows.Forms.TextBox();
            this.lblStartup = new System.Windows.Forms.Label();
            this.lblClass = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.gbParameters = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.gbThread.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbThread
            // 
            this.gbThread.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbThread.Controls.Add(this.cmbStartupType);
            this.gbThread.Controls.Add(this.txtClassName);
            this.gbThread.Controls.Add(this.txtThreadName);
            this.gbThread.Controls.Add(this.lblStartup);
            this.gbThread.Controls.Add(this.lblClass);
            this.gbThread.Controls.Add(this.lblName);
            this.gbThread.Location = new System.Drawing.Point(9, 10);
            this.gbThread.Margin = new System.Windows.Forms.Padding(2);
            this.gbThread.Name = "gbThread";
            this.gbThread.Padding = new System.Windows.Forms.Padding(2);
            this.gbThread.Size = new System.Drawing.Size(568, 80);
            this.gbThread.TabIndex = 0;
            this.gbThread.TabStop = false;
            this.gbThread.Text = "thread";
            // 
            // cmbStartupType
            // 
            this.cmbStartupType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbStartupType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStartupType.FormattingEnabled = true;
            this.cmbStartupType.Location = new System.Drawing.Point(444, 20);
            this.cmbStartupType.Margin = new System.Windows.Forms.Padding(2);
            this.cmbStartupType.Name = "cmbStartupType";
            this.cmbStartupType.Size = new System.Drawing.Size(121, 21);
            this.cmbStartupType.TabIndex = 2;
            // 
            // txtClassName
            // 
            this.txtClassName.Location = new System.Drawing.Point(78, 49);
            this.txtClassName.Margin = new System.Windows.Forms.Padding(2);
            this.txtClassName.Name = "txtClassName";
            this.txtClassName.Size = new System.Drawing.Size(240, 20);
            this.txtClassName.TabIndex = 3;
            // 
            // txtThreadName
            // 
            this.txtThreadName.Location = new System.Drawing.Point(78, 20);
            this.txtThreadName.Margin = new System.Windows.Forms.Padding(2);
            this.txtThreadName.Name = "txtThreadName";
            this.txtThreadName.Size = new System.Drawing.Size(240, 20);
            this.txtThreadName.TabIndex = 1;
            // 
            // lblStartup
            // 
            this.lblStartup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStartup.AutoSize = true;
            this.lblStartup.Location = new System.Drawing.Point(366, 23);
            this.lblStartup.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStartup.Name = "lblStartup";
            this.lblStartup.Size = new System.Drawing.Size(62, 13);
            this.lblStartup.TabIndex = 0;
            this.lblStartup.Text = "startup-type";
            // 
            // lblClass
            // 
            this.lblClass.AutoSize = true;
            this.lblClass.Location = new System.Drawing.Point(14, 51);
            this.lblClass.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(60, 13);
            this.lblClass.TabIndex = 0;
            this.lblClass.Text = "class-name";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(14, 23);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(33, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "name";
            // 
            // gbParameters
            // 
            this.gbParameters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbParameters.Location = new System.Drawing.Point(9, 94);
            this.gbParameters.Margin = new System.Windows.Forms.Padding(2);
            this.gbParameters.Name = "gbParameters";
            this.gbParameters.Padding = new System.Windows.Forms.Padding(2);
            this.gbParameters.Size = new System.Drawing.Size(568, 240);
            this.gbParameters.TabIndex = 1;
            this.gbParameters.TabStop = false;
            this.gbParameters.Text = "parameters";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(514, 340);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(64, 24);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(446, 340);
            this.btnOK.Margin = new System.Windows.Forms.Padding(2);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(64, 24);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // SettingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 375);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.gbParameters);
            this.Controls.Add(this.gbThread);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(604, 414);
            this.Name = "SettingView";
            this.ShowIcon = false;
            this.Text = "setting";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingView_FormClosing);
            this.gbThread.ResumeLayout(false);
            this.gbThread.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbThread;
        private System.Windows.Forms.ComboBox cmbStartupType;
        private System.Windows.Forms.TextBox txtThreadName;
        private System.Windows.Forms.Label lblStartup;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtClassName;
        private System.Windows.Forms.GroupBox gbParameters;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
    }
}