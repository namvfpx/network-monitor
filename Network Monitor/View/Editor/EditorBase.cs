﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View.Editor
{
    public class EditorBase : Form
    {

        public object Value { get; set; } = null;

        public EditorBase()
        {
            MaximumSize = new Size(800, 600);
            MinimumSize = new Size(500, 400);
            Size = MinimumSize;
            StartPosition = FormStartPosition.CenterParent;
        }

        protected virtual object GetValueOnOk()
        {
            return null;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Value = GetValueOnOk();
            DialogResult = DialogResult.OK;
            this.Close();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        protected void Event_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                btnOK_Click(sender, e);
            }
        }
    }
}
