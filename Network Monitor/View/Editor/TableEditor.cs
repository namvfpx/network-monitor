﻿using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View.Editor
{
    public partial class TableEditor : EditorBase
    {
        public List<ThreadParameter> Definition { get; set; }
        public List<object> Values { get; set; }
        public TableEditor(string parameterName, object value, List<ThreadParameter> definition) : base()
        {
            InitializeComponent();
            Size = MinimumSize;
            Localization.LocalizationControl(this);

            btnOK.Click += btnOK_Click;
            btnCancel.Click += btnCancel_Click;

            Definition = definition;
            Values = value as List<object>;
            if (Values == null)
            {
                Values = new List<object>();
            }
            Text = parameterName;

            Definition.ForEach(x => {
                var idx = dgvParameters.Columns.Add(new DataGridViewTextBoxColumn() {
                    HeaderText = x.Name,
                    AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                });
            });

            Values.ForEach(x => {
                if (x == null || string.IsNullOrEmpty(Convert.ToString(x)))
                    return;
                var items = x as List<object>;
                var idx = dgvParameters.Rows.Add();
                var row = dgvParameters.Rows[idx];
                row.Tag = items;
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (items.Count <= i)
                    {
                        items.Add("");
                    }
                    try
                    {
                        row.Cells[i].Value = ThreadParameter.ValueToString(items[i]);
                    }
                    catch { }
                }
            });
        }

        protected override object GetValueOnOk()
        {
            return Values;
        }

        private void Evt_KeyUp(object sender, KeyEventArgs e)
        {
            Event_KeyUp(sender, e);
        }

        private void dgvParameters_AddedRow(object sender, DataGridViewRowEventArgs e)
        {
        }

        private void dgvParameters_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            var item = e.Row.Tag;
            if (item != null)
            {
                Values.Remove(item);
            }
        }

        private void dgvParameters_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var row = dgvParameters.Rows[e.RowIndex];
            var item = row.Tag as List<object>;
            if (row.Tag == null)
            {
                item = new List<object>();
                row.Tag = item;
                
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    item.Add(new object());
                }
                if (Values.Count <= e.RowIndex)
                {
                    Values.Add(item);
                }
                else
                {
                    Values.Insert(e.RowIndex, item);
                }
            }
            var cell = row.Cells[e.ColumnIndex];

            item[e.ColumnIndex] = cell.Value;
        }

        private void dgvParameters_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

        }
    }
}
