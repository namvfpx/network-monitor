﻿using NetworkMonitor.ThreadMonitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View.Editor
{
    public class DataGridViewEditorColumn : DataGridViewColumn
    {
        public DataGridViewEditorColumn() : base(new DataGridViewDynamicCell())
        {

        }
        public override DataGridViewCell CellTemplate { get => base.CellTemplate; set => base.CellTemplate = value; }

    }
    public class DataGridViewDynamicCell : DataGridViewTextBoxCell
    {
        public Type EditorType { get; set; } = typeof(DataGridViewTextBoxEditingControl);
        public List<ThreadParameter> Definition { get; set; } = null;
        public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
        {
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);

            var control = DataGridView.EditingControl;
            if (control is DataGridViewComboBoxEditingControl)
            {
                var combo = control as DataGridViewComboBoxEditingControl;
                combo.DropDownStyle = ComboBoxStyle.DropDownList;
                combo.Items.Clear();
                if (Definition != null)
                {
                    Definition.ForEach(x => combo.Items.Add(x.Value));
                }
                combo.SelectedItem = Value;
            }
            else if (control is DataGridViewMaskTextBoxControl)
            {
                var mstb = control as DataGridViewMaskTextBoxControl;
                if (Definition != null && Definition.Count > 0)
                {
                    mstb.Mask = Convert.ToString(Definition[0].Value);
                }
                mstb.Text = Convert.ToString(Value);
            }
        }

        public override Type EditType => EditorType;

        public override Type ValueType => typeof(string);
        public override object DefaultNewRowValue => "";
    }

    public class DataGridViewMaskTextBoxControl : MaskedTextBox, IDataGridViewEditingControl
    {
        public DataGridViewMaskTextBoxControl() : base()
        {
            TextChanged += DataGridViewMaskTextBoxControl_TextChanged;
        }

        private void DataGridViewMaskTextBoxControl_TextChanged(object sender, EventArgs e)
        {
            EditingControlValueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
        }

        public DataGridView EditingControlDataGridView { get; set; }
        public object EditingControlFormattedValue { get => Text; set => Text = Convert.ToString(value); }
        public int EditingControlRowIndex { get; set; }
        public bool EditingControlValueChanged { get; set; }

        public Cursor EditingPanelCursor => base.Cursor;

        public bool RepositionEditingControlOnValueChange => false;

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
        }

        public bool EditingControlWantsInputKey(Keys keyData, bool dataGridViewWantsInputKey)
        {
            return !dataGridViewWantsInputKey;
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return EditingControlFormattedValue;
        }

        public void PrepareEditingControlForEdit(bool selectAll)
        {
            
        }
    }

}
