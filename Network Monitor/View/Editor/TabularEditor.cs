﻿using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View.Editor
{
    public partial class TabularEditor : EditorBase
    {
        private ParameterView ParameterView { get; set; }
        private List<ThreadParameter> Parameters { get; set; }
        public TabularEditor(string parameterName, object value, List<ThreadParameter> definition) : base()
        {
            InitializeComponent();
            Localization.LocalizationControl(this);
            Size = MinimumSize;
            btnOK.Click += btnOK_Click;
            btnCancel.Click += btnCancel_Click;
            Text = parameterName;
            Parameters = definition.CopyList();

            var values = value as List<object>;
            for (int i = 0; i < Parameters.Count; i++)
            {
                Parameters[i].Value = (values != null && values.Count > i) ? values[i] : "";
            }

            ParameterView = new ParameterView(Parameters);
            pnParameter.Controls.Add(ParameterView);
        }

        protected override object GetValueOnOk()
        {
            return new List<object> { Parameters.Select(x => x.Value).ToList() };
        }
    }
}
