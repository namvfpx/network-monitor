﻿using NetworkMonitor.ThreadMonitor;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.View.Editor
{
    public partial class TextEditor : EditorBase
    {
        private List<ThreadParameter> Definition { get; set; }
        private ParameterType Type { get; set; }
        public TextEditor(string parameterName, object value, ParameterType parameterType, List<ThreadParameter> definition) : base()
        {
            InitializeComponent();
            Localization.LocalizationControl(this);
            Text = parameterName;
            txtValue.Text = Convert.ToString(value);
            Definition = definition;
            Type = parameterType;

            btnOK.Click += btnOK_Click;
            btnCancel.Click += btnCancel_Click;

            if (Type == ParameterType.PARAM_PASSWORD)
            {
                txtValue.PasswordChar = '*';
                //textBox.UseSystemPasswordChar = true;
            }

            if (Type == ParameterType.PARAM_TEXTBOX_FILTER)
            {
                if (Definition != null && Definition.Count > 0)
                {
                    txtValue.KeyPress += txtValue_KeyPress;
                }
            }

            if (Type == ParameterType.PARAM_TEXTAREA_MAX
                || Type == ParameterType.PARAM_TEXTBOX_MAX
                || Type == ParameterType.PARAM_PASSWORD)
            {
                if (Definition != null && Definition.Count > 0)
                {
                    try
                    {
                        var length = Convert.ToInt32(Definition[0].Value);
                        txtValue.MaxLength = length > 0 ? length : txtValue.MaxLength;
                    }
                    catch
                    {
                    }
                }
            }
        }

        protected override object GetValueOnOk()
        {
            return txtValue.Text;
        }

        private void Evt_KeyUp(object sender, KeyEventArgs e)
        {
            Event_KeyUp(sender, e);
        }

        private void txtValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Definition != null && Definition.Count > 0)
            {
                var currentFilter = Convert.ToString(Definition[0].Value);
                if (currentFilter.Contains(e.KeyChar.ToString()))
                {
                    e.Handled = true;
                }
            }
        }
    }
}
