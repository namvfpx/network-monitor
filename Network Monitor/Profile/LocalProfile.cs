﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using NetworkMonitor.Utils;
using NetworkMonitor.ThreadMonitor.ThreadException;

namespace NetworkMonitor.Profile
{
    public class LocalProfile
    {
        public string Username { get; set; } = "";
        public string Password { get; set; } = "";
        public List<StoredNode> StoredNodes { get; set; } = new List<StoredNode>();

        public static void Store(LocalProfile profile)
        {
            var dirPath = Path.Combine(Global.ProfileDirectory, profile.Username);
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            var profFile = Path.Combine(dirPath, Global.ProfileFileName);
            var dataFile = Path.Combine(dirPath, Global.StoredNodeFileName);

            using (var outStream = new FileStream(profFile, FileMode.Create))
            {
                var prof = EncryptUtil.EncryptPassword(profile.Password, EncryptUtil.SHA);
                var bytes = Encoding.Default.GetBytes(prof);
                outStream.Write(bytes, 0, bytes.Length);
            }

            using (var outStream = new FileStream(dataFile, FileMode.Create))
            {
                var data = JsonConvert.SerializeObject(profile.StoredNodes, Formatting.Indented);
                var bytes = Encoding.Default.GetBytes(data);
                bytes = EncryptUtil.AESEncrypt(bytes, profile.Username.ToLower() + "||" + profile.Password);
                outStream.Write(bytes, 0, bytes.Length);
            }
        }

        public static LocalProfile LogIn(string username, string password)
        {
            var dirPath = Path.Combine(Global.ProfileDirectory, username);
            var profFile = Path.Combine(dirPath, Global.ProfileFileName);
            var dataFile = Path.Combine(dirPath, Global.StoredNodeFileName);
            if (!File.Exists(profFile))
            {
                return null;
            }

            var profile = new LocalProfile()
            {
                Username = username
            };
            using (var inStream = new FileStream(profFile, FileMode.Open))
            {
                using (var memoryStream = new MemoryStream())
                {
                    inStream.CopyTo(memoryStream);
                    profile.Password = Encoding.Default.GetString(memoryStream.ToArray());
                }
            }

            if (!profile.Password.Equals(EncryptUtil.EncryptPassword(password, EncryptUtil.SHA)))
            {
                throw new ValidateException(MessageUtil.KEY_PASSWORD_WRONG);
            }

            profile.Password = password;

            try
            {
                using (var inStream = new FileStream(dataFile, FileMode.Open))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        inStream.CopyTo(memoryStream);
                        var bytes = EncryptUtil.AESDecrypt(memoryStream.ToArray(), profile.Username.ToLower() + "||" + profile.Password);
                        var data = Encoding.Default.GetString(bytes);
                        profile.StoredNodes = JsonConvert.DeserializeObject<List<StoredNode>>(data);
                    }
                }
            }
            catch { }

            return profile;
        }
    }
}
