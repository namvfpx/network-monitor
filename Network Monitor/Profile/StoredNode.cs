﻿using NetworkMonitor.ThreadMonitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.Profile
{
    public class StoredNode
    {
        public string Host { get; set; } = "";
        public short Port { get; set; }
        public string Encrypt { get; set; } = "";
        public string Username { get; set; } = "";
        public string Password { get; set; } = "";

        public string Name => string.Format("{0}:{1}({2})", Host, Port, Username);
    }
}
