﻿using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor.DDTProtocol
{
    public class DDTPError
    {
        public static ErrorEvent ConvertError(string respId, string ctx, string info, string desc)
        {
            return new ErrorEvent()
            {
                Identifier = respId,
                ErrorInfo = info,
                ErrorContext = GetContext(ctx),
                ErrorDescription = GetDescription(desc)
            };
        }

        private static string GetContext(string ctx)
        {
            if (ctx.EndsWith("verifyPassword"))
            {
                return MessageUtil.KEY_PASSWORD;
            }
            return ctx;
        }

        private static string GetDescription(string desc)
        {
            if (desc.Equals("FSS-00006"))
            {
                return MessageUtil.KEY_PASSWORD_WRONG;
            }
            return desc;
        }
    }
}
