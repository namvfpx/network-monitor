﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.ThreadMonitor.Command;
using NetworkMonitor.Network;
using NetworkMonitor.ThreadMonitor.Event;
using NetworkMonitor.Utils;
using NetworkMonitor.ThreadMonitor;

namespace NetworkMonitor.DDTProtocol
{
    public class DDTPFactory : IPacketFactory
    {
        string package = "com.fss.thread.";

        public INetworkEvent FromStream(Stream stream, Func<string, EventType> getExpectedType)
        {
            var message = DDTP.LoadData(stream);
            if (message == null)
                return null;
            INetworkEvent evt = null;
            try
            {
                if (!string.IsNullOrEmpty(message.ResponseId))
                {
                    if (message.Contains(DDTP.FIELD_ERROR_CONTEXT)
                       || message.Contains(DDTP.FIELD_ERROR_DESCRIPTION)
                       || message.Contains(DDTP.FIELD_ERROR_INFO))
                    {
                        evt = DDTPError.ConvertError(message.ResponseId,
                                message.GetString(DDTP.FIELD_ERROR_CONTEXT), 
                                message.GetString(DDTP.FIELD_ERROR_INFO), 
                                message.GetString(DDTP.FIELD_ERROR_DESCRIPTION));

                    }
                    else
                    {
                        var exprectedType = getExpectedType(message.ResponseId);
                        switch (exprectedType)
                        {
                            case EventType.LoginResponse:
                                evt = ToLogInResponse(message);
                                break;
                            case EventType.SettingInfo:
                                evt = ToSettingInfo(message);
                                break;
                            case EventType.ScheduleInfo:
                                evt = ToScheduleInfo(message);
                                break;
                            case EventType.AddScheduleResponse:
                                evt = ToScheduleResponse(message);
                                break;
                            case EventType.LogInfo:
                                evt = ToViewLogEvent(message);
                                break;
                            case EventType.LogContent:
                                evt = ToLogContentEvent(message);
                                break;
                            case EventType.ManagedThreadsLoad:
                                evt = ToManageThreadResponse(message);
                                break;
                            case EventType.SuccessEvent:
                                evt = new SuccessEvent() { Identifier = message.ResponseId };
                                break;
                            case EventType.None:
                            default:
                                break;
                        }
                    }
                }
                else if (message.GetString(DDTP.FIELD_FUNCTION).Equals("logMonitor"))
                    evt = ToLogMonitorEvent(message);
                else if (message.GetString(DDTP.FIELD_FUNCTION).Equals("unloadThread"))
                    evt = ToUnloadThreadEvent(message);
                else if (message.GetString(DDTP.FIELD_FUNCTION).Equals("loadThread"))
                    evt = ToLoadThreadEvent(message);
                else if (message.GetString(DDTP.FIELD_FUNCTION).Equals("renameThread"))
                    evt = ToRenameThreadEvent(message);
                else if (message.GetString(DDTP.FIELD_FUNCTION).Equals("logAction"))
                    evt = ToLogActionEvent(message);
                else if (message.GetString(DDTP.FIELD_FUNCTION).Equals("userConnected"))
                    evt = ToUserConnectedEvent(message);

            }
            catch (Exception e)
            {
                if (message.Contains(DDTP.FIELD_ERROR_CONTEXT)
                    || message.Contains(DDTP.FIELD_ERROR_DESCRIPTION)
                    || message.Contains(DDTP.FIELD_ERROR_INFO))
                {
                    evt = new ErrorEvent() {
                        Identifier = message.ResponseId,
                        ErrorInfo = message.GetString(DDTP.FIELD_ERROR_INFO),
                        ErrorContext = message.GetString(DDTP.FIELD_ERROR_CONTEXT),
                        ErrorDescription = message.GetString(DDTP.FIELD_ERROR_DESCRIPTION)
                    };
                }
                else
                {
                    evt = new UnknowEvent() { Identifier = message.ResponseId, Exception = e };
                }
            }

            if (evt == null)
            {
                evt = new UnknowEvent() { Identifier = message.ResponseId };
            }
            if (!(evt is LogInResponse)
                && !(evt is LogContentEvent))
            {
                evt.OriginalContent = message.ToString();
            }
            evt.Protocol = DDTP.SIGN_SYMBOL;
            evt.Function = message.GetString(DDTP.FIELD_FUNCTION);
            evt.ClassName = message.GetString(DDTP.FIELD_CLASS);
            return evt;
        }
        
        public byte[] PacketToBytes(INetworkCommand command)
        {
            if (command == null)
                return new byte[] { };
            command.Protocol = DDTP.SIGN_SYMBOL;
            DDTP message = null;
            if (command is LogIn)
                message = CreateLogin((LogIn)command);
            else if (command is CommandBase)
                message = CreateCommand((CommandBase)command);
            if (message == null)
                return new byte[] { };
            command.SendContent = message.ToString();
            return message.ToByteArray();
        }
        
        private DDTP CreateLogin(LogIn login)
        {
            DDTP request = new DDTP();
            request.RequestId = login.Identifier;
            request.Function = DDTPFunction.GetFunction(login.CommandType);
            request.ClassName = package + DDTPFunction.GetClass(login.CommandType);
            request.Username = login.Username;
            request.SetString(DDTP.FIELD_PASSWORD, login.Password);
            return request;
        }

        private DDTP CreateCommand(CommandBase command)
        {
            DDTP request = new DDTP();
            request.RequestId = command.Identifier;
            request.Function = DDTPFunction.GetFunction(command.CommandType);
            request.ClassName = package + DDTPFunction.GetClass(command.CommandType);
            if (command is ThreadCommand)
                request.SetString(DDTP.FIELD_THREAD_ID, ((ThreadCommand)command).ThreadId);

            CustomCommand(request, command);

            return request;
        }
        private LogActionEvent ToLogActionEvent(DDTP message)
        {
            LogActionEvent ev = new LogActionEvent()
            {
                Log = MessageUtil.RemoveHtmlTag(message.GetString(DDTP.FIELD_LOG)),
                ClassName = message.GetString(DDTP.FIELD_CLASS),
                Function = message.GetString(DDTP.FIELD_FUNCTION)
            };

            return ev;
        }
        
        private LogMonitorEvent ToLogMonitorEvent(DDTP message)
        {
            LogMonitorEvent ev = new LogMonitorEvent();
            ev.ThreadId = message.GetString(DDTP.FIELD_THREAD_ID);
            ev.ThreadStatus = message.GetString(DDTP.FIELD_THREAD_STATUS);
            ev.LogResult = message.GetString(DDTP.FIELD_LOG_RESULT);
            ev.ClassName = message.GetString(DDTP.FIELD_CLASS);
            ev.Function = message.GetString(DDTP.FIELD_FUNCTION);

            return ev;
        }
        private RenameThreadEvent ToRenameThreadEvent(DDTP message)
        {
            RenameThreadEvent ev = new RenameThreadEvent();
            ev.ThreadId = message.GetString(DDTP.FIELD_THREAD_ID);
            ev.ClassName = message.GetString(DDTP.FIELD_CLASS);
            ev.Function = message.GetString(DDTP.FIELD_FUNCTION);
            ev.ThreadName = message.GetString(DDTP.FIELD_THREAD_NAME);

            return ev;
        }
        private UnloadThreadEvent ToUnloadThreadEvent(DDTP message)
        {
            UnloadThreadEvent ev = new UnloadThreadEvent();
            ev.ThreadId = message.GetString(DDTP.FIELD_THREAD_ID);
            ev.ClassName = message.GetString(DDTP.FIELD_CLASS);
            ev.Function = message.GetString(DDTP.FIELD_FUNCTION);

            return ev;
        }
        private LoadThreadEvent ToLoadThreadEvent(DDTP message)
        {
            LoadThreadEvent ev = new LoadThreadEvent();
            ev.ThreadId = message.GetString(DDTP.FIELD_THREAD_ID);
            ev.ThreadName = message.GetString(DDTP.FIELD_THREAD_NAME);
            ev.ThreadStatus = message.GetString(DDTP.FIELD_THREAD_STATUS);
            ev.ThreadId = message.GetString(DDTP.FIELD_THREAD_ID);
            ev.ClassName = message.GetString(DDTP.FIELD_CLASS);
            ev.Function = message.GetString(DDTP.FIELD_FUNCTION);

            return ev;
        }
        private UserConnectedEvent ToUserConnectedEvent(DDTP message)
        {
            UserConnectedEvent ev = new UserConnectedEvent();
            ev.Username = message.GetString(DDTP.FIELD_USERNAME_EVT);
            ev.Host = message.GetString(DDTP.FIELD_HOST);
            ev.Token = message.GetString(DDTP.FIELD_CHANNEL);
            var startTime = message.GetString(DDTP.FIELD_START_DATE);

            if (!string.IsNullOrEmpty(startTime))
            {
                try
                {
                    ev.StartTime = DateTime.ParseExact(startTime, "dd/MM/yyyy HH:mm:ss", null);
                }
                catch { }
            }

            return ev;
        }

        private LogInResponse ToLogInResponse(DDTP message)
        {
            LogInResponse resp = new LogInResponse()
            {
                Identifier = message.ResponseId,
                ThreadName = message.GetString(DDTP.FIELD_THREAD_APP_NAME),
                ThreadVersion = message.GetString(DDTP.FIELD_THREAD_APP_VER),
                AppName = message.GetString(DDTP.FIELD_APP_NAME),
                AppVersion = message.GetString(DDTP.FIELD_APP_VERSION),
                SessionId = message.GetString(DDTP.FIELD_CHANNEL),
                IsPasswordExpired = message.Contains(DDTP.FIELD_PASSWORD_EXPIRED),
                Log = MessageUtil.RemoveHtmlTag(message.GetString(DDTP.FIELD_LOG))
            };

            var vector = message.GetObject(DDTP.FIELD_THREAD, new List<object>());
            if (vector != null && vector is List<object>)
            {
                var listThread = (List<object>)vector;
                listThread.ForEach(x => {
                    if (x is List<object>)
                    {
                        var listParam = (List<object>)x;
                        try
                        {
                            ThreadInfo info = new ThreadInfo(Convert.ToString(listParam[0]))
                            {
                                Name = Convert.ToString(listParam[1]),
                                Status = Convert.ToString(listParam[2])
                            };
                            info.LogLines = MessageUtil.AppendLog(info.LogLines, Convert.ToString(listParam[3]));
                            resp.ThreadInfos.Add(info);
                        }
                        catch { }
                    }
                });
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("DDTP");
            message.Data.ToList().ForEach(x => {
                if (DDTP.FIELD_LOG.Equals(x.Key) || x.Value is List<object>)
                    return;
                sb.Append(string.Format("\r\n{0}={1}", x.Key, message.FormatValue(x.Value)));
            });

            sb.Append("\r\n" + DDTP.FIELD_THREAD);
            sb.Append("[");
            resp.ThreadInfos.ForEach(x => sb.Append(string.Format("[{0},{1},{2}]", x.Id, x.Name, x.Status)));
            sb.Append("]");

            resp.OriginalContent = sb.ToString();

            return resp;
        }

        public SettingInfoEvent ToSettingInfo(DDTP message)
        {
            SettingInfoEvent info = new SettingInfoEvent()
            {
                Identifier = message.ResponseId,
                ThreadClassName = message.GetString(DDTP.FIELD_THREAD_CLASS),
                ThreadStartupType = Convert.ToInt32(message.GetString(DDTP.FIELD_THREAD_STARTUP_TYPE, "0"))
            };
            var vtTypes = message.GetObject(DDTP.FIELD_STARTUP_TYPE);
            if (vtTypes != null && vtTypes is List<object>)
            {
                var lstType = (List<object>)vtTypes;
                lstType.ForEach(x => {
                    try
                    {
                        var values = (List<object>)x;
                        info.StartupType.Add(Convert.ToInt32(values[0]), Convert.ToString(values[1]));
                    }
                    catch { }
                });
            }
            var vtSetting = message.GetObject(DDTP.FIELD_THREAD_SETTING);
            if (vtSetting != null && vtSetting is List<object>)
            {
                var lstSetting = (List<object>)vtSetting;
                lstSetting.ForEach(x => {
                    try
                    {
                        var setting = (List<object>)x;
                        ThreadParameter p = new ThreadParameter();
                        p.Name = Convert.ToString(setting[0]);
                        p.Value = setting[1];
                        p.Type = (ParameterType)Convert.ToInt32(setting[2]);
                        p.OriginalDefinition = setting[3];
                        p.Definition = ThreadParameter.GetParameterDefinition(setting[3]);
                        try
                        {
                            p.Description = Convert.ToString(setting[4]);
                            p.Index = Convert.ToString(setting[5]);
                        }
                        catch { }
                        info.Parameters.Add(p);
                    }
                    catch { }
                });
            }

            return info;
        }

        public ScheduleInfoEvent ToScheduleInfo(DDTP message)
        {
            ScheduleInfoEvent ev = new ScheduleInfoEvent()
            {
                Identifier = message.ResponseId
            };

            var content = message.GetString(DDTP.FIELD_RETURN);
            if (!string.IsNullOrWhiteSpace(content))
            {
                content.Split(new string[] { "Schedule==" }, 
                    StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(setting => {
                        setting = setting.Trim();
                        if (string.IsNullOrWhiteSpace(setting))
                        {
                            return;
                        }
                        try
                        {
                            var parameters = setting.Split(new string[] { "\\r\\n" },
                                StringSplitOptions.RemoveEmptyEntries)
                                .Select(item => item.Split(new string[] { "==" },
                                StringSplitOptions.None))
                                .ToDictionary(item => item[0], item => item.Length >= 2 ? item[1] : "");

                            ScheduleSetting schedule = new ScheduleSetting()
                            {
                                ScheduleId = parameters["ScheduleID"],
                                Type = (ScheduleType)Convert.ToInt32(parameters.GetOrDefault("ScheduleType", "")),
                                Times = Convert.ToInt32(parameters.GetOrDefault("ExecutionTime", "")),
                                Cycle = Convert.ToInt32(parameters.GetOrDefault("AdditionValue", "")),
                                StartTime = parameters.GetOrDefault("StartTime", ""),
                                EndTime = parameters.GetOrDefault("EndTime", ""),
                                ExpectDate = DateTime.ParseExact(parameters.GetOrDefault("ExpectedDate", ""),
                                        "dd/MM/yyyy", null),
                                WeekDays = parameters.GetOrDefault("WeekDay", "").Split(new char[] { ',' },
                                                StringSplitOptions.RemoveEmptyEntries).ToList()
                                                .Select(x => Convert.ToInt32(x)).ToList(),
                                MonthDays = parameters.GetOrDefault("MonthDay", "").Split(new char[] { ',' },
                                                StringSplitOptions.RemoveEmptyEntries).ToList()
                                                .Select(x => Convert.ToInt32(x)).ToList(),
                                YearMonths = parameters.GetOrDefault("YearMonth", "").Split(new char[] { ',' },
                                                StringSplitOptions.RemoveEmptyEntries).ToList()
                                                .Select(x => Convert.ToInt32(x)).ToList(),
                            };

                            ev.Schedule.Add(schedule);
                        }
                        catch
                        {
                            throw new Exception("invalid-data");
                        }
                    });
            }

            return ev;
        }

        public AddScheduleResponse ToScheduleResponse(DDTP message)
        {
            return new AddScheduleResponse()
            {
                Identifier = message.ResponseId,
                ScheduleId = message.GetString("ScheduleID")
            };
        }
        public ViewLogEvent ToViewLogEvent(DDTP message)
        {
            var evt = new ViewLogEvent()
            {
                Identifier = message.ResponseId
            };

            var dirObj = message.GetObject("vtDirLog");
            if (dirObj != null && dirObj is List<object>)
            {
                var listDirs = dirObj as List<object>;
                listDirs.ForEach(dir => {
                    if (dir != null && dir is List<object>)
                    {
                        var dirInfo = dir as List<object>;
                        if (dirInfo.Count > 0)
                        {
                            LogDir directory = new LogDir()
                            {
                                Directory = Convert.ToString(dirInfo[0])
                            };
                            for (int i = 1; i < dirInfo.Count; i++)
                            {
                                directory.Files.Add(Convert.ToString(dirInfo[i]));
                            }

                            evt.Dirs.Add(directory);
                        }
                    }
                });
            }

            return evt;
        }

        public LogContentEvent ToLogContentEvent(DDTP message)
        {
            return new LogContentEvent()
            {
                Identifier = message.ResponseId,
                Content = message.GetString("LogContent")
            };
        }

        public ManageThreadResponse ToManageThreadResponse(DDTP message)
        {
            var evt = new ManageThreadResponse()
            {
                Identifier = message.ResponseId
            };

            var vtTypes = message.GetObject(DDTP.FIELD_STARTUP_TYPE);
            if (vtTypes != null && vtTypes is List<object>)
            {
                var lstType = (List<object>)vtTypes;
                lstType.ForEach(x => {
                    try
                    {
                        var values = (List<object>)x;
                        evt.StartupType.Add(Convert.ToInt32(values[0]), Convert.ToString(values[1]));
                    }
                    catch { }
                });
            }

            var vtThreadData = message.GetObject("vtTableData");
            if (vtThreadData != null && vtThreadData is List<object>)
            {
                var lstThread = vtThreadData as List<object>;
                lstThread.ForEach(x => {
                    if (x is List<object>)
                    {
                        var listParam = x as List<object>;
                        try
                        {
                            ThreadInfo info = new ThreadInfo(Convert.ToString(listParam[3]))
                            {
                                Name = Convert.ToString(listParam[0]),
                                ClassName = Convert.ToString(listParam[1]),
                                Status = Convert.ToString(listParam[2])
                            };
                            evt.Threads.Add(info);
                        }
                        catch { }
                    }
                });
            }
            return evt;
        }

        public void CustomCommand(DDTP ddtp, CommandBase cmd)
        {
            if (cmd is StoreSettingCommand)
            {
                var command = cmd as StoreSettingCommand;
                ddtp.SetString(DDTP.FIELD_THREAD_NAME, command.ThreadName);
                ddtp.SetString(DDTP.FIELD_THREAD_CLASS_CMD, command.ClassName);
                ddtp.SetString(DDTP.FIELD_THREAD_STARTUP_TYPE, command.ThreadStartupType);

                var lstParameter = new List<object>();
                command.Parameters.ForEach(parameter => {
                    var param = new List<object>();
                    lstParameter.Add(param);
                    param.Add(parameter.Name);
                    param.Add(parameter.Value);
                    param.Add(parameter.Type);
                    param.Add(parameter.OriginalDefinition);
                    param.Add(parameter.Description);
                    param.Add(parameter.Index);
                });

                ddtp.SetObject(DDTP.FIELD_THREAD_SETTING, lstParameter);
            }
            else if (cmd is DeletecheduleCommand)
            {
                var command = cmd as DeletecheduleCommand;
                ddtp.SetString(DDTP.FIELD_SCHEDULE_ID, command.ScheduleId);
            }
            else if (cmd is ModifyScheduleCommand)
            {
                var command = cmd as ModifyScheduleCommand;
                ddtp.SetString(DDTP.FIELD_SCHEDULE_ID, command.Schedule.ScheduleId);
                ddtp.SetString(DDTP.FIELD_SCHEDULE, ScheduleSettingToString(command.Schedule));
            }
            else if (cmd is AddScheduleCommand)
            {
                var command = cmd as AddScheduleCommand;
                ddtp.SetString(DDTP.FIELD_SCHEDULE, ScheduleSettingToString(command.Schedule));
            }
            else if (cmd is LoadLogContentCommand)
            {
                var command = cmd as LoadLogContentCommand;
                ddtp.SetString(DDTP.FIELD_THREAD_LOG_NAME, command.ThreadLogName);
            }
            else if (cmd is StoreThreadCommand)
            {
                var command = cmd as StoreThreadCommand;
                ddtp.SetString(DDTP.FIELD_THREAD_NAME, command.Name);
                ddtp.SetString(DDTP.FIELD_THREAD_CLASS_CMD, command.ClassName);
                ddtp.SetString(DDTP.FIELD_THREAD_STARTUP_TYPE, command.Status);
            }
            else if (cmd is ChangePasswordCommand)
            {
                var command = cmd as ChangePasswordCommand;
                ddtp.SetString(DDTP.FIELD_OLD_PASSWORD, command.OldPassword);
                ddtp.SetString(DDTP.FIELD_NEW_PASSWORD, command.NewPassword);
                ddtp.SetString(DDTP.FIELD_REAL_PASSWORD, command.RealPassword);
            }
            else if (cmd is KickUserCommand)
            {
                var command = cmd as KickUserCommand;
                ddtp.SetString("strChannel", command.SessionId);
            }
            else if (cmd is SendMessageCommand)
            {
                var command = cmd as SendMessageCommand;
                ddtp.SetString("strMessage", command.Message);
            }
        }

        private string ScheduleSettingToString(ScheduleSetting setting)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("ScheduleType==").Append((int)setting.Type).Append("\r\n");
            sb.Append("ExecutionTime==").Append(setting.Times).Append("\r\n");
            sb.Append("AdditionValue==").Append(setting.Cycle).Append("\r\n");
            sb.Append("StartTime==").Append(setting.StartTime).Append("\r\n");
            sb.Append("EndTime==").Append(setting.EndTime).Append("\r\n");
            sb.Append("ExpectedDate==").Append(setting.ExpectDate.ToString("dd/MM/yyyy")).Append("\r\n");
            sb.Append("WeekDay");
            if (setting.WeekDays.Count > 0)
            {
                sb.Append("==");
                sb.Append(string.Join(",", setting.WeekDays.ToArray()));
            }
            sb.Append("\r\n");
            sb.Append("MonthDay");
            if (setting.MonthDays.Count > 0)
            {
                sb.Append("==");
                sb.Append(string.Join(",", setting.MonthDays.ToArray()));
            }
            sb.Append("\r\n");
            sb.Append("YearMonth");
            if (setting.YearMonths.Count > 0)
            {
                sb.Append("==");
                sb.Append(string.Join(",", setting.YearMonths.ToArray()));
            }
            return sb.ToString();
        }
    }
}
