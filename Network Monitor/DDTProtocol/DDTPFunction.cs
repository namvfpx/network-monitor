﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Utils;
using NetworkMonitor.Network;

namespace NetworkMonitor.DDTProtocol
{
    public class DDTPFunction
    {
        public static Dictionary<ThreadCommandType, string> funcMapping = new Dictionary<ThreadCommandType, string>();
        static DDTPFunction()
        {
            funcMapping.Add(ThreadCommandType.LogIn, "login");
            funcMapping.Add(ThreadCommandType.ChangePassword, "changePassword");
            funcMapping.Add(ThreadCommandType.KichUser, "kickUser");
            funcMapping.Add(ThreadCommandType.LoadManagedThreads, "manageThreadsLoad");
            funcMapping.Add(ThreadCommandType.StoreManagedThread, "manageThreadsStore");
            funcMapping.Add(ThreadCommandType.QueryUserList, "queryUserList");
            funcMapping.Add(ThreadCommandType.SendMessage, "sendMessage");
            funcMapping.Add(ThreadCommandType.StartThread, "startThread");
            funcMapping.Add(ThreadCommandType.StartThreadImmediate, "startImmediate");
            funcMapping.Add(ThreadCommandType.StopThread, "stopThread");
            funcMapping.Add(ThreadCommandType.DestroyThread, "destroyThread");
            funcMapping.Add(ThreadCommandType.LoadThreadSetting, "loadSetting");
            funcMapping.Add(ThreadCommandType.LoadThreadSchedule, "querySchedule");
            funcMapping.Add(ThreadCommandType.AddThreadSchedule, "addSchedule");
            funcMapping.Add(ThreadCommandType.ModifyThreadSchedule, "updateSchedule");
            funcMapping.Add(ThreadCommandType.DeleteThreadSchedule, "deleteSchedule");
            funcMapping.Add(ThreadCommandType.LoadThreadLog, "loadThreadLog");
            funcMapping.Add(ThreadCommandType.LoadThreadLogContent, "loadThreadLogContent");
            funcMapping.Add(ThreadCommandType.StoreThreadSetting, "storeSetting");
        }

        public static string GetFunction(ThreadCommandType cmdType)
        {
            return funcMapping.GetOrDefault(cmdType, "");
        }

        public static string GetClass(ThreadCommandType cmdType)
        {
            return "ThreadProcessor";
        }
    }
}
