﻿using NetworkMonitor.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkMonitor.Utils;
using System.IO;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using System.Collections;

namespace NetworkMonitor.DDTProtocol
{
    public class DDTP
    {
        const string ENCODE = "utf-8";
        public const string SIGN_SYMBOL = "DDTP";
        const int COMPRESSED_BUFFER = 1;
        const int PLAIN_BUFFER = 0;
        const byte STRING_SYMBOL = 0;
        const byte VECTOR_SYMBOL = 1;
        const byte BINARY_SYMBOL = 2;
        public const string FIELD_REQUEST_ID = "$reqid$";
        public const string FIELD_RESPONSE_ID = "$resid$";
        public const string FIELD_RETURN = "$rtn$";
        public const string FIELD_ERROR_DESCRIPTION = "$err.dsc$";
        public const string FIELD_ERROR_CONTEXT = "$err.ctx$";
        public const string FIELD_ERROR_INFO = "$err.inf$";
        public const string FIELD_FUNCTION = "$fnc$";
        public const string FIELD_CLASS = "$cls$";
        public const string FIELD_USERNAME = "UserName";
        public const string FIELD_USERNAME_EVT = "strUserName"; // suck name but it ported from Java DDTP
        public const string FIELD_HOST = "strHost"; // suck name but it ported from Java DDTP
        public const string FIELD_PASSWORD = "Password";
        public const string FIELD_CHANNEL = "strChannel";
        public const string FIELD_THREAD = "vtThread";
        public const string FIELD_LOG = "strLog";
        public const string FIELD_THREAD_APP_NAME = "strThreadAppName";
        public const string FIELD_THREAD_APP_VER = "strThreadAppVersion";
        public const string FIELD_APP_NAME = "strAppName";
        public const string FIELD_APP_VERSION = "strAppVersion";
        public const string FIELD_PASSWORD_EXPIRED = "PasswordExpired";
        public const string FIELD_LOG_RESULT = "LogResult";
        public const string FIELD_THREAD_ID = "ThreadID";
        public const string FIELD_THREAD_STATUS = "ThreadStatus";
        public const string FIELD_THREAD_NAME = "ThreadName";
        public const string FIELD_THREAD_CLASS = "ThreadClassName";
        public const string FIELD_THREAD_CLASS_CMD = "ThreadClass"; // suck name but it ported from Java DDTP
        public const string FIELD_THREAD_STARTUP_TYPE = "ThreadStartupType";
        public const string FIELD_THREAD_SETTING = "vtSetting";
        public const string FIELD_STARTUP_TYPE = "vtStartupType";
        public const string FIELD_START_DATE = "strStartDate";
        public const string FIELD_SCHEDULE_ID = "ScheduleID";
        public const string FIELD_SCHEDULE = "Schedule";
        public const string FIELD_THREAD_LOG_NAME = "ThreadLogName";
        public const string FIELD_OLD_PASSWORD = "OldPassword";
        public const string FIELD_NEW_PASSWORD = "NewPassword";
        public const string FIELD_REAL_PASSWORD = "RealPassword";

        static bool AUTO_COMPRESS = false;
        public Dictionary<string, object> Data { get; private set; } = new Dictionary<string, object>();

        public string RequestId
        {
            get
            {
                return GetString(FIELD_REQUEST_ID, "");
            }
            set
            {
                SetString(FIELD_REQUEST_ID, value);
            }
        }

        public string ResponseId
        {
            get
            {
                return GetString(FIELD_RESPONSE_ID, "");
            }
            set
            {
                SetString(FIELD_RESPONSE_ID, value);
            }
        }

        public string Function
        {
            get
            {
                return GetString(FIELD_FUNCTION, "");
            }
            set
            {
                SetString(FIELD_FUNCTION, value);
            }
        }
        public string ClassName
        {
            get
            {
                return GetString(FIELD_CLASS, "");
            }
            set
            {
                SetString(FIELD_CLASS, value);
            }
        }
        public string Username
        {
            get
            {
                return GetString(FIELD_USERNAME, "");
            }
            set
            {
                SetString(FIELD_USERNAME, value);
            }
        }

        public DDTP()
        {

        }

        public bool Contains(string parameter)
        {
            return Data.ContainsKey(parameter);
        }

        public void SetString(string name, object value)
        {
            if (name == null)
                name = "";
            if (value == null)
                return;
            if (Data.ContainsKey(name))
                Data[name] = Convert.ToString(value);
            else
                Data.Add(name, Convert.ToString(value));
        }

        public void SetObject(string name, object value)
        {
            if (name == null)
                name = "";
            if (value == null)
                return;
            if (Data.ContainsKey(name))
                Data[name] = value;
            else
                Data.Add(name, value);
        }

        public string GetString(string name, string defaultValue = "")
        {
            return Convert.ToString(GetObject(name, ""));
        }

        public object GetObject(string name, object defaultValue = null)
        {
            return Data.GetOrDefault(name, defaultValue);
        }

        public byte[] ToByteArray()
        {
            MemoryStream stream = new MemoryStream();
            Stream wrapStream = null;
            if (AUTO_COMPRESS)
                wrapStream = new DeflaterOutputStream(stream);
            else
                wrapStream = stream;

            try
            {
                Data.ToList().ForEach(x => {
                    var name = Encoding.GetEncoding(ENCODE).GetBytes(x.Key);
                    WriteToStream(wrapStream, IntToByteArray(name.Length));
                    WriteToStream(wrapStream, name);
                    WriteToStream(wrapStream, ValueToByteArray(x.Value));
                });
                wrapStream.Flush();
                var outStream = new MemoryStream();
                WriteToStream(outStream, Encoding.Default.GetBytes(SIGN_SYMBOL));
                if (AUTO_COMPRESS)
                {
                    outStream.WriteByte(COMPRESSED_BUFFER);
                }
                else
                {
                    outStream.WriteByte(PLAIN_BUFFER);
                }
                WriteToStream(outStream, IntToByteArray(Data.Count));
                WriteToStream(outStream, IntToByteArray((int)stream.Length));
                WriteToStream(outStream, stream.ToArray());

                return outStream.ToArray();
            }
            finally
            {
                wrapStream.Close();
            }
        }

        private byte[] ValueToByteArray(object value)
        {
            MemoryStream mem = new MemoryStream();
            if (value is List<object>)
            {
                mem.WriteByte(VECTOR_SYMBOL);
                if (value == null)
                    mem.WriteByte(0x80);
                else
                {
                    List<object> lst = (List<object>)value;
                    WriteToStream(mem, IntToByteArray(lst.Count));
                    lst.ForEach(x => WriteToStream(mem, ValueToByteArray(x)));
                }
            }
            else if (value is byte[])
            {
                mem.WriteByte(BINARY_SYMBOL);

                // Write content
                if (value == null)
                    mem.WriteByte(0x80);
                else
                {
                    // Create content buffer
                    byte[] bt = (byte[])value;

                    // Write content to stream
                    WriteToStream(mem, IntToByteArray(bt.Length));
                    WriteToStream(mem, bt);
                }
            }
            else
            {
                mem.WriteByte(STRING_SYMBOL);

                // Write content
                if (value == null)
                    mem.WriteByte(0x80);
                else
                {
                    byte[] bt = Encoding.GetEncoding(ENCODE).GetBytes(value.ToString());
                    WriteToStream(mem, IntToByteArray(bt.Length));
                    WriteToStream(mem, bt);
                }
            }
            return mem.ToArray();
        }
        public static DDTP LoadData(Stream dataStream)
        {
            DDTP pack = new DDTP();
            byte[] btBuff = null;

            byte[] data = new byte[SIGN_SYMBOL.Length];
            int read = dataStream.Read(data, 0, data.Length);
            if (read <= 0)
            {
                throw new IOException("connection-lost");
            }
            string symb = Encoding.ASCII.GetString(data);
            if (!SIGN_SYMBOL.Equals(symb))
            {
                throw new Exception("invalid-ddtp-pack");
            }

            int iType = Convert.ToByte(dataStream.ReadByte());

            var iParameterCount = ReadBufferSize(dataStream);
            var iBufferSize = ReadBufferSize(dataStream);

            btBuff = new byte[iBufferSize];
            int iByteRemain = iBufferSize;
            while (iByteRemain > 0)
            {
                int iByteRead = dataStream.Read(btBuff, iBufferSize - iByteRemain, iByteRemain);
                if (iByteRead <= 0)
                    throw new IOException("connection-lost");
                iByteRemain -= iByteRead;
            }

            Stream isUsed = new MemoryStream(btBuff);
            try
            {
                if (iType == COMPRESSED_BUFFER)
                    isUsed = new InflaterInputStream(isUsed);
                else if (iType != PLAIN_BUFFER)
                    throw new IOException("Unsupported buffer type " + iType);

                while (iParameterCount > 0)
                {
                    // Get name
                    int iNameSize = ReadBufferSize(isUsed);
                    data = new byte[iNameSize];
                    read = isUsed.Read(data, 0, data.Length);
                    string strName = Encoding.GetEncoding(ENCODE).GetString(data);

                    // Put value into storage
                    pack.Data.Add(strName, LoadValue(isUsed));
                    iParameterCount--;
                }

                return pack;
            }
            finally
            {
                isUsed.Close();
            }
        }

        private static object LoadValue(Stream stream)
        {
            int iType = stream.ReadByte();
            if (iType < 0)
                throw new IOException("connection-lost");

            // Get value length
            int iSize = ReadBufferSize(stream);

            // Get value
            object objValue = null;
            if (iType == VECTOR_SYMBOL) // Vector
            {
                List<object> vt = new List<object>();
                for (int iIndex = 0; iIndex < iSize; iIndex++)
                    vt.Add(LoadValue(stream));
                objValue = vt;
            }
            else if (iType == STRING_SYMBOL) // String
            {
                objValue = ReadFromStream(stream, iSize, ENCODE);
            }
            else if (iType == BINARY_SYMBOL) // Binary
                objValue = ReadFromStream(stream, iSize);
            else
                throw new IOException("Data type not supported");
            return objValue;
        }

        private static byte[] IntToByteArray(int input)
        {
            byte[] bt = null;
            var i = (uint)input;
            if (i > 0xFE00000)
            {
                bt = new byte[5];
                bt[4] = (byte)(0x80 | (0x7F & i));
                i >>= 7;
                bt[3] = (byte)(0x7F & i);
                i >>= 7;
                bt[2] = (byte)(0x7F & i);
                i >>= 7;
                bt[1] = (byte)(0x7F & i);
                i >>= 7;
                bt[0] = (byte)(0x7F & i);
                i >>= 7;
            }
            else if (i > 0x1FC000)
            {
                bt = new byte[4];
                bt[3] = (byte)(0x80 | (0x7F & i));
                i >>= 7;
                bt[2] = (byte)(0x7F & i);
                i >>= 7;
                bt[1] = (byte)(0x7F & i);
                i >>= 7;
                bt[0] = (byte)(0x7F & i);
            }
            else if (i > 0x3F80)
            {
                bt = new byte[3];
                bt[2] = (byte)(0x80 | (0x7F & i));
                i >>= 7;
                bt[1] = (byte)(0x7F & i);
                i >>= 7;
                bt[0] = (byte)(0x7F & i);
            }
            else if (i > 0x7F)
            {
                bt = new byte[2];
                bt[1] = (byte)(0x80 | (0x7F & i));
                i >>= 7;
                bt[0] = (byte)(0x7F & i);
            }
            else
            {
                bt = new byte[1];
                bt[0] = (byte)(0x80 | (0x7F & i));
            }
            return bt;
        }

        private static int ReadBufferSize(Stream stream)
        {
            int iResult = 0;
            int iCount = 0;
            int iByteRead = stream.ReadByte();

            while (iByteRead >= 0)
		    {
			    if(iCount > 3 && ((iByteRead & 0x7F) > 0xF))
				    throw new IOException("Value out of range");
			    else
				    iCount++;
			    iResult = (iResult << 7) | (iByteRead & 0x7F);
			    if(iByteRead > 127)
				    break;
			    iByteRead = stream.ReadByte();
            }
		    if(iByteRead< 0)
			    throw new IOException("No data to read");
		    return iResult;
	    }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("DDTP");
            Data.ToList().ForEach(x => sb.Append(string.Format("\r\n{0}={1}", x.Key, FormatValue(x.Value))));
            return sb.ToString();
        }

        public string FormatValue(object value)
        {
            if (value is IList &&
                   value.GetType().IsGenericType &&
                   value.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>)))
            {
                var lst = (List<object>)value;
                StringBuilder sb = new StringBuilder();

                sb.Append("[");
                if (lst != null)
                {
                    if (lst.Count > 0)
                    {
                        sb.Append(FormatValue(lst[0]));
                    }
                    for (int i = 1; i < lst.Count; i++)
                    {
                        sb.Append(",").Append(FormatValue(lst[i]));
                    }

                }
                sb.Append("]");
                return sb.ToString();
            }
            else
            {
                return Convert.ToString(value);
            }
        }

        private static void WriteToStream<T>(T stream, byte[] data) where T : Stream
        {
            stream.Write(data, 0, data.Length);
        }

        private static string ReadFromStream<T>(T stream, int length, string encoding) where T : Stream
        {
            if (length == 0)
                return "";
            var data = new byte[length];
            stream.Read(data, 0, data.Length);
            return Encoding.GetEncoding(encoding).GetString(data);
        }
        private static byte[] ReadFromStream<T>(T stream, int length) where T : Stream
        {
            var data = new byte[length];
            stream.Read(data, 0, data.Length);
            return data;
        }
    } 
}
