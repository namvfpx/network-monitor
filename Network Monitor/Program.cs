﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using NetworkMonitor.Utils;
using NetworkMonitor.View;

namespace NetworkMonitor
{
    static class Program
    {
        public static object LocalizationUtil { get; private set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ConfigLoader.Init();
            LanguageUtil.FromFile(Config.DefaultLanguage);
            while (Global.ContinueToLoginForm)
            {
                Application.Run(new View.ProfileLoginView());
                if (Global.ContinueToMainForm)
                {
                    Application.Run(new View.MonitorForm());
                }
            }
            LanguageUtil.ToFile(Config.DefaultLanguage);
        }
    }
}
