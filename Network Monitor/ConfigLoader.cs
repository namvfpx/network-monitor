﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NetworkMonitor.Utils;

namespace NetworkMonitor
{
    public class ConfigLoader
    {
        public static void Init()
        {
            Config.Timeout = Properties.Settings.Default.Timeout;
            Config.DefaultLanguage = Properties.Settings.Default.DefaultLanguage;
            Global.ProfileDirectory = Properties.Settings.Default.ProfileDirectory;
        }
    }
}
