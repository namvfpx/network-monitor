﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkMonitor.Utils
{
    public class Localization
    {
        public static void LocalizationControl(Control control)
        {
            if (control != null)
            {
                control.Text = LanguageUtil.GetText(control.Text);
            }
            control.Controls.OfType<Control>().ToList().ForEach(x => {
                LocalizationControl(x);
            });
        }
    }
}
