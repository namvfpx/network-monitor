﻿using NetworkMonitor.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkMonitor
{
    public class Global
    {
        public static string ProfileDirectory { get; set; } = "";
        public static string ProfileFileName { get; set; } = "user.pf";
        public static string StoredNodeFileName { get; set; } = "user.dat";
        public static LocalProfile CurrentProfile { get; set; } = null;
        public static bool ContinueToMainForm { get; set; } = true;
        public static bool ContinueToLoginForm { get; set; } = true;
    }
}
